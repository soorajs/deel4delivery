<!DOCTYPE html>
<html>
<body>

Name: <input type="text" id="myText">

<p>Click the button to disable the text field.</p>

<button onclick="myFunction()">Disable Text field</button>
<button onclick="myFunction1()">Disable Text field</button>
<script>
function myFunction() {
    document.getElementById("myText").disabled = true;
}
</script>
<script>
function myFunction1() {
    document.getElementById("myText").disabled = false;
}
</script>
</body>
</html>