﻿<!DOCTYPE html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head><title>Oliu Tech</title>
</head>
<style>
/* The Modal (background) */
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content */
.modal-content {
    position: relative;
    background-color: #fefefe;
    margin: auto;
    padding: 0;
    border: 1px solid #888;
    width: 40%;
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);
    -webkit-animation-name: animatetop;
    -webkit-animation-duration: 0.4s;
    animation-name: animatetop;
    animation-duration: 0.4s
}

/* Add Animation */
@-webkit-keyframes animatetop {
    from {top:-300px; opacity:0}
    to {top:0; opacity:1}
}

@keyframes animatetop {
    from {top:-300px; opacity:0}
    to {top:0; opacity:1}
}

/* The Close Button */
.close {
    color: white;
    float: right;
    font-size: 28px;
    font-weight: bold;
}

.close:hover,
.close:focus {
    color: #000;
    text-decoration: none;
    cursor: pointer;
}

.modal-header {
    padding: 2px 16px;
    background-color:#59b210;
    color: white;
}

.modal-body {padding: 2px 16px;}

.modal-footer {
    padding: 2px 16px;
    background-color:#59b210;
    color: white;
}
</style>
<style type="text/css" xml:space="preserve">
BODY, P,TD{ font-family: Arial,Verdana,Helvetica, sans-serif; font-size: 10pt }
A{font-family: Arial,Verdana,Helvetica, sans-serif;}
B {	font-family : Arial, Helvetica, sans-serif;	font-size : 12px;	font-weight : bold;}
.error_strings{ font-family:Verdana; font-size:14px; color:#660000; background-color:#F7F4F4;}
</style><script language="JavaScript" src="gen_validatorv4.js"
    type="text/javascript" xml:space="preserve"></script>
<script type="text/javascript">
$(document).on("keypress", "form", function(event) { 
    return event.keyCode != 13;
});
</script>
<script src="https://code.jquery.com/jquery-2.1.1.min.js" type="text/javascript"></script>
<script>
function checkAvailability() {
	//$("#loaderIcon").show();
		$("#addto1-cart").hide();
		$("#addto2-cart").hide();
		$("#addto3-cart").hide();
		$("#addto4-cart").hide();
		$("#addto6-cart").hide();
	jQuery.ajax({
	url: "check_availability.php",
	data:'username='+$("#qty").val(),
	type: "POST",
	success:function(data){
		$("#user-availability-status").html(data);
		$("#cart-dis").html(data);
		$("#loaderIcon").hide();
		$("#addto-cart").hide();

	},
	error:function (){}
	});
}
</script>
<body>
<!-- ============================================================= HEADER : END ============================================================= -->	
<?php
include("topbar.php");
?>
<div id="single-product">
<br><br><br>
<div class="container">
<div class="no-margin col-xs-12 col-sm-6 col-md-5 gallery-holder">
<div class="product-item-holder size-big single-product-gallery small-gallery">
<div id="owl-single-product">
<!-- /.single-product-gallery-item --><!-- /.single-product-gallery-item -->
<div class="single-product-gallery-item" id="slide3">

<?php

	//include("dboperation.php");
	if (!isset($_SESSION["c_id"]))
		{
 		 $_SESSION["c_id"] = 0;
		}
		//echo $_SESSION["c_id"];
	if(isset($_GET['var']))
	{
		//echo $_GET['var'];
		$pid=$_GET['var'];
	}
		$obj1=new dboperation();
		$query1="SELECT * FROM `productt` WHERE p_id='$pid' ";
		$result1=$obj1->selectdata($query1);
		if($f1=$obj1->fetch($result1))
		{
			//echo'<div class="single-product-gallery-item" id="slide1">';
			//echo'<a data-rel="prettyphoto" href="images/'.$f1['p_image'].'">';
			echo '<img  alt="" src="images/'.$f1['p_image'].'" />' ;
			
			//echo'</a>';
			//echo'</div>';
			$p_id=$f1['p_id'];
			$_SESSION['pid']=$p_id;
			$price=$f1['price'];
			$_SESSION['price']=$price;
echo'</div><!-- /.single-product-gallery-item -->';
echo'</div><!-- /.single-product-slider -->';
echo'<!-- /.gallery-thumbs -->';
echo'</div><!-- /.single-product-gallery -->';
echo'</div><!-- /.gallery-holder -->';
echo'<div class="no-margin col-xs-12 col-sm-7 body-holder">';
echo'<div class="body">';
//echo'<div class="star-holder inline"><div class="star" data-score="4"></div></div>';
$count=$f1['count'];
if($f1['count'] >= 1)
{
	echo'<div class="availability"><label>Availability:</label><span class="label label-success wishlist-in-stock"> in stock</span></div>';
}
else
{
	echo'<div class="availability"><label>Availability: </label></div>';
	echo'<div>';
	echo'<span class="label label-danger wishlist-out-of-stock">Out of Stock</span>';
	echo'</div>';
	}
	echo '<form action="specific_product_action.php" name="myform" id="myform" method="post" onkeypress="return event.keyCode != 13;">';
echo'<div class="title"><a href="#"><h1><font color="#009900">'.$f1['product'].'</font></h1></a></div>';
$br=$f1['brand_id'];
$query7="SELECT brand_name FROM `brand` WHERE brand_id='".$br."'";
$result7=$obj1->selectdata($query7);
if($f7=$obj1->fetch($result7))
{
echo'<div class="brand"><h2>'.$f7['brand_name'].'</h2></div>';
}
echo'<div class="social-row">';
echo'<span >Only '.$f1['count'].' numbers are remaining</span>';
//echo'<span class="st_twitter_hcount"></span>';
//echo'<span class="st_pinterest_hcount"></span>';
echo'</div>';
echo'<div class="buttons-holder">';
//echo'<a class="btn-add-to-wishlist" href="#">add to wishlist</a>';
//echo'<a class="btn-add-to-compare" href="#">add to compare list</a>';
echo'</div>';
echo'<div class="excerpt">';
echo '<h4 ><font color="#009900" >'. $f1['product'].'    has the following Features</font></h4></br>';
$des=$f1['description'];
$myArray = explode(',', $des);
echo'<ul>';
foreach($myArray as $my_Array){
    echo '<li><font color="#009900" >*</font>&nbsp;&nbsp;'.$my_Array.'</li>';  
}
echo'</ul>';
//echo'<p>'.$f1['description'].'</p>';
echo'</div>';
echo'<div class="prices">';
echo'<div class="price-current">Rs  ' .$f1['price']. '</div>';
//echo'<div class="price-prev">Rs'.$f1['price'].'</div>';
		}

echo'</div>';
echo'<div class="qnt-holder">';
echo'<div class="le-quantity">';
/*echo'<form>';
echo'<a class="minus" href="#reduce"></a>';
echo'<input name="quantity" readonly type="text" value="1"/>';
echo'<a class="plus" href="#add"></a>';
echo'</form>'; */
echo'Quantity    <input name="qty" min="0" id="qty" type="number" value="1" onchange="checkAvailability()">';?>
<div id='myform_qty_errorloc' class="error_strings"></div>
<?php
echo'</div>';
echo'&nbsp;&nbsp;&nbsp;&nbsp;';
echo'<span id="user-availability-status"></span>';
echo'&nbsp;&nbsp;&nbsp;&nbsp;';



//echo'<a id="addto-cart" href="index5f38.html?page=cart" class="le-button huge">add to cart</a>';
//----------------------------------------------------
//----------------------------------------------------
$k=1;

	$obj4=new dboperation();
	if($s!=0)
	{
	$query4="SELECT count(cart_id) FROM `cart_details` WHERE cust_id='".$s."' AND purchase=0 " ;
	}
	else
	{
		$query4="SELECT count(cart_id) FROM `cart_details` WHERE cust_id='".$id."' AND purchase=0 " ;
	}
	$reslt4=$obj4->selectdata($query4);
	$f4=$obj4->fetch($reslt4);
	$p_c=$f4[0];
	//echo'<h1>'.$p_c.'</h1>';
$obj3=new dboperation();
	if($s!=0)
	{
$query3="SELECT * FROM `cart_details` WHERE cust_id='".$s."'  AND purchase=0 " ;
	}
	else
	{
		$query3="SELECT * FROM `cart_details` WHERE cust_id='".$id."' AND purchase=0 " ;
	}
$result3=$obj3->selectdata($query3);
while($f3=$obj3->fetch($result3))
{
	$pp_id=$f3["p_id"];
//----------------------------------------------------
//----------------------------------------------------
	if($pid!=$pp_id )
	{
	$k=1;
	}
	else
	{
	$k=0;
	break;
	}
}
$_SESSION["k"]=$k;
//echo $k;
	if($k==0)
	{
		echo '<input name="addto_cart" type="button" id="addto6-cart" title="Product already added to the cart1"  value="Add To Cart" class="le-button huge disabled product_type_simple" >';
	}
	else
	{
		if($count <= 0)
	{
	//echo'<a class="le-button disabled product_type_simple" href="index5f38.html?page=cart">Add to cart</a>';
	echo '<input name="addto_cart1" type="button" id="addto4-cart" title="Product already added to the cart2"  value="Add To Cart" class="le-button huge disabled product_type_simple" >';
	
		
	
	//echo "The product is already added to the cart or it is out of stock";
	}
	else
	{
	echo '<input name="addto_cart" id="addto3-cart" type="submit"  value="Add To Cart" class="le-button huge " >';
		//echo'<div><span id="cart-dis"></span></div>';
	}
	}


echo '<div></div>';
echo'<div></div></div><!-- /.qnt-holder -->';
echo '</form>';
?>

</div><!-- /.body -->
</div><!-- /.body-holder -->
</div><!-- /.container -->

<br><br><br>
<?php
if($count<1)
{
	
?>
<p align="center">

<!-- Trigger/Open The Modal -->
<button id="myBtn" class="le-button">subscribe</button>

<!-- The Modal -->
<div id="myModal" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
    <div class="modal-header">
      <span class="close">×</span>
      <h3>Subscribe</h3>
    </div>
    <div class="modal-body">
    <p>If you will give your email id, we will inform when the stocks are update</p>
    <form method="post">
    
      <p align="center"><input type="email" placeholder="Enter your mail id" class="le-input" name="email_sub"></p>
      <p align="center"><input type="submit" class="le-button" value="Subscribe" name="email_btn"></p>
      
      </form>
      <?php
	  if(isset($_POST['email_btn']))
	  {
		  $obj5=new dboperation();
		  $email_name=$_POST['email_sub'];
		  $query9="INSERT INTO `email_sub` VALUES ('','$email_name','$p_id');";
		  $obj5->Ex_query($query9);
		  
	  }
	  ?>
    </div>
    <div class="modal-footer">
      <h3>Thank you</h3>
    </div>
  </div>

</div>



</p>
<?php
}
?>
</div><!-- /.single-product -->
<!-- ========================================= SINGLE PRODUCT TAB ========================================= -->
<!-- ========================================= SINGLE PRODUCT TAB ========================================= -->
<section id="single-product-tab">
<div class="container">
<div class="tab-holder">
<ul class="nav nav-tabs simple">

<li class="active"><a href="#additional-info" data-toggle="tab">Additional Information</a></li>
<li><a href="#reviews" data-toggle="tab">Reviews</a></li>
</ul><!-- /.nav-tabs -->
<div class="tab-content">

<div class="tab-pane active" id="additional-info">
<ul class="tabled-data">
<?php
$obj8=new dboperation();
$query8="select * from productt where p_id='".$p_id."'";
$result8=$obj8->selectdata($query8);
while($f8=$obj8->fetch($result8))
{
$des1=$f8['description'];
$myArray1 = explode(',', $des1);

foreach($myArray1 as $my_Array1){
    echo '<li><font color="#009900" >*</font>&nbsp;&nbsp;'.$my_Array1.'</li>';  
}
}
?>
</ul><!-- /.tabled-data -->

</div><!-- /.tab-pane #additional-info -->
<div class="tab-pane" id="reviews">
<div class="comments">
<?php
$objdr=new dboperation();
$querydr="SELECT * FROM `product_review` WHERE p_id='".$p_id."' AND approve=1 ";
$resultdr=$objdr->selectdata($querydr);
	$count_rev=0;
		 while($fdr=$objdr->fetch($resultdr))
		 {
			 $count_rev+=1;
	echo'
	<div class="comment-item">
	<div class="row no-margin">
	<div class="col-lg-1 col-xs-12 col-sm-2 no-margin">
	<div class="avatar">
	<img alt="avatar" src="assets/images/xdefault-avatar.jpg.pagespeed.ic.p_Ft6rm2_B.jpg">
	</div><!-- /.avatar -->
	</div><!-- /.col -->
	<div class="col-xs-12 col-lg-11 col-sm-10 no-margin">
	<div class="comment-body">
	<div class="meta-info">
	<div class="author inline">
	<a href="#" class="bold">'.$fdr['name'].'</a>
	</div>

	<div class="date inline pull-right">
12.07.2013
	</div>
	</div><!-- /.meta-info -->
	<p class="comment-text">
'.$fdr['review'].'
	</p><!-- /.comment-text -->
	</div><!-- /.comment-body -->
	</div><!-- /.col -->
	</div><!-- /.row -->
	</div><!-- /.comment-item -->';
	if($count_rev==4)
	{
		break;
	}
}
?>
</div><!-- /.comments -->
<div class="add-review row">
<div class="col-sm-8 col-xs-12">
<div class="new-review-form">
<h2>Add review</h2>
<form id="contact-form" class="contact-form" method="post">
<div class="row field-row">
<div class="col-xs-12 col-sm-6">
<label>name*</label>
<input class="le-input" name="rname" required>
</div>
<div class="col-xs-12 col-sm-6">
<label>email*</label>
<input class="le-input" name="remail" type="email" required>
</div>
</div><!-- /.field-row -->

<div class="field-row">
<label>your review</label>
<textarea rows="8" name="review" class="le-input" required></textarea>
</div><!-- /.field-row -->
<div class="buttons-holder">
<button type="submit" class="le-button huge" name="sub">submit</button>
</div><!-- /.buttons-holder -->
</form><!-- /.contact-form -->
<?php
if(isset($_POST['sub']))
{
$r_name=$_POST['rname'];
$r_email=$_POST['remail'];
$review=$_POST['review'];
$objr=new dboperation();
$queryr="insert into `product_review`  VALUES ('','$p_id','$review','$r_name','$r_email','')";
$objr->Ex_query($queryr);
}
?>
</div><!-- /.new-review-form -->
</div><!-- /.col -->
</div><!-- /.add-review -->
</div><!-- /.tab-pane #reviews -->
</div><!-- /.tab-content -->
</div><!-- /.tab-holder -->
</div><!-- /.container -->
</section><!-- /#single-product-tab -->
<!-- ========================================= SINGLE PRODUCT TAB : END ========================================= -->
<!-- ========================================= SINGLE PRODUCT TAB : END ========================================= -->
<!-- ========================================= RECENTLY VIEWED ========================================= -->
<section id="recently-reviewd" class="wow fadeInUp">
<div class="container">
<div class="carousel-holder hover">
<div class="title-nav">
<h2 class="h1">Recently Viewed</h2>
<div class="nav-holder">
<a href="#prev" data-target="#owl-recently-viewed" class="slider-prev btn-prev fa fa-angle-left"></a>
<a href="#next" data-target="#owl-recently-viewed" class="slider-next btn-next fa fa-angle-right"></a>
</div>
</div><!-- /.title-nav -->
<div id="owl-recently-viewed" class="owl-carousel product-grid-holder">
<?php
$query3="select * from `productt`";
$result3=$obj1->selectdata($query3);
while($f3=$obj1->fetch($result3))
{
	$ppid=$f3['p_id'];
echo'<div class=" no-margin carousel-item product-item-holder size-small hover">
<div class="product-item">
<div class="ribbon blue"><span>new!</span></div>
<div class="image">
<img alt="" src="images/'.$f3['p_image'].'"/>
</div>
<div class="body">
<div class="title">
'.$f3['product'].'
</div>
<div class="brand">harman</div>
</div>
<div class="prices">
<div class="price-current text-right">'.$f3['price'].'</div>
</div>
<div class="hover-area">
<div class="add-cart-button">';
echo"<a href='specific_product.php?var=$ppid' class='le-button'>add to cart</a>";
echo'</div>
</div>
</div><!-- /.product-item -->
</div><!-- /.product-item-holder -->';
}
?>
</div><!-- /#recently-carousel -->
</div><!-- /.carousel-holder -->
</div><!-- /.container -->
</section><!-- /#recently-reviewd -->
<!-- ========================================= RECENTLY VIEWED : END ========================================= -->	<!-- ============================================================= FOOTER ============================================================= -->

<?php
include("footer.php");
//echo $va;
?>
<!-- ============================================================= FOOTER : END ============================================================= -->	</div><!-- /.wrapper -->
<!-- For demo purposes – can be removed on production -->
<div class="config open"></div>
<script language="JavaScript" type="text/javascript"
    xml:space="preserve">//<![CDATA[
//You should create the validator only after the definition of the HTML form
  var frmvalidator  = new Validator("myform");
    frmvalidator.EnableOnPageErrorDisplay();
    frmvalidator.EnableMsgsTogether();

    frmvalidator.addValidation("qty","decimal","Please enter a valid quantity");
	
	//]]></script>
    <script>
// Get the modal
var modal = document.getElementById('myModal');

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal
btn.onclick = function() {
    modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}
</script>
</body>
</html>