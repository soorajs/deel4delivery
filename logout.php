<?php
   session_start();
   unset($_SESSION["c_id"]);
   session_destroy();
   echo 'Your session has expired';
   header('Refresh: 1; URL = index.php');
?>