-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 06, 2016 at 08:06 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ecommerce`
--

-- --------------------------------------------------------

--
-- Table structure for table `brand`
--

CREATE TABLE IF NOT EXISTS `brand` (
  `brand_id` int(11) NOT NULL AUTO_INCREMENT,
  `brand_name` varchar(30) NOT NULL,
  PRIMARY KEY (`brand_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `brand`
--

INSERT INTO `brand` (`brand_id`, `brand_name`) VALUES
(1, 'hp'),
(2, 'samsung'),
(3, 'acer');

-- --------------------------------------------------------

--
-- Table structure for table `cart_details`
--

CREATE TABLE IF NOT EXISTS `cart_details` (
  `cart_id` int(11) NOT NULL AUTO_INCREMENT,
  `cust_id` int(11) NOT NULL,
  `p_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `price` float NOT NULL,
  `purchase` int(11) NOT NULL,
  `delivered` int(11) NOT NULL,
  PRIMARY KEY (`cart_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=153 ;

--
-- Dumping data for table `cart_details`
--

INSERT INTO `cart_details` (`cart_id`, `cust_id`, `p_id`, `qty`, `price`, `purchase`, `delivered`) VALUES
(126, 36, 23, 2, 43000, 0, 0),
(127, 37, 27, 2, 17000, 0, 0),
(128, 37, 24, 1, 8500, 0, 0),
(129, 1, 23, 1, 43000, 1, 0),
(131, 36, 28, 0, 35000, 0, 0),
(132, 1, 28, 1, 35000, 1, 0),
(133, 28, 28, 1, 35000, 0, 0),
(136, 6, 52, 2, 12000, 1, 0),
(137, 6, 23, 1, 43000, 1, 0),
(138, 4, 23, 1, 43000, 0, 0),
(139, 4, 28, 2, 35000, 0, 0),
(140, 6, 28, 1, 35000, 1, 0),
(141, 1, 23, 1, 43000, 1, 0),
(142, 1, 23, 1, 43000, 1, 0),
(143, 4, 52, 1, 12000, 0, 0),
(145, 1, 22, 1, 12000, 0, 0),
(146, 1, 23, 1, 43000, 0, 0),
(147, 1, 27, 1, 17000, 0, 0),
(149, 38, 22, 1, 12000, 1, 0),
(150, 38, 22, 1, 12000, 0, 0),
(152, 1, 51, 1, 40000, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `categ_id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(30) NOT NULL,
  PRIMARY KEY (`categ_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`categ_id`, `category`) VALUES
(1, 'Electronics'),
(2, 'Home & Kitchen'),
(3, 'Vegitables & Meats'),
(4, 'Building Materials');

-- --------------------------------------------------------

--
-- Table structure for table `clientregistration`
--

CREATE TABLE IF NOT EXISTS `clientregistration` (
  `client_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_name` varchar(30) NOT NULL,
  `client_address` varchar(150) NOT NULL,
  `client_phno` varchar(15) NOT NULL,
  `client_emailid` varchar(50) NOT NULL,
  `client_username` varchar(50) NOT NULL,
  `password` varchar(20) NOT NULL,
  PRIMARY KEY (`client_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `clientregistration`
--

INSERT INTO `clientregistration` (`client_id`, `client_name`, `client_address`, `client_phno`, `client_emailid`, `client_username`, `password`) VALUES
(1, 'KFC', 'dfgh\r\ncvbnm', '2147483647', 'ss@gmail.com', 'Kfc', '21338'),
(2, 'sas', 'wert\r\ncvbn\r\nvbnm', '2147483647', 'esw@gmail.com', 'SAS', '18230'),
(3, 'sd', 'sdf', '8008651607', 'ss@gmail.com', 'student', '87325');

-- --------------------------------------------------------

--
-- Table structure for table `customer_registration`
--

CREATE TABLE IF NOT EXISTS `customer_registration` (
  `cust_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_mysql500_ci NOT NULL,
  `house` varchar(50) NOT NULL,
  `street` varchar(50) NOT NULL,
  `landmark` varchar(50) NOT NULL,
  `pin` int(11) NOT NULL,
  `town` varchar(50) NOT NULL,
  `state` varchar(30) NOT NULL,
  `ph_no` varchar(12) NOT NULL,
  `email` varchar(30) NOT NULL,
  `pass` varchar(15) NOT NULL,
  `ver_code` int(11) NOT NULL,
  PRIMARY KEY (`cust_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=39 ;

--
-- Dumping data for table `customer_registration`
--

INSERT INTO `customer_registration` (`cust_id`, `name`, `house`, `street`, `landmark`, `pin`, `town`, `state`, `ph_no`, `email`, `pass`, `ver_code`) VALUES
(1, 'sooraj s', 'SB h', 'TV Puram', 'PM Road', 609812, 'Vaikom', '', '8086516007', 'ssooraj699@gmail.com', 'soo', 5755),
(2, 'MuraliKrishna', '', '', '', 690103, 'MAVELIKARA', '', '8086516007', 'murali@gmail.com', 'asdf', 0),
(4, 'Prasanth', 'Njaluvallil', 'TV Puram', 'Near SBT', 609811, 'Vaikom', '', '8123456789', 'prasanthsudha@gmail.com', 'prasanth', 0),
(5, 'ddaddb', '', '', '', 690103, 'MAVELIKARA', '', '8086516007', 'dda@gmail.com', 'dda', 0),
(6, 'Robert Joseph', 'kalapura', 'cngry', 'Near kala', 675263, 'Changanasseri', '', '9995757893', 'robertjoseph@gmail.com', 'robo', 0),
(28, 'sorajS', 'ss', 'aa', 'zz', 690103, 'tt', '', '87654321098', 'ss@gmail.com', 'ss', 0),
(36, 'sa', '', '', '', 0, '', '', '1234567', 'sa@gmail.com', 'sss', 0),
(37, 'vaisakh', '', '', '', 0, '', '', '9876543210', 'vai@gmail.com', 'vaisakh', 0),
(38, 'Kannan', 'Kannan Nivas', 'Thattarambalam', 'KK Road', 690103, 'Mavelikara', '', '9876543210', 'kannan@gmail.com', 'kannan123', 0);

-- --------------------------------------------------------

--
-- Table structure for table `productt`
--

CREATE TABLE IF NOT EXISTS `productt` (
  `p_id` int(11) NOT NULL AUTO_INCREMENT,
  `categ_id` int(11) NOT NULL,
  `subcateg_id` int(11) NOT NULL,
  `brand_id` int(11) NOT NULL,
  `product` varchar(30) NOT NULL,
  `description` varchar(300) NOT NULL,
  `p_image` varchar(30) NOT NULL,
  `price` int(11) NOT NULL,
  `count` int(11) NOT NULL,
  `offer` varchar(20) NOT NULL,
  `flag` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`p_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=53 ;

--
-- Dumping data for table `productt`
--

INSERT INTO `productt` (`p_id`, `categ_id`, `subcateg_id`, `brand_id`, `product`, `description`, `p_image`, `price`, `count`, `offer`, `flag`, `client_id`, `date`) VALUES
(21, 1, 4, 1, 'Aspire', 'Its an entry level laptop,15.6'' Display,core i3 processor', 'product-10.jpg', 33000, 0, '0', 0, 0, '2016-08-25'),
(22, 1, 1, 2, 'Galaxy', 'sdf,ghjgh,jkcvbnm', 'product-16.jpg', 12000, 3, '20', 0, 0, '2016-06-21'),
(23, 1, 5, 3, 'Nikon', 'It''s an entry level camera                                                              23mp camera                               Good for local use', 'product-07.jpg', 43000, 2, '0', 0, 0, '2016-08-14'),
(24, 1, 1, 1, 'Nokia Lumia new series', 'sdf ,ghjk,hgfdsgfds', 'product-small-07.jpg', 8500, 5, '0', 1, 0, '2016-07-12'),
(26, 1, 3, 3, 'mi9', 'sdfgh', 'product-04.jpg', 25000, 0, '0', 0, 0, '2016-08-02'),
(27, 1, 1, 2, 's3', '16 GB ROM,4GB RAM ,HD display', 'product-small-03.jpg', 17000, 5, '', 0, 0, '2016-05-10'),
(28, 1, 5, 2, 'nniko', '25mp camera,HD,low price', 'product-03.jpg', 35000, 0, '', 0, 0, '2016-08-31'),
(48, 1, 1, 1, 'Micro-soft Lumia x10', '4GB RAM,16 GB internal storage,HD Display', 'product-small-08.jpg', 14000, 8, '', 0, 0, '2016-07-25'),
(51, 1, 4, 1, 'baleno', '4GB RAM,i5 processor,1TB HDD', 'product-01.jpg', 40000, 18, '', 0, 0, '2016-08-13'),
(52, 1, 4, 2, 'Sony 5.1 channel sound system', '5.1 channel,branded one ', 'product-14.jpg', 12000, 1, '', 0, 0, '2016-08-17');

-- --------------------------------------------------------

--
-- Table structure for table `subcategory`
--

CREATE TABLE IF NOT EXISTS `subcategory` (
  `subcateg_id` int(11) NOT NULL AUTO_INCREMENT,
  `sub_category` varchar(30) NOT NULL,
  `categ_id` int(11) NOT NULL,
  PRIMARY KEY (`subcateg_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `subcategory`
--

INSERT INTO `subcategory` (`subcateg_id`, `sub_category`, `categ_id`) VALUES
(1, 'Mobiles', 1),
(2, 'Tablets', 1),
(3, 'TV', 1),
(4, 'Computer', 1),
(5, 'Camera', 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
