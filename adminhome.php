<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from demo2.transvelo.in/html/media-center/index.php?page=home-2&style=alt&style=alt1&style=alt1&style=alt1&style=alt1&style=alt1&style=alt1&style=alt1&style=alt1&style=alt&style=alt&style=alt&style=alt by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 21 Apr 2016 05:04:04 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
<!-- Meta -->
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!--<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">-->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width, height=device-height,target-densitydpi=medium-dpi, user-scalable=0"/>
<meta name="description" content="">
<meta name="author" content="">
<meta name="keywords" content="MediaCenter, Template, eCommerce">
<meta name="robots" content="all">
<title>MediaCenter - Responsive eCommerce Template</title>
<!-- Bootstrap Core CSS -->
<script pagespeed_no_defer>//<![CDATA[
(function(){var d=function(b){var a=window;if(a.addEventListener)a.addEventListener("load",b,!1);else if(a.attachEvent)a.attachEvent("onload",b);else{var c=a.onload;a.onload=function(){b.call(this);c&&c.call(this)}}};window.pagespeed=window.pagespeed||{};var p=window.pagespeed,q=function(){this.a=!0};q.prototype.c=function(b){b=parseInt(b.substring(0,b.indexOf(" ")),10);return!isNaN(b)&&b<=Date.now()};q.prototype.hasExpired=q.prototype.c;q.prototype.b=function(b){return b.substring(b.indexOf(" ",b.indexOf(" ")+1)+1)};q.prototype.getData=q.prototype.b;q.prototype.d=function(b){var a=document.getElementsByTagName("script"),a=a[a.length-1];a.parentNode.replaceChild(b,a)};q.prototype.replaceLastScript=q.prototype.d;
q.prototype.e=function(b){var a=window.localStorage.getItem("pagespeed_lsc_url:"+b),c=document.createElement(a?"style":"link");a&&!this.c(a)?(c.type="text/css",c.appendChild(document.createTextNode(this.b(a)))):(c.rel="stylesheet",c.href=b,this.a=!0);this.d(c)};q.prototype.inlineCss=q.prototype.e;
q.prototype.f=function(b,a){var c=window.localStorage.getItem("pagespeed_lsc_url:"+b+" pagespeed_lsc_hash:"+a),f=document.createElement("img");c&&!this.c(c)?f.src=this.b(c):(f.src=b,this.a=!0);for(var c=2,k=arguments.length;c<k;++c){var g=arguments[c].indexOf("=");f.setAttribute(arguments[c].substring(0,g),arguments[c].substring(g+1))}this.d(f)};q.prototype.inlineImg=q.prototype.f;
var r=function(b,a,c,f){a=document.getElementsByTagName(a);for(var k=0,g=a.length;k<g;++k){var e=a[k],m=e.getAttribute("pagespeed_lsc_hash"),h=e.getAttribute("pagespeed_lsc_url");if(m&&h){h="pagespeed_lsc_url:"+h;c&&(h+=" pagespeed_lsc_hash:"+m);var l=e.getAttribute("pagespeed_lsc_expiry"),l=l?(new Date(l)).getTime():"",e=f(e);if(!e){var n=window.localStorage.getItem(h);n&&(e=b.b(n))}e&&(window.localStorage.setItem(h,l+" "+m+" "+e),b.a=!0)}}},s=function(b){r(b,"img",!0,function(a){return a.src});
r(b,"style",!1,function(a){return a.firstChild?a.firstChild.nodeValue:null})};
p.g=function(){if(window.localStorage){var b=new q;p.localStorageCache=b;d(function(){s(b)});d(function(){if(b.a){for(var a=[],c=[],f=0,k=Date.now(),g=0,e=window.localStorage.length;g<e;++g){var m=window.localStorage.key(g);if(!m.indexOf("pagespeed_lsc_url:")){var h=window.localStorage.getItem(m),l=h.indexOf(" "),n=parseInt(h.substring(0,l),10);if(!isNaN(n))if(n<=k){a.push(m);continue}else if(n<f||0==f)f=n;c.push(h.substring(l+1,h.indexOf(" ",l+1)))}}k="";f&&(k="; expires="+(new Date(f)).toUTCString());
document.cookie="_GPSLSC="+c.join("!")+k;g=0;for(e=a.length;g<e;++g)window.localStorage.removeItem(a[g]);b.a=!1}})}};p.localStorageCacheInit=p.g;})();
pagespeed.localStorageCacheInit();
//]]></script><link rel="stylesheet" href="assets/css/A.bootstrap.min.css%2bmain.css%2bgreen.css%2bowl.carousel.css%2bowl.transitions.css%2banimate.min.css%2bconfig.css%2cMcc.4OjvJhAisg.css.pagespeed.cf.8zZKtPVXrc.css"/>
<!-- Customizable CSS -->
<!-- Demo Purpose Only. Should be removed in production -->
<link href="assets/css/A.green.css.pagespeed.cf.leYubRu3Yv.css" rel="alternate stylesheet" title="Green color">
<link href="assets/css/A.blue.css.pagespeed.cf.A6--0Mc9P2.css" rel="alternate stylesheet" title="Blue color">
<link href="assets/css/A.red.css.pagespeed.cf.tz7hZueTcO.css" rel="alternate stylesheet" title="Red color">
<link href="assets/css/A.orange.css.pagespeed.cf.b0cYBJejv2.css" rel="alternate stylesheet" title="Orange color">
<link href="assets/css/A.navy.css.pagespeed.cf.N_02KAQJ6q.css" rel="alternate stylesheet" title="Navy color">
<link href="assets/css/A.dark-green.css.pagespeed.cf.FAQIvhM9Fm.css" rel="alternate stylesheet" title="Darkgreen color">
<!-- Demo Purpose Only. Should be removed in production : END -->
<!-- Fonts -->
<style pagespeed_lsc_url="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" pagespeed_lsc_hash="diV6nGfO-x" pagespeed_lsc_expiry="Thu, 21 Apr 2016 13:50:50 GMT">@font-face {
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 300;
  src: local('Open Sans Light'), local('OpenSans-Light'), url(http://fonts.gstatic.com/s/opensans/v13/DXI1ORHCpsQm3Vp6mXoaTYnF5uFdDttMLvmWuJdhhgs.ttf) format('truetype');
}
@font-face {
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 400;
  src: local('Open Sans'), local('OpenSans'), url(http://fonts.gstatic.com/s/opensans/v13/cJZKeOuBrn4kERxqtaUH3aCWcynf_cDxXwCLxiixG1c.ttf) format('truetype');
}
@font-face {
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 600;
  src: local('Open Sans Semibold'), local('OpenSans-Semibold'), url(http://fonts.gstatic.com/s/opensans/v13/MTP_ySUJH_bn48VBG8sNSonF5uFdDttMLvmWuJdhhgs.ttf) format('truetype');
}
@font-face {
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 700;
  src: local('Open Sans Bold'), local('OpenSans-Bold'), url(http://fonts.gstatic.com/s/opensans/v13/k3k702ZOKiLJc3WVjuplzInF5uFdDttMLvmWuJdhhgs.ttf) format('truetype');
}
@font-face {
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 800;
  src: local('Open Sans Extrabold'), local('OpenSans-Extrabold'), url(http://fonts.gstatic.com/s/opensans/v13/EInbV5DfGHOiMmvb1Xr-honF5uFdDttMLvmWuJdhhgs.ttf) format('truetype');
}
</style>
<style>
select {
    width: 100%;
    padding: 16px 20px;
    border: none;
    border-radius: 4px;
    background-color: #f1f1f1;
}
</style>
<!-- Icons/Glyphs -->
<link rel="stylesheet" href="assets/css/A.font-awesome.min.css.pagespeed.cf.kXDJT4wQIU.css">
<!-- Favicon -->
<link rel="shortcut icon" href="assets/images/favicon.ico">
<!-- HTML5 elements and media queries Support for IE8 : HTML5 shim and Respond.js -->
<!--[if lt IE 9]>
			<script src="assets/js/html5shiv.js"></script>
			<script src="assets/js/respond.min.js"></script>
		<![endif]-->
<script>(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','../../../www.google-analytics.com/analytics.js','ga');ga('create','UA-52598430-1','auto');ga('send','pageview');</script>
</head>
<body>
<div class="wrapper">
<!-- ============================================================= TOP NAVIGATION ============================================================= -->
<nav class="top-bar animate-dropdown">
<div class="col-xs-12 col-sm-6 no-margin">
</div>
<div class="container"> 
<div class="col-xs-12 col-sm-6 no-margin">
<ul class="right">
<li><a href="index9ba3.html?page=home">Home</a></li>
<li><a href="index53a6.html?page=contact">Contact</a></li>
<li><a href="index466d.html?page=authentication">Login</a></li>
</ul>
</div><!-- /.col -->
</div><!-- /.container -->
</nav><!-- /.top-bar -->
<!-- ============================================================= TOP NAVIGATION : END ============================================================= -->	<!-- ============================================================= HEADER ============================================================= -->
<header class="no-padding-bottom header-alt">
<div class="container no-padding">
<div class="col-xs-12 col-md-3 logo-holder">
<!-- ============================================================= LOGO ============================================================= -->
<div class="logo">
<a href="index9ba3.html?page=home">
<!--<img alt="logo" src="assets/images/logo.svg" width="233" height="54"/>-->
<!--<object id="sp" type="image/svg+xml" data="assets/images/logo.svg" width="233" height="54"></object>-->
<svg width="233px" height="54px" viewBox="0 0 233 54" version="1.1" xmlns="http://www.w3.org/2000/svg">
<path fill="#ffffff" d=" M 0.00 0.00 L 233.00 0.00 L 233.00 54.00 L 0.00 54.00 L 0.00 0.00 Z"/>
<path class="logo-svg" d=" M 104.82 6.46 C 107.74 5.13 110.66 3.80 113.58 2.46 C 113.59 18.71 113.59 34.95 113.59 51.20 C 110.65 51.20 107.71 51.20 104.78 51.20 C 104.84 50.67 104.96 49.62 105.02 49.09 C 100.02 53.56 91.66 52.69 87.37 47.67 C 84.80 44.83 83.96 40.97 83.20 37.33 C 75.63 37.37 68.05 37.26 60.47 37.40 C 61.41 39.88 62.49 42.75 65.24 43.71 C 69.03 45.31 73.10 43.58 75.89 40.91 C 77.67 42.73 79.47 44.54 81.22 46.40 C 75.60 52.47 65.66 53.95 58.77 49.23 C 53.06 45.18 51.58 37.52 52.30 30.95 C 52.75 25.29 55.84 19.51 61.29 17.27 C 66.83 15.00 73.85 15.40 78.54 19.37 C 81.58 21.92 82.87 25.85 83.50 29.64 C 84.32 24.24 87.32 18.69 92.71 16.75 C 96.83 15.07 101.64 15.89 104.93 18.89 C 104.77 14.75 104.83 10.60 104.82 6.46 Z"/>
<path class="logo-svg" d=" M 118.44 3.44 C 121.37 1.09 126.26 3.21 126.52 6.97 C 127.17 10.89 122.39 14.12 119.00 12.03 C 115.74 10.46 115.39 5.41 118.44 3.44 Z"/>
<path class="logo-svg" d=" M 10.22 19.63 C 14.71 14.58 23.51 14.56 27.42 20.34 C 33.58 13.24 48.54 14.42 50.33 24.80 C 51.41 33.54 50.55 42.41 50.82 51.20 C 47.88 51.20 44.94 51.20 42.00 51.20 C 41.92 44.09 42.14 36.97 41.93 29.87 C 41.95 27.41 40.48 24.57 37.76 24.43 C 34.66 23.72 30.88 25.51 30.74 29.01 C 30.35 36.39 30.71 43.80 30.59 51.20 C 27.67 51.20 24.75 51.20 21.82 51.20 C 21.74 44.12 21.98 37.04 21.73 29.96 C 21.79 27.24 19.97 24.22 16.95 24.37 C 13.91 23.84 10.58 25.90 10.49 29.15 C 10.13 36.49 10.47 43.85 10.35 51.20 C 7.43 51.20 4.51 51.20 1.59 51.20 C 1.59 39.69 1.59 28.18 1.59 16.67 C 4.53 16.67 7.47 16.67 10.41 16.67 C 10.36 17.41 10.27 18.89 10.22 19.63 Z"/>
<path class="logo-svg" d=" M 129.25 19.84 C 135.49 16.15 143.23 14.75 150.23 16.89 C 154.92 18.35 157.47 23.34 157.42 28.02 C 157.56 35.75 157.42 43.47 157.47 51.20 C 154.57 51.20 151.68 51.20 148.79 51.20 C 148.84 50.65 148.93 49.56 148.98 49.01 C 144.10 52.49 137.26 53.09 132.09 49.91 C 126.05 46.27 125.51 36.43 131.16 32.19 C 136.26 28.22 143.30 28.77 149.13 30.64 C 148.62 28.53 148.91 25.65 146.65 24.49 C 141.77 22.26 136.27 24.40 131.93 26.90 C 131.04 24.55 130.14 22.19 129.25 19.84 Z"/>
<path class="logo-svg" d=" M 117.06 16.67 C 119.98 16.67 122.90 16.67 125.82 16.67 C 125.82 28.18 125.82 39.69 125.82 51.20 C 122.90 51.20 119.98 51.20 117.06 51.20 C 117.06 39.69 117.06 28.18 117.06 16.67 Z"/>
<path fill="#3a3a3a" d=" M 201.86 17.62 C 202.52 17.30 203.82 16.67 204.48 16.35 C 204.48 18.10 204.48 19.84 204.48 21.59 C 205.23 21.59 206.73 21.60 207.48 21.60 C 207.48 22.17 207.48 23.30 207.48 23.86 C 206.73 23.87 205.22 23.89 204.47 23.91 C 204.49 26.42 204.34 28.95 204.62 31.45 C 205.86 32.02 207.13 32.53 208.42 32.99 C 206.71 34.03 204.25 35.64 202.51 33.72 C 201.29 30.65 202.08 27.15 201.88 23.91 C 201.39 23.89 200.42 23.87 199.94 23.86 C 199.94 23.30 199.94 22.18 199.94 21.62 C 200.43 21.60 201.41 21.57 201.90 21.56 C 201.88 20.24 201.87 18.93 201.86 17.62 Z"/>
<path fill="#3a3a3a" d=" M 169.01 34.60 C 161.80 34.48 161.85 21.38 169.03 21.30 C 171.56 20.91 173.24 22.99 174.55 24.80 C 172.38 25.03 170.35 23.99 168.21 24.05 C 165.19 25.78 165.69 32.04 169.72 32.24 C 171.04 30.86 172.68 30.26 174.58 30.81 C 173.29 32.73 171.68 35.03 169.01 34.60 Z"/>
<path fill="#3a3a3a" d=" M 176.14 24.15 C 177.71 20.91 182.63 20.34 185.06 22.88 C 186.61 24.47 186.37 26.86 186.72 28.88 C 183.63 28.96 180.54 28.94 177.46 29.07 C 178.33 30.06 178.74 31.71 180.15 32.08 C 182.15 31.99 184.30 31.15 185.94 32.84 C 183.84 33.87 181.50 35.30 179.09 34.28 C 175.09 32.89 174.46 27.52 176.14 24.15 Z"/>
<path fill="#3a3a3a" d=" M 188.27 21.67 C 191.44 22.12 195.65 20.04 197.89 23.12 C 199.40 26.61 198.50 30.58 198.71 34.28 C 197.84 34.27 196.97 34.27 196.10 34.26 C 196.04 31.26 196.36 28.21 195.84 25.24 C 195.30 22.95 190.93 23.34 191.03 25.84 C 190.71 28.63 190.93 31.46 190.90 34.27 C 190.25 34.27 188.94 34.27 188.28 34.27 C 188.28 30.07 188.28 25.87 188.27 21.67 Z"/>
<path fill="#3a3a3a" d=" M 209.34 24.33 C 210.88 20.80 216.32 20.24 218.67 23.23 C 219.85 24.84 219.70 26.96 220.06 28.84 C 216.93 28.95 213.79 28.96 210.67 29.17 C 211.42 30.17 212.14 31.20 213.03 32.08 C 214.53 32.45 215.99 31.66 217.44 31.37 C 217.81 31.81 218.56 32.68 218.93 33.11 C 216.82 34.02 214.49 35.28 212.18 34.20 C 208.39 32.73 207.86 27.62 209.34 24.33 Z"/>
<path fill="#3a3a3a" d=" M 221.57 21.68 C 224.18 22.17 229.35 19.73 229.26 23.99 C 227.69 24.37 225.10 23.31 224.48 25.41 C 223.89 28.32 224.25 31.33 224.17 34.27 C 223.52 34.27 222.23 34.27 221.58 34.27 C 221.58 30.07 221.58 25.88 221.57 21.68 Z"/>
<path fill="#ffffff" d=" M 177.47 26.72 C 178.35 25.45 179.09 23.45 180.99 23.60 C 182.85 23.48 183.54 25.46 184.45 26.68 C 182.12 26.79 179.79 26.80 177.47 26.72 Z"/>
<path fill="#ffffff" d=" M 210.63 26.63 C 211.70 25.38 212.59 23.21 214.62 23.63 C 216.40 23.58 216.80 25.59 217.66 26.73 C 215.32 26.80 212.97 26.77 210.63 26.63 Z"/>
<path fill="#ffffff" d=" M 60.57 29.87 C 61.45 26.41 64.15 23.26 68.04 23.58 C 71.91 23.17 74.23 26.62 75.30 29.84 C 70.39 29.99 65.48 29.97 60.57 29.87 Z"/>
<path fill="#ffffff" d=" M 96.52 24.70 C 99.50 23.55 102.54 25.02 104.87 26.85 C 104.80 31.61 104.80 36.38 104.86 41.14 C 102.46 43.09 99.18 44.42 96.17 43.02 C 92.82 41.46 91.99 37.36 91.96 34.01 C 91.89 30.51 92.82 26.06 96.52 24.70 Z"/>
<path fill="#ffffff" d=" M 137.58 37.63 C 141.09 35.82 145.16 36.85 148.82 37.59 C 148.82 38.98 148.80 40.38 148.79 41.78 C 145.51 43.89 141.25 45.34 137.54 43.42 C 135.23 42.33 135.28 38.72 137.58 37.63 Z"/>
<path class="logo-svg" d=" M 163.30 39.16 C 165.64 38.00 168.47 38.66 171.01 38.49 C 172.96 38.53 176.17 38.23 176.35 40.94 C 176.51 46.79 170.77 51.96 165.05 51.93 C 161.43 51.79 162.41 47.39 162.23 44.97 C 162.49 43.09 161.71 40.56 163.30 39.16 Z"/>
</svg>
</a>
</div><!-- /.logo -->
<!-- ============================================================= LOGO : END ============================================================= -->	</div><!-- /.logo-holder -->
<div class="col-xs-12 col-md-6 top-search-holder no-margin">
<div class="contact-row">
<div class="phone inline">
<i class="fa fa-phone"></i> (+800) 123 456 7890
</div>
<div class="contact inline">
<i class="fa fa-envelope"></i> contact@<span class="le-color">oursupport.com</span>
</div>
</div><!-- /.contact-row -->
<!-- ============================================================= SEARCH AREA ============================================================= -->
<div class="search-area">
<form>
<div class="control-group">
<input class="search-field" placeholder="Search for item"/>
<ul class="categories-filter animate-dropdown">
<li class="dropdown">
<a class="dropdown-toggle" data-toggle="dropdown" href="indexe96c.html?page=category-grid">all categories</a>
<ul class="dropdown-menu" role="menu">
<li role="presentation"><a role="menuitem" tabindex="-1" href="indexe96c.html?page=category-grid">laptops</a></li>
<li role="presentation"><a role="menuitem" tabindex="-1" href="indexe96c.html?page=category-grid">tv & audio</a></li>
<li role="presentation"><a role="menuitem" tabindex="-1" href="indexe96c.html?page=category-grid">gadgets</a></li>
<li role="presentation"><a role="menuitem" tabindex="-1" href="indexe96c.html?page=category-grid">cameras</a></li>
</ul>
</li>
</ul>
<a class="search-button" href="#"></a>
</div>
</form>
</div><!-- /.search-area -->
<!-- ============================================================= SEARCH AREA : END ============================================================= -->	</div><!-- /.top-search-holder -->

<!-- ============================================================= SHOPPING CART DROPDOWN : END ============================================================= -->	</div><!-- /.top-cart-row -->
</div><!-- /.container -->
<!-- ========================================= NAVIGATION ========================================= -->
<nav id="top-megamenu-nav" class="megamenu-vertical animate-dropdown">
<div class="container">
<div class="yamm navbar">
<div class="navbar-header">
<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#mc-horizontal-menu-collapse">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
</div><!-- /.navbar-header -->
<div class="collapse navbar-collapse" id="mc-horizontal-menu-collapse">
<ul class="nav navbar-nav">
<li class="dropdown"></li>
<li class="dropdown">
<a href="#" class="dropdown-toggle" data-hover="dropdown" data-toggle="dropdown">Home</a>
</li>
<li class="dropdown">
<a href="#" class="dropdown-toggle" data-hover="dropdown" data-toggle="dropdown">Product Mnagement</a>
<ul class="dropdown-menu">
<li><a href="addproduct.php">Add Product</a></li>
<li><a href="#">Remove Product</a></li>
<li><a href="#">Edit Product</a></li>
<li><a href="#">Add Product</a></li>

</ul>
</li>
<li class="dropdown">
<a href="#" class="dropdown-toggle" data-hover="dropdown" data-toggle="dropdown">Manage Clients</a>
</li>
<li class="dropdown">
<a href="#" class="dropdown-toggle" data-hover="dropdown" data-toggle="dropdown">Manage ADS</a>
</li>
<li class="dropdown">
<a href="#" class="dropdown-toggle" data-hover="dropdown" data-toggle="dropdown">Transaction Details</a>
</li>
<li class="dropdown">
<a href="settings.php" >Settings</a>
</li>
<li class="dropdown yamm-fw">
<a href="#" class="dropdown-toggle" data-hover="dropdown" data-toggle="dropdown">Logout</a>
</li>
</ul><!-- /.navbar-nav -->
</div><!-- /.navbar-collapse -->
</div><!-- /.navbar -->
</div><!-- /.container -->
</nav><!-- /.megamenu-vertical -->
<!-- ========================================= NAVIGATION : END ========================================= -->
</header>
<!-- ============================================================= HEADER : END ============================================================= -->	<div id="top-banner-and-menu" class="homepage2">
<div class="container">
<div class="col-xs-12">
<!-- ========================================== SECTION – HERO ========================================= -->
<div id="hero">
<div id="owl-main" class="owl-carousel height-lg owl-inner-nav owl-ui-lg">
<div class="item" style="background-image:url(assets/images/sliders/xslider02.jpg.pagespeed.ic.OP-4PLdeFm.jpg)">
<div class="container-fluid">
<div class="caption vertical-center text-left right" style="padding-right:0;">
<div class="big-text fadeInDown-1">
Save up to a<span class="big"><span class="sign">$</span>400</span>
</div>
<div class="excerpt fadeInDown-2">
on selected laptops<br>
&amp; desktop pcs or<br>
smartphones
</div>
<div class="small fadeInDown-2">
terms and conditions apply
</div>
<div class="button-holder fadeInDown-3">
<a href="index6c11.html?page=single-product" class="big le-button ">shop now</a>
</div>
</div><!-- /.caption -->
</div><!-- /.container-fluid -->
</div><!-- /.item -->
<div class="item" style="background-image:url(assets/images/sliders/xslider04.jpg.pagespeed.ic.Q3L7zYv52h.jpg)">
<div class="container-fluid">
<div class="caption vertical-center text-left left" style="padding-left:7%;">
<div class="big-text fadeInDown-1">
Want a<span class="big"><span class="sign">$</span>200</span>Discount?
</div>
<div class="excerpt fadeInDown-2">
on desktop pcs
</div>
<div class="small fadeInDown-2">
terms and conditions apply
</div>
<div class="button-holder fadeInDown-3">
<a href="index6c11.html?page=single-product" class="big le-button ">shop now</a>
</div>
</div><!-- /.caption -->
</div><!-- /.container-fluid -->
</div><!-- /.item -->
</div><!-- /.owl-carousel -->
</div>
<!-- ========================================= SECTION – HERO : END ========================================= -->	</div>
</div>
</div><!-- /.homepage2 -->
<!-- ========================================= HOME BANNERS ========================================= -->
<section id="banner-holder" class="wow fadeInUp">
<div class="container">
<div class="row">
<div class="col-xs-12 col-md-6 col-sm-6 col-lg-6 banner">
<a href="index7bbc.html?page=category-grid-2">
<div class="banner-text theblue">
<h1>New Life</h1>
<span class="tagline">Introducing New Category</span>
</div>
<img class="banner-image img-responsive" alt="" src= "assets/images/banners/banner-narrow-01.jpg"/>
</a>
</div>
<div class="col-xs-12 col-md-6 col-sm-6 col-lg-6 text-right banner">
<a href="index7bbc.html?page=category-grid-2">
<div class="banner-text right">
<h1>Time &amp; Style</h1>
<span class="tagline">Checkout new arrivals</span>
</div>
<img class="banner-image img-responsive" alt="" src="assets/images/banners/banner-narrow-02.jpg"/>
</a>
</div>
</div>
</div><!-- /.container -->
</section><!-- /#banner-holder -->
<!-- ========================================= HOME BANNERS : END ========================================= -->
<div id="products-tab" class="wow fadeInUp">
<div class="container">
<div class="tab-holder">
<!-- Nav tabs -->
<ul class="nav nav-tabs">
<li class="active"><a href="#featured" data-toggle="tab">featured</a></li>
<li><a href="#new-arrivals" data-toggle="tab">new arrivals</a></li>
<li><a href="#top-sales" data-toggle="tab">top sales</a></li>
</ul>
<!-- Tab panes -->
<div class="tab-content">
<div class="tab-pane active" id="featured">
<div class="product-grid-holder">
<div class="col-sm-4 col-md-3  no-margin product-item-holder hover">
<div class="product-item">
<div class="ribbon red"><span>sale</span></div>
<div class="image">
<img alt="" src="assets/images/products/product-01.jpg"/>
</div>
<div class="body">
<div class="label-discount green">-50% sale</div>
<div class="title">
<a href="index6c11.html?page=single-product">VAIO Fit Laptop - Windows 8 SVF14322CXW</a>
</div>
<div class="brand">sony</div>
</div>
<div class="prices">
<div class="price-prev">$1399.00</div>
<div class="price-current pull-right">$1199.00</div>
</div>
<div class="hover-area">
<div class="add-cart-button">
<a href="index6c11.html?page=single-product" class="le-button">add to cart</a>
</div>
<div class="wish-compare">
<a class="btn-add-to-wishlist" href="#">add to wishlist</a>
<a class="btn-add-to-compare" href="#">compare</a>
</div>
</div>
</div>
</div>
<div class="col-sm-4 col-md-3 no-margin product-item-holder hover">
<div class="product-item">
<div class="ribbon blue"><span>new!</span></div>
<div class="image">
<img alt="" src="assets/images/products/product-02.jpg"/>
</div>
<div class="body">
<div class="label-discount clear"></div>
<div class="title">
<a href="index6c11.html?page=single-product">White lumia 9001</a>
</div>
<div class="brand">nokia</div>
</div>
<div class="prices">
<div class="price-prev">$1399.00</div>
<div class="price-current pull-right">$1199.00</div>
</div>
<div class="hover-area">
<div class="add-cart-button">
<a href="index6c11.html?page=single-product" class="le-button">add to cart</a>
</div>
<div class="wish-compare">
<a class="btn-add-to-wishlist" href="#">add to wishlist</a>
<a class="btn-add-to-compare" href="#">compare</a>
</div>
</div>
</div>
</div>
<div class="col-sm-4 col-md-3 no-margin product-item-holder hover">
<div class="product-item">
<div class="image">
<img alt="" src="assets/images/products/product-03.jpg"/>
</div>
<div class="body">
<div class="label-discount clear"></div>
<div class="title">
<a href="index6c11.html?page=single-product">POV Action Cam</a>
</div>
<div class="brand">sony</div>
</div>
<div class="prices">
<div class="price-prev">$1399.00</div>
<div class="price-current pull-right">$1199.00</div>
</div>
<div class="hover-area">
<div class="add-cart-button">
<a href="index6c11.html?page=single-product" class="le-button">add to cart</a>
</div>
<div class="wish-compare">
<a class="btn-add-to-wishlist" href="#">add to wishlist</a>
<a class="btn-add-to-compare" href="#">compare</a>
</div>
</div>
</div>
</div>
<div class="col-sm-4 col-md-3 no-margin product-item-holder hover">
<div class="product-item">
<div class="ribbon red"><span>sale</span></div>
<div class="ribbon green"><span>bestseller</span></div>
<div class="image">
<img alt="" src="assets/images/products/product-04.jpg"/>
</div>
<div class="body">
<div class="label-discount clear"></div>
<div class="title">
<a href="index6c11.html?page=single-product">Netbook Acer TravelMate
B113-E-10072</a>
</div>
<div class="brand">acer</div>
</div>
<div class="prices">
<div class="price-prev">$1399.00</div>
<div class="price-current pull-right">$1199.00</div>
</div>
<div class="hover-area">
<div class="add-cart-button">
<a href="index6c11.html?page=single-product" class="le-button">add to cart</a>
</div>
<div class="wish-compare">
<a class="btn-add-to-wishlist" href="#">add to wishlist</a>
<a class="btn-add-to-compare" href="#">compare</a>
</div>
</div>
</div>
</div>
</div>
<div class="loadmore-holder text-center">
<a class="btn-loadmore" href="#">
<i class="fa fa-plus"></i>
load more products</a>
</div>
</div>
<div class="tab-pane" id="new-arrivals">
<div class="product-grid-holder">
<div class="col-sm-4 col-md-3 no-margin product-item-holder hover">
<div class="product-item">
<div class="ribbon blue"><span>new!</span></div>
<div class="image">
<img alt="" src="assets/images/products/product-02.jpg"/>
</div>
<div class="body">
<div class="label-discount clear"></div>
<div class="title">
<a href="index6c11.html?page=single-product">White lumia 9001</a>
</div>
<div class="brand">nokia</div>
</div>
<div class="prices">
<div class="price-prev">$1399.00</div>
<div class="price-current pull-right">$1199.00</div>
</div>
<div class="hover-area">
<div class="add-cart-button">
<a href="index6c11.html?page=single-product" class="le-button">add to cart</a>
</div>
<div class="wish-compare">
<a class="btn-add-to-wishlist" href="#">add to wishlist</a>
<a class="btn-add-to-compare" href="#">compare</a>
</div>
</div>
</div>
</div>
<div class="col-sm-4 col-md-3 no-margin product-item-holder hover">
<div class="product-item">
<div class="ribbon red"><span>sale</span></div>
<div class="image">
<img alt="" src="assets/images/products/product-01.jpg"/>
</div>
<div class="body">
<div class="label-discount green">-50% sale</div>
<div class="title">
<a href="index6c11.html?page=single-product">VAIO Fit Laptop - Windows 8 SVF14322CXW</a>
</div>
<div class="brand">sony</div>
</div>
<div class="prices">
<div class="price-prev">$1399.00</div>
<div class="price-current pull-right">$1199.00</div>
</div>
<div class="hover-area">
<div class="add-cart-button">
<a href="index6c11.html?page=single-product" class="le-button">add to cart</a>
</div>
<div class="wish-compare">
<a class="btn-add-to-wishlist" href="#">add to wishlist</a>
<a class="btn-add-to-compare" href="#">compare</a>
</div>
</div>
</div>
</div>
<div class="col-sm-4 col-md-3 no-margin product-item-holder hover">
<div class="product-item">
<div class="ribbon red"><span>sale</span></div>
<div class="ribbon green"><span>bestseller</span></div>
<div class="image">
<img alt="" src="assets/images/products/product-04.jpg"/>
</div>
<div class="body">
<div class="label-discount clear"></div>
<div class="title">
<a href="index6c11.html?page=single-product">Netbook Acer TravelMate
B113-E-10072</a>
</div>
<div class="brand">acer</div>
</div>
<div class="prices">
<div class="price-prev">$1399.00</div>
<div class="price-current pull-right">$1199.00</div>
</div>
<div class="hover-area">
<div class="add-cart-button">
<a href="index6c11.html?page=single-product" class="le-button">add to cart</a>
</div>
<div class="wish-compare">
<a class="btn-add-to-wishlist" href="#">add to wishlist</a>
<a class="btn-add-to-compare" href="#">compare</a>
</div>
</div>
</div>
</div>
<div class="col-sm-4 col-md-3 no-margin product-item-holder hover">
<div class="product-item">
<div class="image">
<img alt="" src="assets/images/products/product-03.jpg"/>
</div>
<div class="body">
<div class="label-discount clear"></div>
<div class="title">
<a href="index6c11.html?page=single-product">POV Action Cam</a>
</div>
<div class="brand">sony</div>
</div>
<div class="prices">
<div class="price-prev">$1399.00</div>
<div class="price-current pull-right">$1199.00</div>
</div>
<div class="hover-area">
<div class="add-cart-button">
<a href="index6c11.html?page=single-product" class="le-button">add to cart</a>
</div>
<div class="wish-compare">
<a class="btn-add-to-wishlist" href="#">add to wishlist</a>
<a class="btn-add-to-compare" href="#">compare</a>
</div>
</div>
</div>
</div>
</div>
<div class="loadmore-holder text-center">
<a class="btn-loadmore" href="#">
<i class="fa fa-plus"></i>
load more products</a>
</div>
</div>
<div class="tab-pane" id="top-sales">
<div class="product-grid-holder">
<div class="col-sm-4 col-md-3 no-margin product-item-holder hover">
<div class="product-item">
<div class="ribbon red"><span>sale</span></div>
<div class="ribbon green"><span>bestseller</span></div>
<div class="image">
<img alt="" src="assets/images/products/product-04.jpg"/>
</div>
<div class="body">
<div class="label-discount clear"></div>
<div class="title">
<a href="index6c11.html?page=single-product">Netbook Acer TravelMate
B113-E-10072</a>
</div>
<div class="brand">acer</div>
</div>
<div class="prices">
<div class="price-prev">$1399.00</div>
<div class="price-current pull-right">$1199.00</div>
</div>
<div class="hover-area">
<div class="add-cart-button">
<a href="index6c11.html?page=single-product" class="le-button">add to cart</a>
</div>
<div class="wish-compare">
<a class="btn-add-to-wishlist" href="#">add to wishlist</a>
<a class="btn-add-to-compare" href="#">compare</a>
</div>
</div>
</div>
</div>
<div class="col-sm-4 col-md-3 no-margin product-item-holder hover">
<div class="product-item">
<div class="image">
<img alt="" src="assets/images/products/product-03.jpg"/>
</div>
<div class="body">
<div class="label-discount clear"></div>
<div class="title">
<a href="index6c11.html?page=single-product">POV Action Cam</a>
</div>
<div class="brand">sony</div>
</div>
<div class="prices">
<div class="price-prev">$1399.00</div>
<div class="price-current pull-right">$1199.00</div>
</div>
<div class="hover-area">
<div class="add-cart-button">
<a href="index6c11.html?page=single-product" class="le-button">add to cart</a>
</div>
<div class="wish-compare">
<a class="btn-add-to-wishlist" href="#">add to wishlist</a>
<a class="btn-add-to-compare" href="#">compare</a>
</div>
</div>
</div>
</div>
<div class="col-sm-4 col-md-3 no-margin product-item-holder hover">
<div class="product-item">
<div class="ribbon blue"><span>new!</span></div>
<div class="image">
<img alt="" src="assets/images/products/product-02.jpg"/>
</div>
<div class="body">
<div class="label-discount clear"></div>
<div class="title">
<a href="index6c11.html?page=single-product">White lumia 9001</a>
</div>
<div class="brand">nokia</div>
</div>
<div class="prices">
<div class="price-prev">$1399.00</div>
<div class="price-current pull-right">$1199.00</div>
</div>
<div class="hover-area">
<div class="add-cart-button">
<a href="index6c11.html?page=single-product" class="le-button">add to cart</a>
</div>
<div class="wish-compare">
<a class="btn-add-to-wishlist" href="#">add to wishlist</a>
<a class="btn-add-to-compare" href="#">compare</a>
</div>
</div>
</div>
</div>
<div class="col-sm-4 col-md-3 no-margin product-item-holder hover">
<div class="product-item">
<div class="ribbon red"><span>sale</span></div>
<div class="image">
<img alt="" src="assets/images/products/product-01.jpg"/>
</div>
<div class="body">
<div class="label-discount green">-50% sale</div>
<div class="title">
<a href="index6c11.html?page=single-product">VAIO Fit Laptop - Windows 8 SVF14322CXW</a>
</div>
<div class="brand">sony</div>
</div>
<div class="prices">
<div class="price-prev">$1399.00</div>
<div class="price-current pull-right">$1199.00</div>
</div>
<div class="hover-area">
<div class="add-cart-button">
<a href="index6c11.html?page=single-product" class="le-button">add to cart</a>
</div>
<div class="wish-compare">
<a class="btn-add-to-wishlist" href="#">add to wishlist</a>
<a class="btn-add-to-compare" href="#">compare</a>
</div>
</div>
</div>
</div>
</div>
<div class="loadmore-holder text-center">
<a class="btn-loadmore" href="#">
<i class="fa fa-plus"></i>
load more products</a>
</div>
</div>
</div>
</div>
</div>
</div>
<!-- ========================================= BEST SELLERS ========================================= -->
<section id="bestsellers" class="color-bg wow fadeInUp">
<div class="container">
<h1 class="section-title">Best Sellers</h1>
<div class="product-grid-holder medium">
<div class="col-xs-12 col-md-7 no-margin">
<div class="row no-margin">
<div class="col-xs-12 col-sm-4 no-margin product-item-holder size-medium hover">
<div class="product-item">
<div class="image">
<img alt="" src="assets/images/blank.gif.pagespeed.ce.2JdGiI2i2V.gif" data-echo="assets/images/products/product-05.jpg"/>
</div>
<div class="body">
<div class="label-discount clear"></div>
<div class="title">
<a href="index6c11.html?page=single-product">beats studio headphones official one</a>
</div>
<div class="brand">beats</div>
</div>
<div class="prices">
<div class="price-current text-right">$1199.00</div>
</div>
<div class="hover-area">
<div class="add-cart-button">
<a href="index6c11.html?page=single-product" class="le-button">Add to cart</a>
</div>
<div class="wish-compare">
<a class="btn-add-to-wishlist" href="#">Add to Wishlist</a>
<a class="btn-add-to-compare" href="#">Compare</a>
</div>
</div>
</div>
</div><!-- /.product-item-holder -->
<div class="col-xs-12 col-sm-4 no-margin product-item-holder size-medium hover">
<div class="product-item">
<div class="image">
<img alt="" src="assets/images/blank.gif.pagespeed.ce.2JdGiI2i2V.gif" data-echo="assets/images/products/product-06.jpg"/>
</div>
<div class="body">
<div class="label-discount clear"></div>
<div class="title">
<a href="index6c11.html?page=single-product">playstasion 4 with four games and pad</a>
</div>
<div class="brand">acer</div>
</div>
<div class="prices">
<div class="price-current text-right">$1199.00</div>
</div>
<div class="hover-area">
<div class="add-cart-button">
<a href="index6c11.html?page=single-product" class="le-button">Add to cart</a>
</div>
<div class="wish-compare">
<a class="btn-add-to-wishlist" href="#">Add to Wishlist</a>
<a class="btn-add-to-compare" href="#">Compare</a>
</div>
</div>
</div>
</div><!-- /.product-item-holder -->
<div class="col-xs-12 col-sm-4 no-margin product-item-holder size-medium hover">
<div class="product-item">
<div class="image">
<img alt="" src="assets/images/blank.gif.pagespeed.ce.2JdGiI2i2V.gif" data-echo="assets/images/products/product-07.jpg"/>
</div>
<div class="body">
<div class="label-discount clear"></div>
<div class="title">
<a href="index6c11.html?page=single-product">EOS rebel t5i DSLR Camera with 18-55mm IS STM lens</a>
</div>
<div class="brand">canon</div>
</div>
<div class="prices">
<div class="price-current text-right">$1199.00</div>
</div>
<div class="hover-area">
<div class="add-cart-button">
<a href="index6c11.html?page=single-product" class="le-button">Add to cart</a>
</div>
<div class="wish-compare">
<a class="btn-add-to-wishlist" href="#">Add to Wishlist</a>
<a class="btn-add-to-compare" href="#">Compare</a>
</div>
</div>
</div>
</div><!-- /.product-item-holder -->
</div><!-- /.row -->
<div class="row no-margin">
<div class="col-xs-12 col-sm-4 no-margin product-item-holder size-medium hover">
<div class="product-item">
<div class="image">
<img alt="" src="assets/images/blank.gif.pagespeed.ce.2JdGiI2i2V.gif" data-echo="assets/images/products/product-08.jpg"/>
</div>
<div class="body">
<div class="label-discount clear"></div>
<div class="title">
<a href="index6c11.html?page=single-product">fitbit zip wireless activity tracker - lime</a>
</div>
<div class="brand">fitbit zip</div>
</div>
<div class="prices">
<div class="price-current text-right">$1199.00</div>
</div>
<div class="hover-area">
<div class="add-cart-button">
<a href="index6c11.html?page=single-product" class="le-button">Add to cart</a>
</div>
<div class="wish-compare">
<a class="btn-add-to-wishlist" href="#">Add to Wishlist</a>
<a class="btn-add-to-compare" href="#">Compare</a>
</div>
</div>
</div>
</div><!-- /.product-item-holder -->
<div class="col-xs-12 col-sm-4 no-margin product-item-holder size-medium hover">
<div class="product-item">
<div class="image">
<img alt="" src="assets/images/blank.gif.pagespeed.ce.2JdGiI2i2V.gif" data-echo="assets/images/products/product-09.jpg"/>
</div>
<div class="body">
<div class="label-discount clear"></div>
<div class="title">
<a href="index6c11.html?page=single-product">PowerShot elph 115 16MP digital camera</a>
</div>
<div class="brand">canon</div>
</div>
<div class="prices">
<div class="price-current text-right">$1199.00</div>
</div>
<div class="hover-area">
<div class="add-cart-button">
<a href="index6c11.html?page=single-product" class="le-button">Add to cart</a>
</div>
<div class="wish-compare">
<a class="btn-add-to-wishlist" href="#">Add to Wishlist</a>
<a class="btn-add-to-compare" href="#">Compare</a>
</div>
</div>
</div>
</div><!-- /.product-item-holder -->
<div class="col-xs-12 col-sm-4 no-margin product-item-holder size-medium hover">
<div class="product-item">
<div class="image">
<img alt="" src="assets/images/blank.gif.pagespeed.ce.2JdGiI2i2V.gif" data-echo="assets/images/products/product-10.jpg"/>
</div>
<div class="body">
<div class="label-discount clear"></div>
<div class="title">
<a href="index6c11.html?page=single-product">netbook acer travelMate b113-E-10072</a>
</div>
<div class="brand">acer</div>
</div>
<div class="prices">
<div class="price-current text-right">$1199.00</div>
</div>
<div class="hover-area">
<div class="add-cart-button">
<a href="index6c11.html?page=single-product" class="le-button">Add to cart</a>
</div>
<div class="wish-compare">
<a class="btn-add-to-wishlist" href="#">Add to Wishlist</a>
<a class="btn-add-to-compare" href="#">Compare</a>
</div>
</div>
</div>
</div><!-- /.product-item-holder -->
</div><!-- /.row -->
</div><!-- /.col -->
<div class="col-xs-12 col-md-5 no-margin">
<div class="product-item-holder size-big single-product-gallery small-gallery">
<div id="best-seller-single-product-slider" class="single-product-slider owl-carousel">
<div class="single-product-gallery-item" id="slide1">
<a data-rel="prettyphoto" href="images/products/product-gallery-01.html">
<img alt="" src="assets/images/blank.gif.pagespeed.ce.2JdGiI2i2V.gif" data-echo="assets/images/products/product-gallery-01.jpg"/>
</a>
</div><!-- /.single-product-gallery-item -->
<div class="single-product-gallery-item" id="slide2">
<a data-rel="prettyphoto" href="images/products/product-gallery-01.html">
<img alt="" src="assets/images/blank.gif.pagespeed.ce.2JdGiI2i2V.gif" data-echo="assets/images/products/product-gallery-01.jpg"/>
</a>
</div><!-- /.single-product-gallery-item -->
<div class="single-product-gallery-item" id="slide3">
<a data-rel="prettyphoto" href="images/products/product-gallery-01.html">
<img alt="" src="assets/images/blank.gif.pagespeed.ce.2JdGiI2i2V.gif" data-echo="assets/images/products/product-gallery-01.jpg"/>
</a>
</div><!-- /.single-product-gallery-item -->
</div><!-- /.single-product-slider -->
<div class="gallery-thumbs clearfix">
<ul>
<li><a class="horizontal-thumb active" data-target="#best-seller-single-product-slider" data-slide="0" href="#slide1"><img alt="" src="assets/images/blank.gif.pagespeed.ce.2JdGiI2i2V.gif" data-echo="assets/images/products/gallery-thumb-01.jpg"/></a></li>
<li><a class="horizontal-thumb" data-target="#best-seller-single-product-slider" data-slide="1" href="#slide2"><img alt="" src="assets/images/blank.gif.pagespeed.ce.2JdGiI2i2V.gif" data-echo="assets/images/products/gallery-thumb-01.jpg"/></a></li>
<li><a class="horizontal-thumb" data-target="#best-seller-single-product-slider" data-slide="2" href="#slide3"><img alt="" src="assets/images/blank.gif.pagespeed.ce.2JdGiI2i2V.gif" data-echo="assets/images/products/gallery-thumb-01.jpg"/></a></li>
</ul>
</div><!-- /.gallery-thumbs -->
<div class="body">
<div class="label-discount clear"></div>
<div class="title">
<a href="index6c11.html?page=single-product">CPU intel core i5-4670k 3.4GHz BOX B82-12-122-41</a>
</div>
<div class="brand">sony</div>
</div>
<div class="prices text-right">
<div class="price-current inline">$1199.00</div>
<a href="cart.html" class="le-button big inline">add to cart</a>
</div>
</div><!-- /.product-item-holder -->
</div><!-- /.col -->
</div><!-- /.product-grid-holder -->
</div><!-- /.container -->
</section><!-- /#bestsellers -->
<!-- ========================================= BEST SELLERS : END ========================================= -->
<!-- ========================================= RECENTLY VIEWED ========================================= -->
<section id="recently-reviewd" class="wow fadeInUp">
<div class="container">
<div class="carousel-holder hover">
<div class="title-nav">
<h2 class="h1">Recently Viewed</h2>
<div class="nav-holder">
<a href="#prev" data-target="#owl-recently-viewed" class="slider-prev btn-prev fa fa-angle-left"></a>
<a href="#next" data-target="#owl-recently-viewed" class="slider-next btn-next fa fa-angle-right"></a>
</div>
</div><!-- /.title-nav -->
<div id="owl-recently-viewed" class="owl-carousel product-grid-holder">
<div class="no-margin carousel-item product-item-holder size-small hover">
<div class="product-item">
<div class="ribbon red"><span>sale</span></div>
<div class="image">
<img alt="" src="assets/images/blank.gif.pagespeed.ce.2JdGiI2i2V.gif" data-echo="assets/images/products/product-11.jpg"/>
</div>
<div class="body">
<div class="title">
<a href="index6c11.html?page=single-product">LC-70UD1U 70" class aquos 4K ultra HD</a>
</div>
<div class="brand">Sharp</div>
</div>
<div class="prices">
<div class="price-current text-right">$1199.00</div>
</div>
<div class="hover-area">
<div class="add-cart-button">
<a href="index6c11.html?page=single-product" class="le-button">Add to Cart</a>
</div>
<div class="wish-compare">
<a class="btn-add-to-wishlist" href="#">Add to Wishlist</a>
<a class="btn-add-to-compare" href="#">Compare</a>
</div>
</div>
</div><!-- /.product-item -->
</div><!-- /.product-item-holder -->
<div class="no-margin carousel-item product-item-holder size-small hover">
<div class="product-item">
<div class="ribbon blue"><span>new!</span></div>
<div class="image">
<img alt="" src="assets/images/blank.gif.pagespeed.ce.2JdGiI2i2V.gif" data-echo="assets/images/products/product-12.jpg"/>
</div>
<div class="body">
<div class="title">
<a href="index6c11.html?page=single-product">cinemizer OLED 3D virtual reality TV Video</a>
</div>
<div class="brand">zeiss</div>
</div>
<div class="prices">
<div class="price-current text-right">$1199.00</div>
</div>
<div class="hover-area">
<div class="add-cart-button">
<a href="index6c11.html?page=single-product" class="le-button">Add to cart</a>
</div>
<div class="wish-compare">
<a class="btn-add-to-wishlist" href="#">Add to Wishlist</a>
<a class="btn-add-to-compare" href="#">Compare</a>
</div>
</div>
</div><!-- /.product-item -->
</div><!-- /.product-item-holder -->
<div class=" no-margin carousel-item product-item-holder size-small hover">
<div class="product-item">
<div class="image">
<img alt="" src="assets/images/blank.gif.pagespeed.ce.2JdGiI2i2V.gif" data-echo="assets/images/products/product-13.jpg"/>
</div>
<div class="body">
<div class="title">
<a href="index6c11.html?page=single-product">s2340T23" full HD multi-Touch Monitor</a>
</div>
<div class="brand">dell</div>
</div>
<div class="prices">
<div class="price-current text-right">$1199.00</div>
</div>
<div class="hover-area">
<div class="add-cart-button">
<a href="index6c11.html?page=single-product" class="le-button">Add to cart</a>
</div>
<div class="wish-compare">
<a class="btn-add-to-wishlist" href="#">Add to Wishlist</a>
<a class="btn-add-to-compare" href="#">Compare</a>
</div>
</div>
</div><!-- /.product-item -->
</div><!-- /.product-item-holder -->
<div class=" no-margin carousel-item product-item-holder size-small hover">
<div class="product-item">
<div class="ribbon blue"><span>new!</span></div>
<div class="image">
<img alt="" src="assets/images/blank.gif.pagespeed.ce.2JdGiI2i2V.gif" data-echo="assets/images/products/product-14.jpg"/>
</div>
<div class="body">
<div class="title">
<a href="index6c11.html?page=single-product">kardon BDS 7772/120 integrated 3D</a>
</div>
<div class="brand">harman</div>
</div>
<div class="prices">
<div class="price-current text-right">$1199.00</div>
</div>
<div class="hover-area">
<div class="add-cart-button">
<a href="index6c11.html?page=single-product" class="le-button">Add to cart</a>
</div>
<div class="wish-compare">
<a class="btn-add-to-wishlist" href="#">Add to Wishlist</a>
<a class="btn-add-to-compare" href="#">Compare</a>
</div>
</div>
</div><!-- /.product-item -->
</div><!-- /.product-item-holder -->
<div class=" no-margin carousel-item product-item-holder size-small hover">
<div class="product-item">
<div class="ribbon green"><span>bestseller</span></div>
<div class="image">
<img alt="" src="assets/images/blank.gif.pagespeed.ce.2JdGiI2i2V.gif" data-echo="assets/images/products/product-15.jpg"/>
</div>
<div class="body">
<div class="title">
<a href="index6c11.html?page=single-product">netbook acer travel B113-E-10072</a>
</div>
<div class="brand">acer</div>
</div>
<div class="prices">
<div class="price-current text-right">$1199.00</div>
</div>
<div class="hover-area">
<div class="add-cart-button">
<a href="index6c11.html?page=single-product" class="le-button">Add to cart</a>
</div>
<div class="wish-compare">
<a class="btn-add-to-wishlist" href="#">Add to Wishlist</a>
<a class="btn-add-to-compare" href="#">Compare</a>
</div>
</div>
</div><!-- /.product-item -->
</div><!-- /.product-item-holder -->
<div class=" no-margin carousel-item product-item-holder size-small hover">
<div class="product-item">
<div class="image">
<img alt="" src="assets/images/blank.gif.pagespeed.ce.2JdGiI2i2V.gif" data-echo="assets/images/products/product-16.jpg"/>
</div>
<div class="body">
<div class="title">
<a href="index6c11.html?page=single-product">iPod touch 5th generation,64GB, blue</a>
</div>
<div class="brand">apple</div>
</div>
<div class="prices">
<div class="price-current text-right">$1199.00</div>
</div>
<div class="hover-area">
<div class="add-cart-button">
<a href="index6c11.html?page=single-product" class="le-button">Add to cart</a>
</div>
<div class="wish-compare">
<a class="btn-add-to-wishlist" href="#">Add to Wishlist</a>
<a class="btn-add-to-compare" href="#">Compare</a>
</div>
</div>
</div><!-- /.product-item -->
</div><!-- /.product-item-holder -->
<div class=" no-margin carousel-item product-item-holder size-small hover">
<div class="product-item">
<div class="image">
<img alt="" src="assets/images/blank.gif.pagespeed.ce.2JdGiI2i2V.gif" data-echo="assets/images/products/product-13.jpg"/>
</div>
<div class="body">
<div class="title">
<a href="index6c11.html?page=single-product">s2340T23" full HD multi-Touch Monitor</a>
</div>
<div class="brand">dell</div>
</div>
<div class="prices">
<div class="price-current text-right">$1199.00</div>
</div>
<div class="hover-area">
<div class="add-cart-button">
<a href="index6c11.html?page=single-product" class="le-button">Add to cart</a>
</div>
<div class="wish-compare">
<a class="btn-add-to-wishlist" href="#">Add to Wishlist</a>
<a class="btn-add-to-compare" href="#">Compare</a>
</div>
</div>
</div><!-- /.product-item -->
</div><!-- /.product-item-holder -->
<div class=" no-margin carousel-item product-item-holder size-small hover">
<div class="product-item">
<div class="ribbon blue"><span>new!</span></div>
<div class="image">
<img alt="" src="assets/images/blank.gif.pagespeed.ce.2JdGiI2i2V.gif" data-echo="assets/images/products/product-14.jpg"/>
</div>
<div class="body">
<div class="title">
<a href="index6c11.html?page=single-product">kardon BDS 7772/120 integrated 3D</a>
</div>
<div class="brand">harman</div>
</div>
<div class="prices">
<div class="price-current text-right">$1199.00</div>
</div>
<div class="hover-area">
<div class="add-cart-button">
<a href="index6c11.html?page=single-product" class="le-button">Add to cart</a>
</div>
<div class="wish-compare">
<a class="btn-add-to-wishlist" href="#">Add to Wishlist</a>
<a class="btn-add-to-compare" href="#">Compare</a>
</div>
</div>
</div><!-- /.product-item -->
</div><!-- /.product-item-holder -->
</div><!-- /#recently-carousel -->
</div><!-- /.carousel-holder -->
</div><!-- /.container -->
</section><!-- /#recently-reviewd -->
<!-- ========================================= RECENTLY VIEWED : END ========================================= -->
<!-- ========================================= TOP BRANDS ========================================= -->
<section id="top-brands" class="wow fadeInUp">
<div class="container">
<div class="carousel-holder">
<div class="title-nav">
<h1>Top Brands</h1>
<div class="nav-holder">
<a href="#prev" data-target="#owl-brands" class="slider-prev btn-prev fa fa-angle-left"></a>
<a href="#next" data-target="#owl-brands" class="slider-next btn-next fa fa-angle-right"></a>
</div>
</div><!-- /.title-nav -->
<div id="owl-brands" class="owl-carousel brands-carousel">
<div class="carousel-item">
<a href="#">
<img alt="" src="assets/images/brands/xbrand-01.jpg.pagespeed.ic.kIH08WkO6X.jpg"/>
</a>
</div><!-- /.carousel-item -->
<div class="carousel-item">
<a href="#">
<img alt="" src="assets/images/brands/xbrand-02.jpg.pagespeed.ic.BIRo28nbI_.jpg"/>
</a>
</div><!-- /.carousel-item -->
<div class="carousel-item">
<a href="#">
<img alt="" src="assets/images/brands/xbrand-03.jpg.pagespeed.ic.hIhTU044uJ.jpg"/>
</a>
</div><!-- /.carousel-item -->
<div class="carousel-item">
<a href="#">
<img alt="" src="assets/images/brands/xbrand-04.jpg.pagespeed.ic.yC5XGevAqN.jpg"/>
</a>
</div><!-- /.carousel-item -->
<div class="carousel-item">
<a href="#">
<img alt="" src="assets/images/brands/xbrand-01.jpg.pagespeed.ic.kIH08WkO6X.jpg"/>
</a>
</div><!-- /.carousel-item -->
<div class="carousel-item">
<a href="#">
<img alt="" src="assets/images/brands/xbrand-02.jpg.pagespeed.ic.BIRo28nbI_.jpg"/>
</a>
</div><!-- /.carousel-item -->
<div class="carousel-item">
<a href="#">
<img alt="" src="assets/images/brands/xbrand-03.jpg.pagespeed.ic.hIhTU044uJ.jpg"/>
</a>
</div><!-- /.carousel-item -->
<div class="carousel-item">
<a href="#">
<img alt="" src="assets/images/brands/xbrand-04.jpg.pagespeed.ic.yC5XGevAqN.jpg"/>
</a>
</div><!-- /.carousel-item -->
</div><!-- /.brands-caresoul -->
</div><!-- /.carousel-holder -->
</div><!-- /.container -->
</section><!-- /#top-brands -->
<!-- ========================================= TOP BRANDS : END ========================================= -->	<!-- ============================================================= FOOTER ============================================================= -->
<footer id="footer" class="color-bg">
<div class="container">
<div class="row no-margin widgets-row">
<div class="col-xs-12  col-sm-4 no-margin-left">
<!-- ============================================================= FEATURED PRODUCTS ============================================================= -->
<div class="widget">
<h2>Featured products</h2>
<div class="body">
<ul>
<li>
<div class="row">
<div class="col-xs-12 col-sm-9 no-margin">
<a href="index6c11.html?page=single-product">Netbook Acer Travel B113-E-10072</a>
<div class="price">
<div class="price-prev">$2000</div>
<div class="price-current">$1873</div>
</div>
</div>
<div class="col-xs-12 col-sm-3 no-margin">
<a href="#" class="thumb-holder">
<img alt="" src="assets/images/blank.gif.pagespeed.ce.2JdGiI2i2V.gif" data-echo="assets/images/products/product-small-01.jpg"/>
</a>
</div>
</div>
</li>
<li>
<div class="row">
<div class="col-xs-12 col-sm-9 no-margin">
<a href="index6c11.html?page=single-product">PowerShot Elph 115 16MP Digital Camera</a>
<div class="price">
<div class="price-prev">$2000</div>
<div class="price-current">$1873</div>
</div>
</div>
<div class="col-xs-12 col-sm-3 no-margin">
<a href="#" class="thumb-holder">
<img alt="" src="assets/images/blank.gif.pagespeed.ce.2JdGiI2i2V.gif" data-echo="assets/images/products/product-small-02.jpg"/>
</a>
</div>
</div>
</li>
<li>
<div class="row">
<div class="col-xs-12 col-sm-9 no-margin">
<a href="index6c11.html?page=single-product">PowerShot Elph 115 16MP Digital Camera</a>
<div class="price">
<div class="price-prev">$2000</div>
<div class="price-current">$1873</div>
</div>
</div>
<div class="col-xs-12 col-sm-3 no-margin">
<a href="#" class="thumb-holder">
<img alt="" src="assets/images/blank.gif.pagespeed.ce.2JdGiI2i2V.gif" data-echo="assets/images/products/product-small-03.jpg"/>
</a>
</div>
</div>
</li>
</ul>
</div><!-- /.body -->
</div> <!-- /.widget -->
<!-- ============================================================= FEATURED PRODUCTS : END ============================================================= --> </div><!-- /.col -->
<div class="col-xs-12 col-sm-4 ">
<!-- ============================================================= ON SALE PRODUCTS ============================================================= -->
<div class="widget">
<h2>On-Sale Products</h2>
<div class="body">
<ul>
<li>
<div class="row">
<div class="col-xs-12 col-sm-9 no-margin">
<a href="index6c11.html?page=single-product">HP Scanner 2910P</a>
<div class="price">
<div class="price-prev">$2000</div>
<div class="price-current">$1873</div>
</div>
</div>
<div class="col-xs-12 col-sm-3 no-margin">
<a href="#" class="thumb-holder">
<img alt="" src="assets/images/blank.gif.pagespeed.ce.2JdGiI2i2V.gif" data-echo="assets/images/products/product-small-04.jpg"/>
</a>
</div>
</div>
</li>
<li>
<div class="row">
<div class="col-xs-12 col-sm-9 no-margin">
<a href="index6c11.html?page=single-product">Galaxy Tab 3 GT-P5210 16GB, Wi-Fi, 10.1in - White</a>
<div class="price">
<div class="price-prev">$2000</div>
<div class="price-current">$1873</div>
</div>
</div>
<div class="col-xs-12 col-sm-3 no-margin">
<a href="#" class="thumb-holder">
<img alt="" src="assets/images/blank.gif.pagespeed.ce.2JdGiI2i2V.gif" data-echo="assets/images/products/product-small-05.jpg"/>
</a>
</div>
</div>
</li>
<li>
<div class="row">
<div class="col-xs-12 col-sm-9 no-margin">
<a href="index6c11.html?page=single-product">PowerShot Elph 115 16MP Digital Camera</a>
<div class="price">
<div class="price-prev">$2000</div>
<div class="price-current">$1873</div>
</div>
</div>
<div class="col-xs-12 col-sm-3 no-margin">
<a href="#" class="thumb-holder">
<img alt="" src="assets/images/blank.gif.pagespeed.ce.2JdGiI2i2V.gif" data-echo="assets/images/products/product-small-06.jpg"/>
</a>
</div>
</div>
</li>
</ul>
</div><!-- /.body -->
</div> <!-- /.widget -->
<!-- ============================================================= ON SALE PRODUCTS : END ============================================================= --> </div><!-- /.col -->
<div class="col-xs-12 col-sm-4 ">
<!-- ============================================================= TOP RATED PRODUCTS ============================================================= -->
<div class="widget">
<h2>Top Rated Products</h2>
<div class="body">
<ul>
<li>
<div class="row">
<div class="col-xs-12 col-sm-9 no-margin">
<a href="index6c11.html?page=single-product">Galaxy Tab GT-P5210, 10" 16GB Wi-Fi</a>
<div class="price">
<div class="price-prev">$2000</div>
<div class="price-current">$1873</div>
</div>
</div>
<div class="col-xs-12 col-sm-3 no-margin">
<a href="#" class="thumb-holder">
<img alt="" src="assets/images/blank.gif.pagespeed.ce.2JdGiI2i2V.gif" data-echo="assets/images/products/product-small-07.jpg"/>
</a>
</div>
</div>
</li>
<li>
<div class="row">
<div class="col-xs-12 col-sm-9 no-margin">
<a href="index6c11.html?page=single-product">PowerShot Elph 115 16MP Digital Camera</a>
<div class="price">
<div class="price-prev">$2000</div>
<div class="price-current">$1873</div>
</div>
</div>
<div class="col-xs-12 col-sm-3 no-margin">
<a href="#" class="thumb-holder">
<img alt="" src="assets/images/blank.gif.pagespeed.ce.2JdGiI2i2V.gif" data-echo="assets/images/products/product-small-08.jpg"/>
</a>
</div>
</div>
</li>
<li>
<div class="row">
<div class="col-xs-12 col-sm-9 no-margin">
<a href="index6c11.html?page=single-product">Surface RT 64GB, Wi-Fi, 10.6in - Dark Titanium</a>
<div class="price">
<div class="price-prev">$2000</div>
<div class="price-current">$1873</div>
</div>
</div>
<div class="col-xs-12 col-sm-3 no-margin">
<a href="#" class="thumb-holder">
<img alt="" src="assets/images/blank.gif.pagespeed.ce.2JdGiI2i2V.gif" data-echo="assets/images/products/product-small-09.jpg"/>
</a>
</div>
</div>
</li>
</ul>
</div><!-- /.body -->
</div><!-- /.widget -->
<!-- ============================================================= TOP RATED PRODUCTS : END ============================================================= --> </div><!-- /.col -->
</div><!-- /.widgets-row-->
</div><!-- /.container -->
<div class="sub-form-row">
<div class="container">
<div class="col-xs-12 col-sm-8 col-sm-offset-2 no-padding">
<form role="form">
<input placeholder="Subscribe to our newsletter">
<button class="le-button">Subscribe</button>
</form>
</div>
</div><!-- /.container -->
</div><!-- /.sub-form-row -->
<div class="link-list-row">
<div class="container no-padding">
<div class="col-xs-12 col-md-4 ">
<!-- ============================================================= CONTACT INFO ============================================================= -->
<div class="contact-info">
<div class="footer-logo">
<svg width="233px" height="54px" viewBox="0 0 233 54" version="1.1" xmlns="http://www.w3.org/2000/svg">
<path fill="#f9f9f9" d=" M 0.00 0.00 L 233.00 0.00 L 233.00 54.00 L 0.00 54.00 L 0.00 0.00 Z"/>
<path class="logo-svg" d=" M 104.82 6.46 C 107.74 5.13 110.66 3.80 113.58 2.46 C 113.59 18.71 113.59 34.95 113.59 51.20 C 110.65 51.20 107.71 51.20 104.78 51.20 C 104.84 50.67 104.96 49.62 105.02 49.09 C 100.02 53.56 91.66 52.69 87.37 47.67 C 84.80 44.83 83.96 40.97 83.20 37.33 C 75.63 37.37 68.05 37.26 60.47 37.40 C 61.41 39.88 62.49 42.75 65.24 43.71 C 69.03 45.31 73.10 43.58 75.89 40.91 C 77.67 42.73 79.47 44.54 81.22 46.40 C 75.60 52.47 65.66 53.95 58.77 49.23 C 53.06 45.18 51.58 37.52 52.30 30.95 C 52.75 25.29 55.84 19.51 61.29 17.27 C 66.83 15.00 73.85 15.40 78.54 19.37 C 81.58 21.92 82.87 25.85 83.50 29.64 C 84.32 24.24 87.32 18.69 92.71 16.75 C 96.83 15.07 101.64 15.89 104.93 18.89 C 104.77 14.75 104.83 10.60 104.82 6.46 Z"/>
<path class="logo-svg" d=" M 118.44 3.44 C 121.37 1.09 126.26 3.21 126.52 6.97 C 127.17 10.89 122.39 14.12 119.00 12.03 C 115.74 10.46 115.39 5.41 118.44 3.44 Z"/>
<path class="logo-svg" d=" M 10.22 19.63 C 14.71 14.58 23.51 14.56 27.42 20.34 C 33.58 13.24 48.54 14.42 50.33 24.80 C 51.41 33.54 50.55 42.41 50.82 51.20 C 47.88 51.20 44.94 51.20 42.00 51.20 C 41.92 44.09 42.14 36.97 41.93 29.87 C 41.95 27.41 40.48 24.57 37.76 24.43 C 34.66 23.72 30.88 25.51 30.74 29.01 C 30.35 36.39 30.71 43.80 30.59 51.20 C 27.67 51.20 24.75 51.20 21.82 51.20 C 21.74 44.12 21.98 37.04 21.73 29.96 C 21.79 27.24 19.97 24.22 16.95 24.37 C 13.91 23.84 10.58 25.90 10.49 29.15 C 10.13 36.49 10.47 43.85 10.35 51.20 C 7.43 51.20 4.51 51.20 1.59 51.20 C 1.59 39.69 1.59 28.18 1.59 16.67 C 4.53 16.67 7.47 16.67 10.41 16.67 C 10.36 17.41 10.27 18.89 10.22 19.63 Z"/>
<path class="logo-svg" d=" M 129.25 19.84 C 135.49 16.15 143.23 14.75 150.23 16.89 C 154.92 18.35 157.47 23.34 157.42 28.02 C 157.56 35.75 157.42 43.47 157.47 51.20 C 154.57 51.20 151.68 51.20 148.79 51.20 C 148.84 50.65 148.93 49.56 148.98 49.01 C 144.10 52.49 137.26 53.09 132.09 49.91 C 126.05 46.27 125.51 36.43 131.16 32.19 C 136.26 28.22 143.30 28.77 149.13 30.64 C 148.62 28.53 148.91 25.65 146.65 24.49 C 141.77 22.26 136.27 24.40 131.93 26.90 C 131.04 24.55 130.14 22.19 129.25 19.84 Z"/>
<path class="logo-svg" d=" M 117.06 16.67 C 119.98 16.67 122.90 16.67 125.82 16.67 C 125.82 28.18 125.82 39.69 125.82 51.20 C 122.90 51.20 119.98 51.20 117.06 51.20 C 117.06 39.69 117.06 28.18 117.06 16.67 Z"/>
<path fill="#3a3a3a" d=" M 201.86 17.62 C 202.52 17.30 203.82 16.67 204.48 16.35 C 204.48 18.10 204.48 19.84 204.48 21.59 C 205.23 21.59 206.73 21.60 207.48 21.60 C 207.48 22.17 207.48 23.30 207.48 23.86 C 206.73 23.87 205.22 23.89 204.47 23.91 C 204.49 26.42 204.34 28.95 204.62 31.45 C 205.86 32.02 207.13 32.53 208.42 32.99 C 206.71 34.03 204.25 35.64 202.51 33.72 C 201.29 30.65 202.08 27.15 201.88 23.91 C 201.39 23.89 200.42 23.87 199.94 23.86 C 199.94 23.30 199.94 22.18 199.94 21.62 C 200.43 21.60 201.41 21.57 201.90 21.56 C 201.88 20.24 201.87 18.93 201.86 17.62 Z"/>
<path fill="#3a3a3a" d=" M 169.01 34.60 C 161.80 34.48 161.85 21.38 169.03 21.30 C 171.56 20.91 173.24 22.99 174.55 24.80 C 172.38 25.03 170.35 23.99 168.21 24.05 C 165.19 25.78 165.69 32.04 169.72 32.24 C 171.04 30.86 172.68 30.26 174.58 30.81 C 173.29 32.73 171.68 35.03 169.01 34.60 Z"/>
<path fill="#3a3a3a" d=" M 176.14 24.15 C 177.71 20.91 182.63 20.34 185.06 22.88 C 186.61 24.47 186.37 26.86 186.72 28.88 C 183.63 28.96 180.54 28.94 177.46 29.07 C 178.33 30.06 178.74 31.71 180.15 32.08 C 182.15 31.99 184.30 31.15 185.94 32.84 C 183.84 33.87 181.50 35.30 179.09 34.28 C 175.09 32.89 174.46 27.52 176.14 24.15 Z"/>
<path fill="#3a3a3a" d=" M 188.27 21.67 C 191.44 22.12 195.65 20.04 197.89 23.12 C 199.40 26.61 198.50 30.58 198.71 34.28 C 197.84 34.27 196.97 34.27 196.10 34.26 C 196.04 31.26 196.36 28.21 195.84 25.24 C 195.30 22.95 190.93 23.34 191.03 25.84 C 190.71 28.63 190.93 31.46 190.90 34.27 C 190.25 34.27 188.94 34.27 188.28 34.27 C 188.28 30.07 188.28 25.87 188.27 21.67 Z"/>
<path fill="#3a3a3a" d=" M 209.34 24.33 C 210.88 20.80 216.32 20.24 218.67 23.23 C 219.85 24.84 219.70 26.96 220.06 28.84 C 216.93 28.95 213.79 28.96 210.67 29.17 C 211.42 30.17 212.14 31.20 213.03 32.08 C 214.53 32.45 215.99 31.66 217.44 31.37 C 217.81 31.81 218.56 32.68 218.93 33.11 C 216.82 34.02 214.49 35.28 212.18 34.20 C 208.39 32.73 207.86 27.62 209.34 24.33 Z"/>
<path fill="#3a3a3a" d=" M 221.57 21.68 C 224.18 22.17 229.35 19.73 229.26 23.99 C 227.69 24.37 225.10 23.31 224.48 25.41 C 223.89 28.32 224.25 31.33 224.17 34.27 C 223.52 34.27 222.23 34.27 221.58 34.27 C 221.58 30.07 221.58 25.88 221.57 21.68 Z"/>
<path fill="#f9f9f9" d=" M 177.47 26.72 C 178.35 25.45 179.09 23.45 180.99 23.60 C 182.85 23.48 183.54 25.46 184.45 26.68 C 182.12 26.79 179.79 26.80 177.47 26.72 Z"/>
<path fill="#f9f9f9" d=" M 210.63 26.63 C 211.70 25.38 212.59 23.21 214.62 23.63 C 216.40 23.58 216.80 25.59 217.66 26.73 C 215.32 26.80 212.97 26.77 210.63 26.63 Z"/>
<path fill="#f9f9f9" d=" M 60.57 29.87 C 61.45 26.41 64.15 23.26 68.04 23.58 C 71.91 23.17 74.23 26.62 75.30 29.84 C 70.39 29.99 65.48 29.97 60.57 29.87 Z"/>
<path fill="#f9f9f9" d=" M 96.52 24.70 C 99.50 23.55 102.54 25.02 104.87 26.85 C 104.80 31.61 104.80 36.38 104.86 41.14 C 102.46 43.09 99.18 44.42 96.17 43.02 C 92.82 41.46 91.99 37.36 91.96 34.01 C 91.89 30.51 92.82 26.06 96.52 24.70 Z"/>
<path fill="#f9f9f9" d=" M 137.58 37.63 C 141.09 35.82 145.16 36.85 148.82 37.59 C 148.82 38.98 148.80 40.38 148.79 41.78 C 145.51 43.89 141.25 45.34 137.54 43.42 C 135.23 42.33 135.28 38.72 137.58 37.63 Z"/>
<path class="logo-svg" d=" M 163.30 39.16 C 165.64 38.00 168.47 38.66 171.01 38.49 C 172.96 38.53 176.17 38.23 176.35 40.94 C 176.51 46.79 170.77 51.96 165.05 51.93 C 161.43 51.79 162.41 47.39 162.23 44.97 C 162.49 43.09 161.71 40.56 163.30 39.16 Z"/>
</svg>
</div><!-- /.footer-logo -->
<p class="regular-bold"> Feel free to contact us via phone,email or just send us mail.</p>
<p>
17 Princess Road, London, Greater London NW1 8JR, UK
1-888-8MEDIA (1-888-892-9953)
</p>
<div class="social-icons">
<h3>Get in touch</h3>
<ul>
<li><a href="http://facebook.com/transvelo" class="fa fa-facebook"></a></li>
<li><a href="#" class="fa fa-twitter"></a></li>
<li><a href="#" class="fa fa-pinterest"></a></li>
<li><a href="#" class="fa fa-linkedin"></a></li>
<li><a href="#" class="fa fa-stumbleupon"></a></li>
<li><a href="#" class="fa fa-dribbble"></a></li>
<li><a href="#" class="fa fa-vk"></a></li>
</ul>
</div><!-- /.social-icons -->
</div>
<!-- ============================================================= CONTACT INFO : END ============================================================= --> </div>
<div class="col-xs-12 col-md-8 no-margin">
<!-- ============================================================= LINKS FOOTER ============================================================= -->
<div class="link-widget">
<div class="widget">
<h3>Find it fast</h3>
<ul>
<li><a href="indexe96c.html?page=category-grid">laptops &amp; computers</a></li>
<li><a href="indexe96c.html?page=category-grid">Cameras &amp; Photography</a></li>
<li><a href="indexe96c.html?page=category-grid">Smart Phones &amp; Tablets</a></li>
<li><a href="indexe96c.html?page=category-grid">Video Games &amp; Consoles</a></li>
<li><a href="indexe96c.html?page=category-grid">TV &amp; Audio</a></li>
<li><a href="indexe96c.html?page=category-grid">Gadgets</a></li>
<li><a href="indexe96c.html?page=category-grid">Car Electronic &amp; GPS</a></li>
<li><a href="indexe96c.html?page=category-grid">Accesories</a></li>
</ul>
</div><!-- /.widget -->
</div><!-- /.link-widget -->
<div class="link-widget">
<div class="widget">
<h3>Information</h3>
<ul>
<li><a href="indexe96c.html?page=category-grid">Find a Store</a></li>
<li><a href="indexe96c.html?page=category-grid">About Us</a></li>
<li><a href="indexe96c.html?page=category-grid">Contact Us</a></li>
<li><a href="indexe96c.html?page=category-grid">Weekly Deals</a></li>
<li><a href="indexe96c.html?page=category-grid">Gift Cards</a></li>
<li><a href="indexe96c.html?page=category-grid">Recycling Program</a></li>
<li><a href="indexe96c.html?page=category-grid">Community</a></li>
<li><a href="indexe96c.html?page=category-grid">Careers</a></li>
</ul>
</div><!-- /.widget -->
</div><!-- /.link-widget -->
<div class="link-widget">
<div class="widget">
<h3>Information</h3>
<ul>
<li><a href="indexe96c.html?page=category-grid">My Account</a></li>
<li><a href="indexe96c.html?page=category-grid">Order Tracking</a></li>
<li><a href="indexe96c.html?page=category-grid">Wish List</a></li>
<li><a href="indexe96c.html?page=category-grid">Customer Service</a></li>
<li><a href="indexe96c.html?page=category-grid">Returns / Exchange</a></li>
<li><a href="indexe96c.html?page=category-grid">FAQs</a></li>
<li><a href="indexe96c.html?page=category-grid">Product Support</a></li>
<li><a href="indexe96c.html?page=category-grid">Extended Service Plans</a></li>
</ul>
</div><!-- /.widget -->
</div><!-- /.link-widget -->
<!-- ============================================================= LINKS FOOTER : END ============================================================= --> </div>
</div><!-- /.container -->
</div><!-- /.link-list-row -->
<div class="copyright-bar">
<div class="container">
<div class="col-xs-12 col-sm-6 no-margin">
<div class="copyright">
&copy; <a href="index9ba3.html?page=home">Media Center</a> - all rights reserved
</div><!-- /.copyright -->
</div>
<div class="col-xs-12 col-sm-6 no-margin">
<div class="payment-methods ">
<ul>
<li><img alt="" src="assets/images/payments/xpayment-visa.png.pagespeed.ic.8TKcgXujH9.png"></li>
<li><img alt="" src="assets/images/payments/xpayment-master.png.pagespeed.ic.VEsjw4FcFc.png"></li>
<li><img alt="" src="assets/images/payments/xpayment-paypal.png.pagespeed.ic.qfSak4CvJJ.png"></li>
<li><img alt="" src="assets/images/payments/xpayment-skrill.png.pagespeed.ic.z0h-T_LdHd.png"></li>
</ul>
</div><!-- /.payment-methods -->
</div>
</div><!-- /.container -->
</div><!-- /.copyright-bar -->
</footer><!-- /#footer -->
<!-- ============================================================= FOOTER : END ============================================================= -->	</div><!-- /.wrapper -->
<!-- For demo purposes – can be removed on production -->
<div class="config open">

</div>
<a class="show-theme-options" href="#"><i class="fa fa-wrench"></i></a>
</div>
<!-- For demo purposes – can be removed on production : End -->
<!-- JavaScripts placed at the end of the document so the pages load faster -->
<script src="assets/js/jquery-1.10.2.min.js.pagespeed.jm.ZzSiN_5Whq.js"></script>
<script src="assets/js/jquery-migrate-1.2.1.js%2bbootstrap.min.js.pagespeed.jc.cBPuKJpPR6.js"></script><script>eval(mod_pagespeed_tcIO07jWkY);</script>
<script>eval(mod_pagespeed_$aLAX2BU38);</script>
<script src="http://maps.google.com/maps/api/js?sensor=false&amp;language=en"></script>
<script src="assets/js/gmap3.min.js%2bbootstrap-hover-dropdown.min.js%2bowl.carousel.min.js%2bcss_browser_selector.min.js%2becho.min.js%2bjquery.easing-1.3.min.js%2bbootstrap-slider.min.js%2bjque"></script><script>eval(mod_pagespeed_zLcrFFFb_P);</script>
<script>eval(mod_pagespeed_qQ2YZxlTml);</script>
<script>eval(mod_pagespeed_6ealB$sFbN);</script>
<script>eval(mod_pagespeed_Cn4_rsAjON);</script>
<script>eval(mod_pagespeed_2NB2VnnPWU);</script>
<script>eval(mod_pagespeed_evnHtkyCUN);</script>
<script>eval(mod_pagespeed_t6jjfK8ypo);</script>
<script>eval(mod_pagespeed_vLnQrWN49g);</script>
<script src="assets%2c_js%2c_jquery.prettyPhoto.min.js%2bassets%2c_js%2c_jquery.customSelect.min.js%2bassets%2c_js%2c_wow.min.js%2bassets%2c_js%2c_scripts.js%2bswitchstylesheet%2c_switchstylesheet.js.pagespe"></script><script>eval(mod_pagespeed_JV6WgXtHQM);</script>
<script>eval(mod_pagespeed_Z211ARU5f8);</script>
<script>eval(mod_pagespeed_bEZ9YfBTmN);</script>
<script>eval(mod_pagespeed_U6_dTu8Vaj);</script>
<!-- For demo purposes – can be removed on production -->
<script>eval(mod_pagespeed__SyMhJWgD4);</script>
<script>$(document).ready(function(){$(".changecolor").switchstylesheet({seperator:"color"});$('.show-theme-options').click(function(){$(this).parent().toggleClass('open');return false;});});$(window).bind("load",function(){$('.show-theme-options').delay(2000).trigger('click');});</script>
<!-- For demo purposes – can be removed on production : End -->
<script src="../../../w.sharethis.com/button/buttons.js"></script>
<script pagespeed_no_defer="">//<![CDATA[
(function(){var f=function(b){var a=window;if(a.addEventListener)a.addEventListener("load",b,!1);else if(a.attachEvent)a.attachEvent("onload",b);else{var c=a.onload;a.onload=function(){b.call(this);c&&c.call(this)}}};window.pagespeed=window.pagespeed||{};var k=window.pagespeed,l=function(b,a,c,g,h){this.e=b;this.f=a;this.h=c;this.g=g;this.b=h;this.c=[];this.a=0};l.prototype.d=function(b){for(var a=0;250>a&&this.a<this.b.length;++a,++this.a)try{null!=document.querySelector(this.b[this.a])&&this.c.push(this.b[this.a])}catch(c){}this.a<this.b.length?window.setTimeout(this.d.bind(this),0,b):b()};
k.i=function(b,a,c,g,h){if(document.querySelector&&Function.prototype.bind){var d=new l(b,a,c,g,h);f(function(){window.setTimeout(function(){d.d(function(){for(var a="oh="+d.h+"&n="+d.g,a=a+"&cs=",b=0;b<d.c.length;++b){var c=0<b?",":"",c=c+encodeURIComponent(d.c[b]);if(131072<a.length+c.length)break;a+=c}k.criticalCssBeaconData=a;var b=d.e,c=d.f,e;if(window.XMLHttpRequest)e=new XMLHttpRequest;else if(window.ActiveXObject)try{e=new ActiveXObject("Msxml2.XMLHTTP")}catch(g){try{e=new ActiveXObject("Microsoft.XMLHTTP")}catch(h){}}e&&
(e.open("POST.html",b+(-1==b.indexOf("?")?"?":"&")+"url="+encodeURIComponent(c)),e.setRequestHeader("Content-Type","application/x-www-form-urlencoded"),e.send(a))})},0)})}};k.criticalCssBeaconInit=k.i;})();
pagespeed.selectors=["#authentication .bordered","#banner-holder","#banner-holder .banner","#banner-holder .banner .banner-image","#banner-holder .banner .banner-text","#banner-holder .banner .banner-text .tagline","#banner-holder .banner .banner-text h1","#banner-holder .container .banner","#banner-holder .container .banner > a","#banner-holder .container .banner > a .banner-text","#banner-holder .container .banner > a .banner-text .tagline","#banner-holder .container .banner > a .banner-text h1","#bestsellers","#bestsellers .product-grid-holder > .col-sm-5","#bestsellers .product-grid-holder > .col-sm-7","#bestsellers .single-product-gallery","#bestsellers .single-product-gallery .single-product-gallery-item","#bestsellers .size-medium","#breadcrumb-alt","#cart-page","#cart-page .items-holder","#cart-page .items-holder .cart-item","#cart-page .items-holder .cart-item .brand","#cart-page .items-holder .cart-item .price","#cart-page .items-holder .cart-item .quantity","#cart-page .items-holder .cart-item .title","#cart-page .items-holder .cart-item .title a","#cart-page .items-holder .close-btn","#cart-page .widget","#cart-page .widget #total-price","#cart-page .widget #total-price .value","#cart-page .widget .body","#cart-page .widget .buttons-holder","#cart-page .widget .buttons-holder .simple-link","#cart-page .widget .tabled-data li label","#checkout-page #create-account","#checkout-page #shipping-address","#checkout-page #shipping-address form","#checkout-page #subtotal-holder","#checkout-page #subtotal-holder .radio-group .radio-label","#checkout-page #subtotal-holder .tabled-data li .value","#checkout-page #subtotal-holder .tabled-data li label","#checkout-page #total-field","#checkout-page #total-field .value","#checkout-page #total-field label","#checkout-page #your-order","#checkout-page #your-order .order-item","#checkout-page #your-order .order-item .brand","#checkout-page #your-order .order-item .price","#checkout-page #your-order .order-item .qty","#checkout-page #your-order .order-item .title","#checkout-page #your-order .order-item .title a","#checkout-page .billing-address .field-row","#checkout-page .billing-address .le-checkbox","#checkout-page .billing-address .le-input","#checkout-page .billing-address .placeholder","#checkout-page .billing-address form","#checkout-page .place-order-button","#comments > h3","#contact-us .bordered","#footer","#footer .copyright-bar","#footer .copyright-bar .container > div","#footer .copyright-bar .container > div .payment-methods","#footer .copyright-bar .container > div .payment-methods > ul","#footer .copyright-bar .container > div .payment-methods > ul > li","#footer .copyright-bar .copyright","#footer .copyright-bar .copyright a","#footer .link-list-row","#footer .link-list-row .contact-info","#footer .link-list-row .contact-info .regular-bold","#footer .link-list-row .contact-info .social-icons","#footer .link-list-row .contact-info .social-icons h3","#footer .link-list-row .container > div","#footer .link-list-row .footer-logo","#footer .link-list-row .link-widget","#footer .link-list-row .link-widget h3","#footer .link-list-row .link-widget li","#footer .link-list-row .link-widget li a","#footer .payment-methods","#footer .payment-methods li","#footer .payment-methods li img","#footer .sub-form-row","#footer .sub-form-row button","#footer .sub-form-row input","#footer .widget .body","#footer .widget .body li","#footer .widget .body li a","#footer .widget .price","#footer .widget .price .price-current","#footer .widget .price .price-prev","#footer .widget .row","#footer .widget .thumb-holder","#footer .widget .thumb-holder img","#footer .widgets-row > div","#footer h2","#footer h3","#grid-page-banner","#hero .caption","#loading","#owl-main","#owl-main .caption","#owl-main .caption .big-text","#owl-main .caption .big-text .bg","#owl-main .caption .big-text .big","#owl-main .caption .big-text .big .sign","#owl-main .caption .button-holder","#owl-main .caption .button-holder a","#owl-main .caption .excerpt","#owl-main .caption .small","#owl-main .caption.right","#owl-main .caption.text-center","#owl-main .caption.text-left","#owl-main .caption.text-right","#owl-main .caption.vertical-bottom","#owl-main .caption.vertical-center","#owl-main .caption.vertical-top","#owl-main .container","#owl-main .item","#owl-main .owl-controls","#owl-main .owl-item","#owl-main .owl-next","#owl-main .owl-pagination","#owl-main .owl-prev","#owl-main.height-lg .item","#owl-main.height-md .item","#owl-recently-viewed .owl-item .product-item-holder","#owl-recently-viewed .owl-wrapper-outer","#owl-recently-viewed .owl-wrapper-outer .owl-item .product-item-holder","#owl-recently-viewed .owl-wrapper-outer .owl-item .product-item-holder .wish-compare a","#owl-recently-viewed .size-small","#owl-recently-viewed-2 .owl-item .product-item-holder","#owl-recently-viewed-2 .owl-wrapper-outer","#owl-recently-viewed-2 .owl-wrapper-outer .owl-item .product-item-holder","#owl-recently-viewed-2 .owl-wrapper-outer .owl-item .product-item-holder .wish-compare a","#owl-recently-viewed-2 .size-small","#owl-recommended-products .owl-item .product-item-holder","#owl-recommended-products .owl-wrapper-outer","#owl-recommended-products .owl-wrapper-outer .owl-item .product-item-holder","#owl-recommended-products .owl-wrapper-outer .owl-item .product-item-holder .wish-compare a","#owl-single-product .single-product-gallery-item > a","#owl-single-product-thumbnails .owl-item .horizontal-thumb","#owl-single-product-thumbnails .owl-wrapper-outer","#payment-method-options","#payment-method-options .payment-method-option","#payment-method-options .payment-method-option .fake-box","#payment-method-options .payment-method-option .radio-label","#payment-method-options .payment-method-option .value","#payment-method-options .payment-method-option .value p","#products-tab","#products-tab .image","#recently-reviewd","#recommended-products","#recommended-products .size-medium","#recommended-products .size-medium .body","#recommended-products .size-medium .image","#recommended-products .size-medium .ribbon","#recommended-products .title-nav","#reply-block .le-input","#single-product .body-holder","#single-product .body-holder #addto-cart","#single-product .body-holder .body","#single-product .body-holder .body .availability","#single-product .body-holder .body .brand","#single-product .body-holder .body .title a","#single-product .body-holder .buttons-holder","#single-product .body-holder .buttons-holder .btn-add-to-wishlist","#single-product .body-holder .excerpt","#single-product .body-holder .price-current","#single-product .body-holder .price-prev","#single-product .body-holder .qnt-holder","#single-product .body-holder .qnt-holder #addto-cart","#single-product .body-holder .social-row","#single-product .body-holder .stButton .stFb","#single-product .body-holder .stButton .stMainServices","#single-product .body-holder .stButton .stTwbutton","#single-product .gallery-holder","#single-product .gallery-holder .single-product-gallery-item","#single-product .gallery-thumbs","#single-product .gallery-thumbs .owl-wrapper-outer","#single-product .gallery-thumbs img","#single-product .product-item-holder","#single-product .product-item-holder .owl-wrapper-outer","#single-product .single-product-gallery-item","#single-product .single-product-gallery-item img","#single-product-tab","#single-product-tab .new-review-form","#single-product-tab .new-review-form .buttons-holder","#single-product-tab .new-review-form .buttons-holder button","#single-product-tab .new-review-form .field-row","#single-product-tab .new-review-form .star-row","#single-product-tab .new-review-form .star-row .star-holder","#single-product-tab .new-review-form form","#single-product-tab .new-review-form h2","#single-product-tab .tab-content","#single-product-tab .tab-pane","#single-product-tab .tab-pane .meta-row","#single-product-tab .tab-pane .meta-row .seperator","#single-product-tab .tab-pane p","#top-banner-and-menu","#top-banner-and-menu.homepage2","#top-brands","#top-mega-nav","#top-mega-nav nav","#top-mega-nav nav .le-dropdown","#top-megamenu-nav","#top-megamenu-nav .dropdown","#top-megamenu-nav .dropdown .dropdown-toggle","#top-megamenu-nav .dropdown.active .dropdown-toggle","#top-megamenu-nav .dropdown.open .dropdown-toggle","#top-megamenu-nav .nav","#top-megamenu-nav .nav .dropdown","#top-megamenu-nav .nav .dropdown-menu","#top-megamenu-nav .nav > .dropdown","#top-megamenu-nav .nav > li","#top-megamenu-nav .nav > li .dropdown-toggle","#top-megamenu-nav .nav > li > a","#top-megamenu-nav .nav > li.active a","#top-megamenu-nav .nav > li.open .dropdown-toggle","#top-megamenu-nav .navbar","#top-megamenu-nav .navbar-nav .dropdown.open .dropdown-toggle","#top-megamenu-nav .navbar-nav > li","#top-megamenu-nav .navbar-nav > li .dropdown-menu > li > a","#top-megamenu-nav .navbar-nav > li .dropdown-toggle","#top-megamenu-nav .navbar-nav > li > a","*",".accordion-widget",".accordion-widget .accordion",".accordion-widget .accordion .accordion-inner",".accordion-widget .accordion .accordion-inner .accordion-body ul",".accordion-widget .accordion .accordion-inner li",".accordion-widget .accordion .accordion-inner li a",".accordion-widget .accordion .accordion-toggle",".accordion-widget .accordion .accordion-toggle.collapsed",".accordion-widget .accordion a",".affix",".alert",".alert .alert-link",".alert > p",".alert > p + p",".alert > ul",".alert h4",".alert-danger",".alert-danger .alert-link",".alert-danger hr",".alert-dismissable",".alert-dismissable .close",".alert-info",".alert-info .alert-link",".alert-info hr",".alert-success",".alert-success .alert-link",".alert-success hr",".alert-warning",".alert-warning .alert-link",".alert-warning hr",".animate-dropdown .open > .dropdown-menu",".animate-dropdown .open > .dropdown-menu > .dropdown-submenu > .dropdown-menu",".animated",".animated.flip",".animated.hinge",".animated.infinite",".auto-width",".availability",".availability .available",".availability .not-available",".availability span",".badge",".banner .banner-text",".banner .banner-text .tagline",".banner .banner-text h1",".banner .banner-text.right",".banner .banner-text.theblue .tagline",".banner .banner-text.theblue h1",".banner > a",".banner img",".bg-danger",".bg-info",".bg-primary",".bg-success",".bg-warning",".block",".blockquote-reverse",".blockquote-reverse .small",".blockquote-reverse footer",".blockquote-reverse small",".blog-pagination",".blog-pagination > li",".blog-post-author",".blog-post-author .media .media-body p",".blog-post-author .media .media-heading",".blog-post-author .media .media-heading > a",".blog-post-author .media > .pull-left",".blog-sidebar .le-links > li > a",".blog-sidebar .tagcloud a",".blog-sidebar .widget",".blog-sidebar .widget .body",".blog-sidebar .widget .body .le-links > li",".blog-sidebar .widget .body .le-links > li > a",".blog-sidebar .widget .body .search-form .form-control",".blog-sidebar .widget .body .search-form .form-group",".blog-sidebar .widget .body .search-form button",".blog-sidebar .widget .body .tagcloud a",".blog-sidebar .widget .recent-post-list .posted-date",".blog-sidebar .widget .recent-post-list .sidebar-recent-post-item",".blog-sidebar .widget .recent-post-list h5",".blog-sidebar .widget .recent-post-list h5 a",".blog-sidebar .widget .recent-post-list img",".blog-sidebar .widget h4",".blue",".blue-text",".blue.ribbon",".bold",".bounce",".bounceIn",".bounceInDown",".bounceInLeft",".bounceInRight",".bounceInUp",".bounceOut",".bounceOutDown",".bounceOutLeft",".bounceOutRight",".bounceOutUp",".brands-carousel .carousel-item",".brands-carousel .carousel-item a",".brands-carousel .carousel-item a img",".breadcrumb",".breadcrumb > .active",".breadcrumb > li",".breadcrumb > li + li",".breadcrumb-nav-holder",".breadcrumb-nav-holder .breadcrumb-item",".breadcrumb-nav-holder .breadcrumb-item .dropdown-menu",".breadcrumb-nav-holder .breadcrumb-item .dropdown-menu li > a",".breadcrumb-nav-holder .breadcrumb-item > a",".breadcrumb-nav-holder .breadcrumb-item > a.dropdown-toggle",".breadcrumb-nav-holder .breadcrumb-item a",".breadcrumb-nav-holder .breadcrumb-item.current",".breadcrumb-nav-holder .breadcrumb-item.current a",".breadcrumb-nav-holder .breadcrumb-item.current.gray a",".breadcrumb-nav-holder .breadcrumb-item.open",".breadcrumb-nav-holder .breadcrumb-item.open > a",".breadcrumb-nav-holder.minimal .breadcrumb-item",".breadcrumb-nav-holder.minimal .breadcrumb-item > a",".btn",".btn .badge",".btn .caret",".btn .label",".btn-add-to-compare",".btn-add-to-wishlist",".btn-block",".btn-block + .btn-block",".btn-danger",".btn-danger .badge",".btn-danger.active",".btn-danger.disabled",".btn-danger.disabled.active",".btn-danger[disabled]",".btn-danger[disabled].active",".btn-default",".btn-default .badge",".btn-default.active",".btn-default.disabled",".btn-default.disabled.active",".btn-default[disabled]",".btn-default[disabled].active",".btn-facebook",".btn-facebook i",".btn-group",".btn-group .btn + .btn",".btn-group .btn + .btn-group",".btn-group .btn-group + .btn",".btn-group .btn-group + .btn-group",".btn-group .dropdown-toggle",".btn-group > .btn",".btn-group > .btn + .dropdown-toggle",".btn-group > .btn-group",".btn-group > .btn-group > .btn",".btn-group > .btn-group > .dropdown-toggle",".btn-group > .btn-lg + .dropdown-toggle",".btn-group > .btn.active",".btn-group-justified",".btn-group-justified > .btn",".btn-group-justified > .btn-group",".btn-group-justified > .btn-group .btn",".btn-group-lg > .btn",".btn-group-sm > .btn",".btn-group-vertical",".btn-group-vertical > .btn",".btn-group-vertical > .btn + .btn",".btn-group-vertical > .btn + .btn-group",".btn-group-vertical > .btn-group",".btn-group-vertical > .btn-group + .btn",".btn-group-vertical > .btn-group + .btn-group",".btn-group-vertical > .btn-group > .btn",".btn-group-vertical > .btn.active",".btn-group-xs > .btn",".btn-group.open .dropdown-toggle",".btn-group.open .dropdown-toggle.btn-link",".btn-info",".btn-info .badge",".btn-info.active",".btn-info.disabled",".btn-info.disabled.active",".btn-info[disabled]",".btn-info[disabled].active",".btn-lg",".btn-lg .caret",".btn-link",".btn-link[disabled]",".btn-loadmore",".btn-loadmore i",".btn-primary",".btn-primary .badge",".btn-primary.active",".btn-primary.disabled",".btn-primary.disabled.active",".btn-primary[disabled]",".btn-primary[disabled].active",".btn-sm",".btn-success",".btn-success .badge",".btn-success.active",".btn-success.disabled",".btn-success.disabled.active",".btn-success[disabled]",".btn-success[disabled].active",".btn-toolbar",".btn-toolbar .btn-group",".btn-toolbar .input-group",".btn-toolbar > .btn",".btn-toolbar > .btn-group",".btn-toolbar > .input-group",".btn-twitter",".btn-twitter i",".btn-warning",".btn-warning .badge",".btn-warning.active",".btn-warning.disabled",".btn-warning.disabled.active",".btn-warning[disabled]",".btn-warning[disabled].active",".btn-xs",".btn-xs .badge",".btn.active",".btn.disabled",".btn[disabled]",".capital",".caret",".caroufredsel_wrapper",".caroufredsel_wrapper ul",".carousel",".carousel-caption",".carousel-caption .btn",".carousel-control",".carousel-control .glyphicon-chevron-left",".carousel-control .glyphicon-chevron-right",".carousel-control .icon-next",".carousel-control .icon-prev",".carousel-control.left",".carousel-control.right",".carousel-holder .title-nav",".carousel-holder .title-nav .inverse",".carousel-holder .title-nav .nav-holder",".carousel-holder .title-nav .nav-holder a",".carousel-holder .title-nav h1",".carousel-holder .title-nav h2",".carousel-holder.hover",".carousel-holder.hover.small .size-small",".carousel-holder.hover.small .size-small .hover-area",".carousel-indicators",".carousel-indicators .active",".carousel-indicators li",".carousel-inner",".carousel-inner > .active",".carousel-inner > .active.left",".carousel-inner > .active.right",".carousel-inner > .item",".carousel-inner > .item > a > img",".carousel-inner > .item > img",".carousel-inner > .next",".carousel-inner > .next.left",".carousel-inner > .prev",".carousel-inner > .prev.right",".carousel-item.size-medium",".carousel-item.size-small",".center-absolute",".center-block",".cf-style-1 .field-row",".cf-style-1 .le-input",".cf-style-1 label",".checkbox",".checkbox + .checkbox",".checkbox input[type=\"checkbox\"]",".checkbox label",".checkbox-inline",".checkbox-inline + .checkbox-inline",".checkbox-inline input[type=\"checkbox\"]",".checkbox-inline[disabled]",".checkbox[disabled]",".clearfix",".close",".col-lg-1",".col-lg-10",".col-lg-11",".col-lg-12",".col-lg-2",".col-lg-3",".col-lg-4",".col-lg-5",".col-lg-6",".col-lg-7",".col-lg-8",".col-lg-9",".col-lg-offset-0",".col-lg-offset-1",".col-lg-offset-10",".col-lg-offset-11",".col-lg-offset-12",".col-lg-offset-2",".col-lg-offset-3",".col-lg-offset-4",".col-lg-offset-5",".col-lg-offset-6",".col-lg-offset-7",".col-lg-offset-8",".col-lg-offset-9",".col-lg-pull-0",".col-lg-pull-1",".col-lg-pull-10",".col-lg-pull-11",".col-lg-pull-12",".col-lg-pull-2",".col-lg-pull-3",".col-lg-pull-4",".col-lg-pull-5",".col-lg-pull-6",".col-lg-pull-7",".col-lg-pull-8",".col-lg-pull-9",".col-lg-push-0",".col-lg-push-1",".col-lg-push-10",".col-lg-push-11",".col-lg-push-12",".col-lg-push-2",".col-lg-push-3",".col-lg-push-4",".col-lg-push-5",".col-lg-push-6",".col-lg-push-7",".col-lg-push-8",".col-lg-push-9",".col-md-1",".col-md-10",".col-md-11",".col-md-12",".col-md-2",".col-md-3",".col-md-4",".col-md-5",".col-md-6",".col-md-7",".col-md-8",".col-md-9",".col-md-offset-0",".col-md-offset-1",".col-md-offset-10",".col-md-offset-11",".col-md-offset-12",".col-md-offset-2",".col-md-offset-3",".col-md-offset-4",".col-md-offset-5",".col-md-offset-6",".col-md-offset-7",".col-md-offset-8",".col-md-offset-9",".col-md-pull-0",".col-md-pull-1",".col-md-pull-10",".col-md-pull-11",".col-md-pull-12",".col-md-pull-2",".col-md-pull-3",".col-md-pull-4",".col-md-pull-5",".col-md-pull-6",".col-md-pull-7",".col-md-pull-8",".col-md-pull-9",".col-md-push-0",".col-md-push-1",".col-md-push-10",".col-md-push-11",".col-md-push-12",".col-md-push-2",".col-md-push-3",".col-md-push-4",".col-md-push-5",".col-md-push-6",".col-md-push-7",".col-md-push-8",".col-md-push-9",".col-sm-1",".col-sm-10",".col-sm-11",".col-sm-12",".col-sm-2",".col-sm-3",".col-sm-4",".col-sm-5",".col-sm-6",".col-sm-7",".col-sm-8",".col-sm-9",".col-sm-offset-0",".col-sm-offset-1",".col-sm-offset-10",".col-sm-offset-11",".col-sm-offset-12",".col-sm-offset-2",".col-sm-offset-3",".col-sm-offset-4",".col-sm-offset-5",".col-sm-offset-6",".col-sm-offset-7",".col-sm-offset-8",".col-sm-offset-9",".col-sm-pull-0",".col-sm-pull-1",".col-sm-pull-10",".col-sm-pull-11",".col-sm-pull-12",".col-sm-pull-2",".col-sm-pull-3",".col-sm-pull-4",".col-sm-pull-5",".col-sm-pull-6",".col-sm-pull-7",".col-sm-pull-8",".col-sm-pull-9",".col-sm-push-0",".col-sm-push-1",".col-sm-push-10",".col-sm-push-11",".col-sm-push-12",".col-sm-push-2",".col-sm-push-3",".col-sm-push-4",".col-sm-push-5",".col-sm-push-6",".col-sm-push-7",".col-sm-push-8",".col-sm-push-9",".col-xs-1",".col-xs-10",".col-xs-11",".col-xs-12",".col-xs-2",".col-xs-3",".col-xs-4",".col-xs-5",".col-xs-6",".col-xs-7",".col-xs-8",".col-xs-9",".col-xs-offset-0",".col-xs-offset-1",".col-xs-offset-10",".col-xs-offset-11",".col-xs-offset-12",".col-xs-offset-2",".col-xs-offset-3",".col-xs-offset-4",".col-xs-offset-5",".col-xs-offset-6",".col-xs-offset-7",".col-xs-offset-8",".col-xs-offset-9",".col-xs-pull-0",".col-xs-pull-1",".col-xs-pull-10",".col-xs-pull-11",".col-xs-pull-12",".col-xs-pull-2",".col-xs-pull-3",".col-xs-pull-4",".col-xs-pull-5",".col-xs-pull-6",".col-xs-pull-7",".col-xs-pull-8",".col-xs-pull-9",".col-xs-push-0",".col-xs-push-1",".col-xs-push-10",".col-xs-push-11",".col-xs-push-12",".col-xs-push-2",".col-xs-push-3",".col-xs-push-4",".col-xs-push-5",".col-xs-push-6",".col-xs-push-7",".col-xs-push-8",".col-xs-push-9",".collapse",".collapse.in",".collapsing",".color-bg",".comment-item",".comment-item .comment-body",".comment-item .comment-body .author",".comment-item .comment-body .comment-content",".comment-item .comment-body .comment-reply",".comment-item .comment-body .comment-text",".comment-item .comment-body .date",".comment-item .comment-body .dislikes .icon",".comment-item .comment-body .likes",".comment-item .comment-body .likes .dislikes .icon",".comment-item .comment-body .likes .icon",".comment-item .comment-body .likes a",".comment-item .comment-body .likes-count",".comment-item .comment-body header",".compare-list .add-cart-button",".compare-list .amount",".compare-list .image-wrap",".compare-list .price del",".compare-list .price del > .amount",".compare-list .price ins",".compare-list .price ins > .amount",".compare-list .price-current",".compare-list .price-prev",".compare-list .remove-link",".compare-list .tr-add-to-cart td",".compare-list thead > tr > td",".compare-list tr.description > td",".config",".config .dropdown .dropdown-toggle",".config-options",".config-options .list-unstyled",".config-options .list-unstyled > li > a",".config-options h4",".config.open",".container",".container .jumbotron",".container > .navbar-collapse",".container > .navbar-header",".container-fluid",".container-fluid > .navbar-collapse",".container-fluid > .navbar-header",".content-color",".dark-green-text",".dl-horizontal dd",".dl-horizontal dt",".dropdown",".dropdown .dropdown-menu",".dropdown .dropdown-toggle",".dropdown-backdrop",".dropdown-header",".dropdown-menu",".dropdown-menu .divider",".dropdown-menu > .active > a",".dropdown-menu > .disabled > a",".dropdown-menu > li > a",".dropdown-menu-left",".dropdown-menu-right",".dropdown-menu.pull-right",".dropdown-toggle",".dropup .btn-lg .caret",".dropup .caret",".dropup .dropdown-menu",".extra-bold",".fa",".fa-2x",".fa-3x",".fa-4x",".fa-5x",".fa-adjust",".fa-adn",".fa-align-center",".fa-align-justify",".fa-align-left",".fa-align-right",".fa-ambulance",".fa-anchor",".fa-android",".fa-angle-double-down",".fa-angle-double-left",".fa-angle-double-right",".fa-angle-double-up",".fa-angle-down",".fa-angle-left",".fa-angle-right",".fa-angle-up",".fa-apple",".fa-archive",".fa-arrow-circle-down",".fa-arrow-circle-left",".fa-arrow-circle-o-down",".fa-arrow-circle-o-left",".fa-arrow-circle-o-right",".fa-arrow-circle-o-up",".fa-arrow-circle-right",".fa-arrow-circle-up",".fa-arrow-down",".fa-arrow-left",".fa-arrow-right",".fa-arrow-up",".fa-arrows",".fa-arrows-alt",".fa-arrows-h",".fa-arrows-v",".fa-asterisk",".fa-automobile",".fa-backward",".fa-ban",".fa-bank",".fa-bar-chart-o",".fa-barcode",".fa-bars",".fa-beer",".fa-behance",".fa-behance-square",".fa-bell",".fa-bell-o",".fa-bitbucket",".fa-bitbucket-square",".fa-bitcoin",".fa-bold",".fa-bolt",".fa-bomb",".fa-book",".fa-bookmark",".fa-bookmark-o",".fa-border",".fa-briefcase",".fa-btc",".fa-bug",".fa-building",".fa-building-o",".fa-bullhorn",".fa-bullseye",".fa-cab",".fa-calendar",".fa-calendar-o",".fa-camera",".fa-camera-retro",".fa-car",".fa-caret-down",".fa-caret-left",".fa-caret-right",".fa-caret-square-o-down",".fa-caret-square-o-left",".fa-caret-square-o-right",".fa-caret-square-o-up",".fa-caret-up",".fa-certificate",".fa-chain",".fa-chain-broken",".fa-check",".fa-check-circle",".fa-check-circle-o",".fa-check-square",".fa-check-square-o",".fa-chevron-circle-down",".fa-chevron-circle-left",".fa-chevron-circle-right",".fa-chevron-circle-up",".fa-chevron-down",".fa-chevron-left",".fa-chevron-right",".fa-chevron-up",".fa-child",".fa-circle",".fa-circle-o",".fa-circle-o-notch",".fa-circle-thin",".fa-clipboard",".fa-clock-o",".fa-cloud",".fa-cloud-download",".fa-cloud-upload",".fa-cny",".fa-code",".fa-code-fork",".fa-codepen",".fa-coffee",".fa-cog",".fa-cogs",".fa-columns",".fa-comment",".fa-comment-o",".fa-comments",".fa-comments-o",".fa-compass",".fa-compress",".fa-copy",".fa-credit-card",".fa-crop",".fa-crosshairs",".fa-css3",".fa-cube",".fa-cubes",".fa-cut",".fa-cutlery",".fa-dashboard",".fa-database",".fa-dedent",".fa-delicious",".fa-desktop",".fa-deviantart",".fa-digg",".fa-dollar",".fa-dot-circle-o",".fa-download",".fa-dribbble",".fa-dropbox",".fa-drupal",".fa-edit",".fa-eject",".fa-ellipsis-h",".fa-ellipsis-v",".fa-empire",".fa-envelope",".fa-envelope-o",".fa-envelope-square",".fa-eraser",".fa-eur",".fa-euro",".fa-exchange",".fa-exclamation",".fa-exclamation-circle",".fa-exclamation-triangle",".fa-expand",".fa-external-link",".fa-external-link-square",".fa-eye",".fa-eye-slash",".fa-facebook",".fa-facebook-square",".fa-fast-backward",".fa-fast-forward",".fa-fax",".fa-female",".fa-fighter-jet",".fa-file",".fa-file-archive-o",".fa-file-audio-o",".fa-file-code-o",".fa-file-excel-o",".fa-file-image-o",".fa-file-movie-o",".fa-file-o",".fa-file-pdf-o",".fa-file-photo-o",".fa-file-picture-o",".fa-file-powerpoint-o",".fa-file-sound-o",".fa-file-text",".fa-file-text-o",".fa-file-video-o",".fa-file-word-o",".fa-file-zip-o",".fa-files-o",".fa-film",".fa-filter",".fa-fire",".fa-fire-extinguisher",".fa-flag",".fa-flag-checkered",".fa-flag-o",".fa-flash",".fa-flask",".fa-flickr",".fa-flip-horizontal",".fa-flip-vertical",".fa-floppy-o",".fa-folder",".fa-folder-o",".fa-folder-open",".fa-folder-open-o",".fa-font",".fa-forward",".fa-foursquare",".fa-frown-o",".fa-fw",".fa-gamepad",".fa-gavel",".fa-gbp",".fa-ge",".fa-gear",".fa-gears",".fa-gift",".fa-git",".fa-git-square",".fa-github",".fa-github-alt",".fa-github-square",".fa-gittip",".fa-glass",".fa-globe",".fa-google",".fa-google-plus",".fa-google-plus-square",".fa-graduation-cap",".fa-group",".fa-h-square",".fa-hacker-news",".fa-hand-o-down",".fa-hand-o-left",".fa-hand-o-right",".fa-hand-o-up",".fa-hdd-o",".fa-header",".fa-headphones",".fa-heart",".fa-heart-o",".fa-history",".fa-home",".fa-hospital-o",".fa-html5",".fa-image",".fa-inbox",".fa-indent",".fa-info",".fa-info-circle",".fa-inr",".fa-instagram",".fa-institution",".fa-inverse",".fa-italic",".fa-joomla",".fa-jpy",".fa-jsfiddle",".fa-key",".fa-keyboard-o",".fa-krw",".fa-language",".fa-laptop",".fa-leaf",".fa-legal",".fa-lemon-o",".fa-level-down",".fa-level-up",".fa-lg",".fa-li",".fa-li.fa-lg",".fa-life-bouy",".fa-life-ring",".fa-life-saver",".fa-lightbulb-o",".fa-link",".fa-linkedin",".fa-linkedin-square",".fa-linux",".fa-list",".fa-list-alt",".fa-list-ol",".fa-list-ul",".fa-location-arrow",".fa-lock",".fa-long-arrow-down",".fa-long-arrow-left",".fa-long-arrow-right",".fa-long-arrow-up",".fa-magic",".fa-magnet",".fa-mail-forward",".fa-mail-reply",".fa-mail-reply-all",".fa-male",".fa-map-marker",".fa-maxcdn",".fa-medkit",".fa-meh-o",".fa-microphone",".fa-microphone-slash",".fa-minus",".fa-minus-circle",".fa-minus-square",".fa-minus-square-o",".fa-mobile",".fa-mobile-phone",".fa-money",".fa-moon-o",".fa-mortar-board",".fa-music",".fa-navicon",".fa-openid",".fa-outdent",".fa-pagelines",".fa-paper-plane",".fa-paper-plane-o",".fa-paperclip",".fa-paragraph",".fa-paste",".fa-pause",".fa-paw",".fa-pencil",".fa-pencil-square",".fa-pencil-square-o",".fa-phone",".fa-phone-square",".fa-photo",".fa-picture-o",".fa-pied-piper",".fa-pied-piper-alt",".fa-pied-piper-square",".fa-pinterest",".fa-pinterest-square",".fa-plane",".fa-play",".fa-play-circle",".fa-play-circle-o",".fa-plus",".fa-plus-circle",".fa-plus-square",".fa-plus-square-o",".fa-power-off",".fa-print",".fa-puzzle-piece",".fa-qq",".fa-qrcode",".fa-question",".fa-question-circle",".fa-quote-left",".fa-quote-right",".fa-ra",".fa-random",".fa-rebel",".fa-recycle",".fa-reddit",".fa-reddit-square",".fa-refresh",".fa-renren",".fa-reorder",".fa-repeat",".fa-reply",".fa-reply-all",".fa-retweet",".fa-rmb",".fa-road",".fa-rocket",".fa-rotate-180",".fa-rotate-270",".fa-rotate-90",".fa-rotate-left",".fa-rotate-right",".fa-rouble",".fa-rss",".fa-rss-square",".fa-rub",".fa-ruble",".fa-rupee",".fa-save",".fa-scissors",".fa-search",".fa-search-minus",".fa-search-plus",".fa-send",".fa-send-o",".fa-share",".fa-share-alt",".fa-share-alt-square",".fa-share-square",".fa-share-square-o",".fa-shield",".fa-shopping-cart",".fa-sign-in",".fa-sign-out",".fa-signal",".fa-sitemap",".fa-skype",".fa-slack",".fa-sliders",".fa-smile-o",".fa-sort",".fa-sort-alpha-asc",".fa-sort-alpha-desc",".fa-sort-amount-asc",".fa-sort-amount-desc",".fa-sort-asc",".fa-sort-desc",".fa-sort-down",".fa-sort-numeric-asc",".fa-sort-numeric-desc",".fa-sort-up",".fa-soundcloud",".fa-space-shuttle",".fa-spin",".fa-spinner",".fa-spoon",".fa-spotify",".fa-square",".fa-square-o",".fa-stack",".fa-stack-1x",".fa-stack-2x",".fa-stack-exchange",".fa-stack-overflow",".fa-star",".fa-star-half",".fa-star-half-empty",".fa-star-half-full",".fa-star-half-o",".fa-star-o",".fa-steam",".fa-steam-square",".fa-step-backward",".fa-step-forward",".fa-stethoscope",".fa-stop",".fa-strikethrough",".fa-stumbleupon",".fa-stumbleupon-circle",".fa-subscript",".fa-suitcase",".fa-sun-o",".fa-superscript",".fa-support",".fa-table",".fa-tablet",".fa-tachometer",".fa-tag",".fa-tags",".fa-tasks",".fa-taxi",".fa-tencent-weibo",".fa-terminal",".fa-text-height",".fa-text-width",".fa-th",".fa-th-large",".fa-th-list",".fa-thumb-tack",".fa-thumbs-down",".fa-thumbs-o-down",".fa-thumbs-o-up",".fa-thumbs-up",".fa-ticket",".fa-times",".fa-times-circle",".fa-times-circle-o",".fa-tint",".fa-toggle-down",".fa-toggle-left",".fa-toggle-right",".fa-toggle-up",".fa-trash-o",".fa-tree",".fa-trello",".fa-trophy",".fa-truck",".fa-try",".fa-tumblr",".fa-tumblr-square",".fa-turkish-lira",".fa-twitter",".fa-twitter-square",".fa-ul",".fa-ul > li",".fa-umbrella",".fa-underline",".fa-undo",".fa-university",".fa-unlink",".fa-unlock",".fa-unlock-alt",".fa-unsorted",".fa-upload",".fa-usd",".fa-user",".fa-user-md",".fa-users",".fa-video-camera",".fa-vimeo-square",".fa-vine",".fa-vk",".fa-volume-down",".fa-volume-off",".fa-volume-up",".fa-warning",".fa-wechat",".fa-weibo",".fa-weixin",".fa-wheelchair",".fa-windows",".fa-won",".fa-wordpress",".fa-wrench",".fa-xing",".fa-xing-square",".fa-yahoo",".fa-yen",".fa-youtube",".fa-youtube-play",".fa-youtube-square",".fa.pull-left",".fa.pull-right",".fade",".fade.in",".fadeIn",".fadeInDown",".fadeInDownBig",".fadeInLeft",".fadeInLeftBig",".fadeInRight",".fadeInRightBig",".fadeInUp",".fadeInUpBig",".fadeOut",".fadeOutDown",".fadeOutDownBig",".fadeOutLeft",".fadeOutLeftBig",".fadeOutRight",".fadeOutRightBig",".fadeOutUp",".fadeOutUpBig",".field-row input",".field-row label",".flash",".flipInX",".flipInY",".flipOutX",".flipOutY",".font-opensans",".fontawesome",".form-control",".form-control-static",".form-control[disabled]",".form-control[readonly]",".form-group",".form-horizontal .checkbox",".form-horizontal .checkbox-inline",".form-horizontal .control-label",".form-horizontal .form-control-static",".form-horizontal .form-group",".form-horizontal .has-feedback .form-control-feedback",".form-horizontal .radio",".form-horizontal .radio-inline",".form-inline .checkbox",".form-inline .checkbox input[type=\"checkbox\"]",".form-inline .control-label",".form-inline .form-control",".form-inline .form-group",".form-inline .has-feedback .form-control-feedback",".form-inline .input-group > .form-control",".form-inline .radio",".form-inline .radio input[type=\"radio\"]",".gecko .dropdown > a",".gecko .dropdown-toggle",".gecko header .top-search-holder .search-area .search-button",".get-direction",".get-direction .btn-lg",".get-direction .le-input",".glyphicon",".glyphicon-adjust",".glyphicon-align-center",".glyphicon-align-justify",".glyphicon-align-left",".glyphicon-align-right",".glyphicon-arrow-down",".glyphicon-arrow-left",".glyphicon-arrow-right",".glyphicon-arrow-up",".glyphicon-asterisk",".glyphicon-backward",".glyphicon-ban-circle",".glyphicon-barcode",".glyphicon-bell",".glyphicon-bold",".glyphicon-book",".glyphicon-bookmark",".glyphicon-briefcase",".glyphicon-bullhorn",".glyphicon-calendar",".glyphicon-camera",".glyphicon-certificate",".glyphicon-check",".glyphicon-chevron-down",".glyphicon-chevron-left",".glyphicon-chevron-right",".glyphicon-chevron-up",".glyphicon-circle-arrow-down",".glyphicon-circle-arrow-left",".glyphicon-circle-arrow-right",".glyphicon-circle-arrow-up",".glyphicon-cloud",".glyphicon-cloud-download",".glyphicon-cloud-upload",".glyphicon-cog",".glyphicon-collapse-down",".glyphicon-collapse-up",".glyphicon-comment",".glyphicon-compressed",".glyphicon-copyright-mark",".glyphicon-credit-card",".glyphicon-cutlery",".glyphicon-dashboard",".glyphicon-download",".glyphicon-download-alt",".glyphicon-earphone",".glyphicon-edit",".glyphicon-eject",".glyphicon-envelope",".glyphicon-euro",".glyphicon-exclamation-sign",".glyphicon-expand",".glyphicon-export",".glyphicon-eye-close",".glyphicon-eye-open",".glyphicon-facetime-video",".glyphicon-fast-backward",".glyphicon-fast-forward",".glyphicon-file",".glyphicon-film",".glyphicon-filter",".glyphicon-fire",".glyphicon-flag",".glyphicon-flash",".glyphicon-floppy-disk",".glyphicon-floppy-open",".glyphicon-floppy-remove",".glyphicon-floppy-save",".glyphicon-floppy-saved",".glyphicon-folder-close",".glyphicon-folder-open",".glyphicon-font",".glyphicon-forward",".glyphicon-fullscreen",".glyphicon-gbp",".glyphicon-gift",".glyphicon-glass",".glyphicon-globe",".glyphicon-hand-down",".glyphicon-hand-left",".glyphicon-hand-right",".glyphicon-hand-up",".glyphicon-hd-video",".glyphicon-hdd",".glyphicon-header",".glyphicon-headphones",".glyphicon-heart",".glyphicon-heart-empty",".glyphicon-home",".glyphicon-import",".glyphicon-inbox",".glyphicon-indent-left",".glyphicon-indent-right",".glyphicon-info-sign",".glyphicon-italic",".glyphicon-leaf",".glyphicon-link",".glyphicon-list",".glyphicon-list-alt",".glyphicon-lock",".glyphicon-log-in",".glyphicon-log-out",".glyphicon-magnet",".glyphicon-map-marker",".glyphicon-minus",".glyphicon-minus-sign",".glyphicon-move",".glyphicon-music",".glyphicon-new-window",".glyphicon-off",".glyphicon-ok",".glyphicon-ok-circle",".glyphicon-ok-sign",".glyphicon-open",".glyphicon-paperclip",".glyphicon-pause",".glyphicon-pencil",".glyphicon-phone",".glyphicon-phone-alt",".glyphicon-picture",".glyphicon-plane",".glyphicon-play",".glyphicon-play-circle",".glyphicon-plus",".glyphicon-plus-sign",".glyphicon-print",".glyphicon-pushpin",".glyphicon-qrcode",".glyphicon-question-sign",".glyphicon-random",".glyphicon-record",".glyphicon-refresh",".glyphicon-registration-mark",".glyphicon-remove",".glyphicon-remove-circle",".glyphicon-remove-sign",".glyphicon-repeat",".glyphicon-resize-full",".glyphicon-resize-horizontal",".glyphicon-resize-small",".glyphicon-resize-vertical",".glyphicon-retweet",".glyphicon-road",".glyphicon-save",".glyphicon-saved",".glyphicon-screenshot",".glyphicon-sd-video",".glyphicon-search",".glyphicon-send",".glyphicon-share",".glyphicon-share-alt",".glyphicon-shopping-cart",".glyphicon-signal",".glyphicon-sort",".glyphicon-sort-by-alphabet",".glyphicon-sort-by-alphabet-alt",".glyphicon-sort-by-attributes",".glyphicon-sort-by-attributes-alt",".glyphicon-sort-by-order",".glyphicon-sort-by-order-alt",".glyphicon-sound-5-1",".glyphicon-sound-6-1",".glyphicon-sound-7-1",".glyphicon-sound-dolby",".glyphicon-sound-stereo",".glyphicon-star",".glyphicon-star-empty",".glyphicon-stats",".glyphicon-step-backward",".glyphicon-step-forward",".glyphicon-stop",".glyphicon-subtitles",".glyphicon-tag",".glyphicon-tags",".glyphicon-tasks",".glyphicon-text-height",".glyphicon-text-width",".glyphicon-th",".glyphicon-th-large",".glyphicon-th-list",".glyphicon-thumbs-down",".glyphicon-thumbs-up",".glyphicon-time",".glyphicon-tint",".glyphicon-tower",".glyphicon-transfer",".glyphicon-trash",".glyphicon-tree-conifer",".glyphicon-tree-deciduous",".glyphicon-unchecked",".glyphicon-upload",".glyphicon-usd",".glyphicon-user",".glyphicon-volume-down",".glyphicon-volume-off",".glyphicon-volume-up",".glyphicon-warning-sign",".glyphicon-wrench",".glyphicon-zoom-in",".glyphicon-zoom-out",".grabbing",".green",".green-text",".green.ribbon",".grid-list-products",".grid-list-products .control-bar",".grid-list-products .control-bar #popularity-sort",".grid-list-products .grid-list-buttons",".grid-list-products .grid-list-buttons ul li",".grid-list-products .grid-list-buttons ul li a",".grid-list-products .grid-list-buttons ul li a i",".grid-list-products .grid-list-buttons ul li.active a",".grid-list-products .grid-list-buttons ul li.active a i",".grid-list-products .product-grid-holder",".grid-list-products .product-grid-holder .pagination-holder .pagination",".grid-list-products .product-grid-holder .product-item-holder",".grid-list-products .product-grid-holder .product-item-holder .image",".grid-list-products .result-counter",".grid-list-products .section-title",".h1",".h1 .small",".h1 small",".h1.border",".h2",".h2 .small",".h2 small",".h3",".h3 .small",".h3 small",".h4",".h4 .small",".h4 small",".h5",".h5 .small",".h5 small",".h6",".h6 .small",".h6 small",".has-error .checkbox",".has-error .checkbox-inline",".has-error .control-label",".has-error .form-control",".has-error .form-control-feedback",".has-error .help-block",".has-error .input-group-addon",".has-error .radio",".has-error .radio-inline",".has-feedback",".has-feedback .form-control",".has-feedback .form-control-feedback",".has-success .checkbox",".has-success .checkbox-inline",".has-success .control-label",".has-success .form-control",".has-success .form-control-feedback",".has-success .help-block",".has-success .input-group-addon",".has-success .radio",".has-success .radio-inline",".has-warning .checkbox",".has-warning .checkbox-inline",".has-warning .control-label",".has-warning .form-control",".has-warning .form-control-feedback",".has-warning .help-block",".has-warning .input-group-addon",".has-warning .radio",".has-warning .radio-inline",".header-alt",".height-lg",".height-md",".height-sm",".height-xs",".help-block",".hidden",".hidden-lg",".hidden-md",".hidden-sm",".hidden-xs",".hide",".hinge",".homebanner",".homebanner a",".homebanner-holder",".homebanner-slider",".homebanner-slider .nav-holder",".homebanner-slider .nav-holder .btn-next",".homebanner-slider .nav-holder .btn-prev",".iconic-link",".ie9 #top-megamenu-nav .dropdown .dropdown-toggle",".ie9 .le-select",".ie9 .le-select select",".img-circle",".img-responsive",".img-rounded",".img-thumbnail",".info-404 .sub-form-row .le-button",".info-404 .sub-form-row input",".info-404 h2",".initialism",".inline",".inline-input .le-button",".inline-input .placeholder",".inline-input input",".inner",".inner-bottom",".inner-bottom-md",".inner-bottom-sm",".inner-bottom-xs",".inner-left",".inner-left-md",".inner-left-sm",".inner-left-xs",".inner-md",".inner-right",".inner-right-md",".inner-right-sm",".inner-right-xs",".inner-sm",".inner-top",".inner-top-md",".inner-top-sm",".inner-top-xs",".inner-xs",".input-group",".input-group .form-control",".input-group-addon",".input-group-addon input[type=\"checkbox\"]",".input-group-addon input[type=\"radio\"]",".input-group-addon.input-lg",".input-group-addon.input-sm",".input-group-btn",".input-group-btn > .btn",".input-group-btn > .btn + .btn",".input-group-btn > .btn-group",".input-group-lg > .form-control",".input-group-lg > .input-group-addon",".input-group-lg > .input-group-btn > .btn",".input-group-sm > .form-control",".input-group-sm > .input-group-addon",".input-group-sm > .input-group-btn > .btn",".input-group[class*=\"col-\"]",".input-lg",".input-sm",".invisible",".jumbotron",".jumbotron .container",".jumbotron .h1",".jumbotron h1",".jumbotron p",".label",".label-danger",".label-danger[href]",".label-default",".label-default[href]",".label-discount",".label-discount.clear",".label-discount.ribbon",".label-info",".label-info[href]",".label-primary",".label-primary[href]",".label-success",".label-success[href]",".label-warning",".label-warning[href]",".label[href]",".le-button",".le-button.big",".le-button.disabled",".le-button.huge",".le-button.inverse",".le-checkbox",".le-checkbox + .fake-box",".le-checkbox.big + .fake-box",".le-color",".le-dropdown",".le-dropdown .dropdown-menu",".le-dropdown .dropdown-menu li",".le-dropdown .dropdown-menu li a",".le-dropdown .dropdown-toggle",".le-dropdown .dropdown-toggle i",".le-dropdown i",".le-dropdown.open",".le-input",".le-links",".le-links li",".le-links li a",".le-quantity",".le-quantity .minus",".le-quantity .plus",".le-quantity input",".le-radio",".le-radio + .fake-box",".le-select",".le-select .le-select-in",".le-select select",".lead",".leave-reply .field-row",".leave-reply .reply-form",".leave-reply h3",".leave-reply p",".light-bg",".lightSpeedIn",".lightSpeedOut",".list-benefits > li",".list-group",".list-group-item",".list-group-item > .badge",".list-group-item > .badge + .badge",".list-group-item-danger",".list-group-item-heading",".list-group-item-info",".list-group-item-success",".list-group-item-text",".list-group-item-warning",".list-inline",".list-inline > li",".list-unstyled",".logo > a",".logo > a > svg",".logo-svg",".m-t-0",".m-t-35",".map-holder",".map-holder #map",".map-holder img",".map-holder label",".media",".media .media",".media > .pull-left",".media > .pull-right",".media-body",".media-heading",".media-list",".media-object",".megamenu-horizontal .dropdown .dropdown-toggle",".megamenu-horizontal .dropdown-menu",".megamenu-horizontal .dropdown-menu .yamm-content .dropdown-banner-holder",".megamenu-horizontal .dropdown.open .dropdown-toggle",".megamenu-horizontal .menu-item .dropdown-menu",".megamenu-horizontal .nav > li",".megamenu-horizontal .nav > li .dropdown-menu",".megamenu-horizontal .nav > li > .dropdown-toggle",".megamenu-horizontal .nav > li > a",".megamenu-vertical .yamm-content",".megamenu-vertical .yamm-content .dropdown-banner-holder",".megamenu-vertical .yamm-content .dropdown-banner-holder a",".megamenu-vertical .yamm-content h2",".megamenu-vertical .yamm-content li",".megamenu-vertical .yamm-content li a",".meta-row label",".meta-row span",".meta-row span a",".modal",".modal-backdrop",".modal-backdrop.fade",".modal-backdrop.in",".modal-body",".modal-content",".modal-dialog",".modal-footer",".modal-footer .btn + .btn",".modal-footer .btn-block + .btn-block",".modal-footer .btn-group .btn + .btn",".modal-header",".modal-header .close",".modal-lg",".modal-open",".modal-sm",".modal-title",".modal.fade .modal-dialog",".modal.in .modal-dialog",".nav",".nav .nav-divider",".nav .open > a",".nav > li",".nav > li > a",".nav > li > a > img",".nav > li.disabled > a",".nav-justified",".nav-justified > .dropdown .dropdown-menu",".nav-justified > li",".nav-justified > li > a",".nav-pills > .active > a > .badge",".nav-pills > li",".nav-pills > li + li",".nav-pills > li > a",".nav-pills > li > a > .badge",".nav-pills > li.active > a",".nav-stacked > li",".nav-stacked > li + li",".nav-tabs",".nav-tabs .dropdown-menu",".nav-tabs > li",".nav-tabs > li > a",".nav-tabs > li.active",".nav-tabs > li.active > a",".nav-tabs a",".nav-tabs li",".nav-tabs li > a",".nav-tabs li a",".nav-tabs li.active a",".nav-tabs ul",".nav-tabs-justified",".nav-tabs-justified > .active > a",".nav-tabs-justified > li > a",".nav-tabs.nav-justified",".nav-tabs.nav-justified > .active > a",".nav-tabs.nav-justified > .dropdown .dropdown-menu",".nav-tabs.nav-justified > li",".nav-tabs.nav-justified > li > a",".nav-tabs.simple li a",".nav-tabs.simple li.active a",".navbar",".navbar .navbar-toggle",".navbar .navbar-toggle .icon-bar",".navbar > .container .navbar-brand",".navbar > .container-fluid .navbar-brand",".navbar-brand",".navbar-btn",".navbar-btn.btn-sm",".navbar-btn.btn-xs",".navbar-collapse",".navbar-collapse.collapse",".navbar-collapse.in",".navbar-default",".navbar-default .navbar-brand",".navbar-default .navbar-collapse",".navbar-default .navbar-form",".navbar-default .navbar-link",".navbar-default .navbar-nav .open .dropdown-menu > .active > a",".navbar-default .navbar-nav .open .dropdown-menu > .disabled > a",".navbar-default .navbar-nav .open .dropdown-menu > li > a",".navbar-default .navbar-nav > .active > a",".navbar-default .navbar-nav > .disabled > a",".navbar-default .navbar-nav > .open > a",".navbar-default .navbar-nav > li > a",".navbar-default .navbar-text",".navbar-default .navbar-toggle",".navbar-default .navbar-toggle .icon-bar",".navbar-fixed-bottom",".navbar-fixed-bottom .dropdown .caret",".navbar-fixed-bottom .dropdown .dropdown-menu",".navbar-fixed-bottom .navbar-collapse",".navbar-fixed-bottom .navbar-nav > li > .dropdown-menu",".navbar-fixed-top",".navbar-fixed-top .navbar-collapse",".navbar-form",".navbar-form .checkbox",".navbar-form .checkbox input[type=\"checkbox\"]",".navbar-form .control-label",".navbar-form .form-control",".navbar-form .form-group",".navbar-form .has-feedback .form-control-feedback",".navbar-form .input-group > .form-control",".navbar-form .radio",".navbar-form .radio input[type=\"radio\"]",".navbar-form.navbar-right",".navbar-header",".navbar-inverse",".navbar-inverse .navbar-brand",".navbar-inverse .navbar-collapse",".navbar-inverse .navbar-form",".navbar-inverse .navbar-link",".navbar-inverse .navbar-nav .open .dropdown-menu .divider",".navbar-inverse .navbar-nav .open .dropdown-menu > .active > a",".navbar-inverse .navbar-nav .open .dropdown-menu > .disabled > a",".navbar-inverse .navbar-nav .open .dropdown-menu > .dropdown-header",".navbar-inverse .navbar-nav .open .dropdown-menu > li > a",".navbar-inverse .navbar-nav > .active > a",".navbar-inverse .navbar-nav > .disabled > a",".navbar-inverse .navbar-nav > .open > a",".navbar-inverse .navbar-nav > li > a",".navbar-inverse .navbar-text",".navbar-inverse .navbar-toggle",".navbar-inverse .navbar-toggle .icon-bar",".navbar-left",".navbar-nav",".navbar-nav .open .dropdown-menu",".navbar-nav .open .dropdown-menu .dropdown-header",".navbar-nav .open .dropdown-menu > li > a",".navbar-nav > li",".navbar-nav > li > .dropdown-menu",".navbar-nav > li > a",".navbar-nav.navbar-right",".navbar-right",".navbar-right .dropdown-menu",".navbar-right .dropdown-menu-left",".navbar-static-top",".navbar-static-top .navbar-collapse",".navbar-text",".navbar-text.navbar-right",".navbar-toggle",".navbar-toggle .icon-bar",".navbar-toggle .icon-bar + .icon-bar",".navy-text",".no-margin",".no-margin-left",".no-margin-right",".no-padding",".no-padding-bottom",".no-padding-left",".no-padding-right",".normal-weight",".open .dropdown-toggle.btn-danger",".open .dropdown-toggle.btn-default",".open .dropdown-toggle.btn-info",".open .dropdown-toggle.btn-primary",".open .dropdown-toggle.btn-success",".open .dropdown-toggle.btn-warning",".open > .dropdown-menu",".open > a",".orange-text",".our-store",".our-store address",".our-store h3",".outer",".outer-bottom",".outer-bottom-md",".outer-bottom-sm",".outer-bottom-xs",".outer-md",".outer-sm",".outer-top",".outer-top-md",".outer-top-sm",".outer-top-xs",".outer-xs",".owl-backSlide-in",".owl-backSlide-out",".owl-buttons",".owl-carousel",".owl-carousel .owl-item",".owl-carousel .owl-next",".owl-carousel .owl-prev",".owl-carousel .owl-wrapper",".owl-carousel .owl-wrapper-outer",".owl-carousel .owl-wrapper-outer.autoHeight",".owl-carousel-blog",".owl-carousel-blog .item",".owl-carousel-blog .owl-next",".owl-carousel-blog .owl-pagination .owl-page span",".owl-carousel-blog .owl-pagination .owl-page.active span",".owl-carousel-blog .owl-prev",".owl-controls",".owl-controls .owl-buttons div",".owl-controls .owl-page",".owl-fade-in",".owl-fade-out",".owl-fadeInAfterOut-in",".owl-fadeInAfterOut-out",".owl-fadeUp-in",".owl-fadeUp-out",".owl-goDown-in",".owl-goDown-out",".owl-inner-nav .owl-controls",".owl-inner-nav .owl-next",".owl-inner-nav .owl-prev",".owl-inner-nav.owl-ui-lg .owl-next",".owl-inner-nav.owl-ui-lg .owl-prev",".owl-inner-nav.owl-ui-md .owl-next",".owl-inner-nav.owl-ui-md .owl-prev",".owl-inner-pagination .owl-next",".owl-inner-pagination .owl-pagination",".owl-inner-pagination .owl-prev",".owl-inner-pagination.owl-inner-nav .owl-next",".owl-inner-pagination.owl-inner-nav .owl-pagination",".owl-inner-pagination.owl-inner-nav .owl-prev",".owl-inner-pagination.owl-inner-nav.owl-ui-lg .owl-pagination",".owl-inner-pagination.owl-inner-nav.owl-ui-md .owl-pagination",".owl-inner-pagination.owl-outer-nav .owl-next",".owl-inner-pagination.owl-outer-nav .owl-pagination",".owl-inner-pagination.owl-outer-nav .owl-prev",".owl-inner-pagination.owl-outer-nav.owl-ui-lg .owl-pagination",".owl-inner-pagination.owl-outer-nav.owl-ui-md .owl-pagination",".owl-inner-pagination.owl-ui-lg .owl-next",".owl-inner-pagination.owl-ui-lg .owl-pagination",".owl-inner-pagination.owl-ui-lg .owl-prev",".owl-inner-pagination.owl-ui-md .owl-next",".owl-inner-pagination.owl-ui-md .owl-pagination",".owl-inner-pagination.owl-ui-md .owl-prev",".owl-item-gap .item",".owl-item-gap-sm .item",".owl-item.loading",".owl-next",".owl-origin",".owl-outer-nav",".owl-outer-nav .owl-controls",".owl-outer-nav .owl-next",".owl-outer-nav .owl-pagination",".owl-outer-nav .owl-prev",".owl-outer-nav .owl-wrapper-outer",".owl-outer-nav.owl-ui-lg",".owl-outer-nav.owl-ui-lg .owl-next",".owl-outer-nav.owl-ui-lg .owl-prev",".owl-outer-nav.owl-ui-md",".owl-outer-nav.owl-ui-md .owl-next",".owl-outer-nav.owl-ui-md .owl-prev",".owl-pagination",".owl-pagination .owl-page span",".owl-pagination .owl-page.active span",".owl-prev",".owl-ui-lg .owl-next",".owl-ui-lg .owl-pagination",".owl-ui-lg .owl-prev",".owl-ui-md .owl-next",".owl-ui-md .owl-pagination",".owl-ui-md .owl-prev",".page-header",".page-header .page-subtitle",".page-header .page-title",".page-track-your-order .field-row",".page-track-your-order .le-input",".page-track-your-order .section",".page-track-your-order p",".page-wishlist .items-holder",".page-wishlist .items-holder .cart-item",".page-wishlist .items-holder .cart-item .brand",".page-wishlist .items-holder .cart-item .price",".page-wishlist .items-holder .cart-item .quantity",".page-wishlist .items-holder .cart-item .title",".page-wishlist .items-holder .cart-item .title a",".page-wishlist .items-holder .close-btn",".page-wishlist .items-holder .remove",".page-wishlist .items-holder .remove-item",".pager",".pager .disabled > a",".pager .disabled > span",".pager .next > a",".pager .next > span",".pager .previous > a",".pager .previous > span",".pager li",".pager li > a",".pager li > span",".pagination",".pagination > .active > a",".pagination > .active > span",".pagination > .disabled > a",".pagination > .disabled > span",".pagination > li",".pagination > li > a",".pagination > li > span",".pagination a",".pagination li a",".pagination li.current a",".pagination-lg > li > a",".pagination-lg > li > span",".pagination-sm > li > a",".pagination-sm > li > span",".panel",".panel > .list-group",".panel > .list-group .list-group-item",".panel > .panel-body + .table",".panel > .panel-body + .table-responsive",".panel > .table",".panel > .table > tbody > tr td",".panel > .table > tbody > tr th",".panel > .table > tfoot > tr td",".panel > .table > tfoot > tr th",".panel > .table > thead > tr td",".panel > .table > thead > tr th",".panel > .table-bordered",".panel > .table-bordered > tbody > tr > td",".panel > .table-bordered > tbody > tr > th",".panel > .table-bordered > tfoot > tr > td",".panel > .table-bordered > tfoot > tr > th",".panel > .table-bordered > thead > tr > td",".panel > .table-bordered > thead > tr > th",".panel > .table-responsive",".panel > .table-responsive > .table",".panel > .table-responsive > .table > tbody > tr td",".panel > .table-responsive > .table > tbody > tr th",".panel > .table-responsive > .table > tfoot > tr td",".panel > .table-responsive > .table > tfoot > tr th",".panel > .table-responsive > .table > thead > tr td",".panel > .table-responsive > .table > thead > tr th",".panel > .table-responsive > .table-bordered",".panel > .table-responsive > .table-bordered > tbody > tr > td",".panel > .table-responsive > .table-bordered > tbody > tr > th",".panel > .table-responsive > .table-bordered > tfoot > tr > td",".panel > .table-responsive > .table-bordered > tfoot > tr > th",".panel > .table-responsive > .table-bordered > thead > tr > td",".panel > .table-responsive > .table-bordered > thead > tr > th",".panel-body",".panel-danger",".panel-danger > .panel-footer + .panel-collapse .panel-body",".panel-danger > .panel-heading",".panel-danger > .panel-heading + .panel-collapse .panel-body",".panel-default",".panel-default > .panel-footer + .panel-collapse .panel-body",".panel-default > .panel-heading",".panel-default > .panel-heading + .panel-collapse .panel-body",".panel-footer",".panel-group",".panel-group .panel",".panel-group .panel + .panel",".panel-group .panel .owl-controls",".panel-group .panel-footer",".panel-group .panel-footer + .panel-collapse .panel-body",".panel-group .panel-heading",".panel-group .panel-heading + .panel-collapse .panel-body",".panel-group-faq .panel-faq .panel-body",".panel-group-faq .panel-faq .panel-heading",".panel-group-faq .panel-faq .panel-heading .panel-title",".panel-group-faq .panel-faq .panel-heading .panel-title > a",".panel-group-faq .panel-faq .panel-heading .panel-title > a.collapsed",".panel-group.blank .panel .owl-controls",".panel-heading",".panel-heading + .list-group .list-group-item",".panel-heading > .dropdown .dropdown-toggle",".panel-info",".panel-info > .panel-footer + .panel-collapse .panel-body",".panel-info > .panel-heading",".panel-info > .panel-heading + .panel-collapse .panel-body",".panel-primary",".panel-primary > .panel-footer + .panel-collapse .panel-body",".panel-primary > .panel-heading",".panel-primary > .panel-heading + .panel-collapse .panel-body",".panel-success",".panel-success > .panel-footer + .panel-collapse .panel-body",".panel-success > .panel-heading",".panel-success > .panel-heading + .panel-collapse .panel-body",".panel-title",".panel-title > a",".panel-warning",".panel-warning > .panel-footer + .panel-collapse .panel-body",".panel-warning > .panel-heading",".panel-warning > .panel-heading + .panel-collapse .panel-body",".popover",".popover > .arrow",".popover-content",".popover-title",".popover.bottom",".popover.bottom > .arrow",".popover.left",".popover.left > .arrow",".popover.right",".popover.right > .arrow",".popover.top",".popover.top > .arrow",".post-comment-button",".posts .format-link",".posts .format-link .post-title",".posts .format-link .post-title > a",".posts .format-quote",".posts .format-quote blockquote",".posts .format-quote blockquote footer",".posts .format-quote blockquote p",".posts .meta",".posts .meta > li",".posts .meta > li a",".posts .post",".posts .post .date-wrapper",".posts .post .date-wrapper .date",".posts .post .date-wrapper .date .day",".posts .post .date-wrapper .date .month",".posts .post .meta > li a",".posts .post .post-media",".posts .post .post-media iframe",".posts .post .post-title",".posts .post p",".posts .post-entry .inner-left",".posts .post-entry .meta",".posts .post-entry .post-title",".posts .post-entry blockquote",".posts .post-entry blockquote p",".posts .post-entry p",".posts .post-entry p.highlight",".posts .post-entry p.highlight-light",".posts.sidemeta",".posts.sidemeta .date-wrapper",".posts.sidemeta .format-wrapper",".posts.sidemeta .format-wrapper a",".pre-scrollable",".primary-bg",".primary-color",".product-grid-holder .product-item-holder",".product-grid-holder .product-item-holder .body",".product-grid-holder .product-item-holder .image",".product-grid-holder .product-item-holder .prices",".product-grid-holder .product-item-holder.size-big .body",".product-grid-holder .product-item-holder.size-big .body .title",".product-grid-holder .product-item-holder.size-big .price-current",".product-grid-holder .product-item-holder.size-big .prices",".product-grid-holder .product-item-holder.size-big .prices .le-button",".product-grid-holder .product-item-holder.size-medium",".product-grid-holder .product-item-holder.size-medium .body",".product-grid-holder .product-item-holder.size-medium .body .title",".product-grid-holder .product-item-holder.size-medium .image",".product-grid-holder .product-item-holder.size-small",".product-grid-holder .product-item-holder.size-small .body",".product-grid-holder .product-item-holder.size-small .body .brand",".product-grid-holder .product-item-holder.size-small .body .title",".product-grid-holder .product-item-holder.size-small .image",".product-grid-holder .product-item-holder.size-small .prices",".product-grid-holder .product-item-holder.size-small .ribbon",".product-grid-holder.medium .product-item-holder .prices",".product-item-holder",".product-item-holder .body .brand",".product-item-holder .body .title",".product-item-holder .body .title a",".product-item-holder .image",".product-item-holder .price-current",".product-item-holder .price-prev",".product-item-holder .prices",".product-item-holder .ribbon",".product-item-holder.hover",".product-item-holder.hover .hover-area",".product-item-holder.hover .product-item",".product-item-holder.hover .product-item .hover-area",".product-item-holder.hover .product-item .hover-area .add-cart-button",".product-item-holder.hover .product-item .hover-area .add-cart-button a",".product-item-holder.hover .product-item .hover-area .wish-compare a",".product-item-holder.hover.size-medium .product-item .add-cart-button",".product-item-holder.hover.size-medium .product-item .add-cart-button a",".product-item-holder.hover.size-medium .product-item .hover-area",".product-item-holder.hover.size-medium .product-item .wish-compare a",".product-item-holder.hover.size-small .product-item .add-cart-button",".product-item-holder.hover.size-small .product-item .add-cart-button a",".product-item-holder.hover.size-small .product-item .hover-area",".product-item-holder.hover.size-small .product-item .hover-area .wish-compare a",".product-item-holder.hover.test .hover-area",".product-item-holder.hover.test .product-item",".products-list",".products-list .product-item",".products-list .product-item .row",".products-list .product-item .row .body-holder",".products-list .product-item .row .body-holder .body .brand",".products-list .product-item .row .body-holder .body .excerpt",".products-list .product-item .row .body-holder .body .excerpt .star-holder",".products-list .product-item .row .body-holder .body .excerpt .star-holder img",".products-list .product-item .row .image-holder",".products-list .product-item .row .price-area",".products-list .product-item .row .price-area .le-button",".products-list .product-item .row .price-area .price-current",".products-list .product-item .row .price-area .price-prev",".progress",".progress-bar",".progress-bar-danger",".progress-bar-info",".progress-bar-success",".progress-bar-warning",".progress-striped .progress-bar",".progress-striped .progress-bar-danger",".progress-striped .progress-bar-info",".progress-striped .progress-bar-success",".progress-striped .progress-bar-warning",".progress.active .progress-bar",".pull-left",".pull-right",".pull-right > .dropdown-menu",".pulse",".radio",".radio + .radio",".radio input[type=\"radio\"]",".radio label",".radio-inline",".radio-inline + .radio-inline",".radio-inline input[type=\"radio\"]",".radio-inline[disabled]",".radio-label",".radio-label p",".radio-label.bold",".radio[disabled]",".red",".red-text",".red.ribbon",".register-form",".regular-bold",".ribbon",".ribbon span",".rollIn",".rollOut",".rotateIn",".rotateInDownLeft",".rotateInDownRight",".rotateInUpLeft",".rotateInUpRight",".rotateOut",".rotateOutDownLeft",".rotateOutDownRight",".rotateOutUpLeft",".rotateOutUpRight",".row",".rubberBand",".section",".section h2",".section li",".section p",".section-title",".semi-bold",".services .service",".services .service .service-icon",".services .service .service-icon i",".services .service h3",".services .service p",".shake",".show",".show-theme-options",".sidebar",".sidebar .price-filter",".sidebar .price-range-holder",".sidebar .price-range-holder .filter-button",".sidebar .price-range-holder .filter-button a",".sidebar .price-range-holder .min-max",".sidebar .price-range-holder .price-range.in",".sidebar .price-range-holder .price-slider",".sidebar .price-range-holder .slider .tooltip",".sidebar .simple-banner",".sidebar .simple-banner a",".sidebar .widget",".sidebar .widget .body",".sidebar .widget .bordered",".sidebar .widget .category-filter ul",".sidebar .widget .category-filter ul label",".sidebar .widget .category-filter ul li",".sidebar .widget .category-filter ul span",".sidebar .widget .product-list",".sidebar .widget .product-list .price",".sidebar .widget .product-list .price .price-current",".sidebar .widget .product-list .price .price-prev",".sidebar .widget .product-list li",".sidebar .widget .product-list li .row .thumb-holder",".sidebar .widget .product-list li .row > div",".sidebar .widget .product-list li a",".sidebar .widget .row",".sidebar .widget hr",".sidebar h1",".sidebar h2",".sidebar h3",".sidebar-page #recently-reviewd",".sidebar-page #single-product-tab",".sidebar-page .body-holder",".sidebar-page .body-holder .body",".sidebar-page .body-holder .body .buttons-holder",".sidebar-page .body-holder .body .social-row",".sidebar-page .gallery-holder",".sidebar.narrow",".sidebar.wide",".sidemenu-holder",".sidemenu-holder .side-menu .head",".sidemenu-holder .side-menu .head i",".sidemenu-holder .side-menu .menu-item",".sidemenu-holder .side-menu .menu-item.open",".sidemenu-holder .side-menu nav",".sidemenu-holder .side-menu nav .nav > li",".sidemenu-holder .side-menu nav .nav > li .yamm-content",".sidemenu-holder .side-menu nav .nav > li .yamm-content .dropdown-banner-holder",".sidemenu-holder .side-menu nav .nav > li .yamm-content .dropdown-banner-holder a",".sidemenu-holder .side-menu nav .nav > li .yamm-content li",".sidemenu-holder .side-menu nav .nav > li .yamm-content li a",".sidemenu-holder .side-menu nav .nav > li > .mega-menu",".sidemenu-holder .side-menu nav .nav > li > .sub-menu",".sidemenu-holder .side-menu nav .nav > li a",".simple-link",".single-product-gallery .gallery-thumbs",".single-product-gallery .gallery-thumbs .item",".single-product-gallery .gallery-thumbs .item a",".single-product-gallery .gallery-thumbs .nav-holder",".single-product-gallery .gallery-thumbs .nav-holder.left",".single-product-gallery .gallery-thumbs .nav-holder.right",".single-product-gallery .gallery-thumbs .next-btn",".single-product-gallery .gallery-thumbs .prev-btn",".single-product-gallery .gallery-thumbs li",".single-product-gallery .gallery-thumbs li a",".single-product-gallery .gallery-thumbs li a.active",".single-product-gallery .single-product-gallery-item",".single-product-gallery .single-product-gallery-item a",".single-product-gallery-thumbs .nav-holder",".slideInDown",".slideInLeft",".slideInRight",".slideInUp",".slideOutDown",".slideOutLeft",".slideOutRight",".slideOutUp",".slider",".slider .tooltip-inner",".slider input",".slider-handle",".slider-selection",".slider-track",".slider.slider-horizontal",".slider.slider-horizontal .slider-handle",".slider.slider-horizontal .slider-selection",".slider.slider-horizontal .slider-track",".small",".social-auth-buttons",".social-icons",".social-icons li",".social-icons li a",".sr-only",".star-holder .star",".swing",".tab-content > .active",".tab-content > .tab-pane",".table",".table .table",".table > caption + thead > tr > td",".table > caption + thead > tr > th",".table > colgroup + thead > tr > td",".table > colgroup + thead > tr > th",".table > tbody + tbody",".table > tbody > tr > td",".table > tbody > tr > td.active",".table > tbody > tr > td.danger",".table > tbody > tr > td.info",".table > tbody > tr > td.success",".table > tbody > tr > td.warning",".table > tbody > tr > th",".table > tbody > tr > th.active",".table > tbody > tr > th.danger",".table > tbody > tr > th.info",".table > tbody > tr > th.success",".table > tbody > tr > th.warning",".table > tbody > tr.active > td",".table > tbody > tr.active > th",".table > tbody > tr.danger > td",".table > tbody > tr.danger > th",".table > tbody > tr.info > td",".table > tbody > tr.info > th",".table > tbody > tr.success > td",".table > tbody > tr.success > th",".table > tbody > tr.warning > td",".table > tbody > tr.warning > th",".table > tfoot > tr > td",".table > tfoot > tr > td.active",".table > tfoot > tr > td.danger",".table > tfoot > tr > td.info",".table > tfoot > tr > td.success",".table > tfoot > tr > td.warning",".table > tfoot > tr > th",".table > tfoot > tr > th.active",".table > tfoot > tr > th.danger",".table > tfoot > tr > th.info",".table > tfoot > tr > th.success",".table > tfoot > tr > th.warning",".table > tfoot > tr.active > td",".table > tfoot > tr.active > th",".table > tfoot > tr.danger > td",".table > tfoot > tr.danger > th",".table > tfoot > tr.info > td",".table > tfoot > tr.info > th",".table > tfoot > tr.success > td",".table > tfoot > tr.success > th",".table > tfoot > tr.warning > td",".table > tfoot > tr.warning > th",".table > thead > tr > td",".table > thead > tr > td.active",".table > thead > tr > td.danger",".table > thead > tr > td.info",".table > thead > tr > td.success",".table > thead > tr > td.warning",".table > thead > tr > th",".table > thead > tr > th.active",".table > thead > tr > th.danger",".table > thead > tr > th.info",".table > thead > tr > th.success",".table > thead > tr > th.warning",".table > thead > tr.active > td",".table > thead > tr.active > th",".table > thead > tr.danger > td",".table > thead > tr.danger > th",".table > thead > tr.info > td",".table > thead > tr.info > th",".table > thead > tr.success > td",".table > thead > tr.success > th",".table > thead > tr.warning > td",".table > thead > tr.warning > th",".table-bordered",".table-bordered > tbody > tr > td",".table-bordered > tbody > tr > th",".table-bordered > tfoot > tr > td",".table-bordered > tfoot > tr > th",".table-bordered > thead > tr > td",".table-bordered > thead > tr > th",".table-condensed > tbody > tr > td",".table-condensed > tbody > tr > th",".table-condensed > tfoot > tr > td",".table-condensed > tfoot > tr > th",".table-condensed > thead > tr > td",".table-condensed > thead > tr > th",".table-hover > tbody > tr > td",".table-hover > tbody > tr > td.active",".table-hover > tbody > tr > td.danger",".table-hover > tbody > tr > td.info",".table-hover > tbody > tr > td.success",".table-hover > tbody > tr > td.warning",".table-hover > tbody > tr > th",".table-hover > tbody > tr > th.active",".table-hover > tbody > tr > th.danger",".table-hover > tbody > tr > th.info",".table-hover > tbody > tr > th.success",".table-hover > tbody > tr > th.warning",".table-hover > tbody > tr.active > td",".table-hover > tbody > tr.active > th",".table-hover > tbody > tr.danger > td",".table-hover > tbody > tr.danger > th",".table-hover > tbody > tr.info > td",".table-hover > tbody > tr.info > th",".table-hover > tbody > tr.success > td",".table-hover > tbody > tr.success > th",".table-hover > tbody > tr.warning > td",".table-hover > tbody > tr.warning > th",".table-responsive",".table-responsive > .table",".table-responsive > .table > tbody > tr > td",".table-responsive > .table > tbody > tr > th",".table-responsive > .table > tfoot > tr > td",".table-responsive > .table > tfoot > tr > th",".table-responsive > .table > thead > tr > td",".table-responsive > .table > thead > tr > th",".table-responsive > .table-bordered",".table-responsive > .table-bordered > tbody > tr > td",".table-responsive > .table-bordered > tbody > tr > th",".table-responsive > .table-bordered > tfoot > tr > td",".table-responsive > .table-bordered > tfoot > tr > th",".table-responsive > .table-bordered > thead > tr > td",".table-responsive > .table-bordered > thead > tr > th",".tabled-data li",".tabled-data li .value",".tabled-data li label",".tabled-data.inverse-bold .value",".tabled-data.inverse-bold label",".tabled-data.no-border",".tabled-data.no-border li",".tada",".team-members .team-member",".team-members .team-member .profile",".team-members .team-member .profile .social",".team-members .team-member .profile h3",".team-members .team-member .profile h3 .designation",".team-members .team-member .profile-pic",".text-center",".text-danger",".text-hide",".text-info",".text-justify",".text-left",".text-muted",".text-primary",".text-right",".text-success",".text-warning",".thumb-holder",".thumbnail",".thumbnail .caption",".thumbnail > img",".thumbnail a > img",".title-color",".tooltip",".tooltip-arrow",".tooltip-inner",".tooltip.bottom",".tooltip.bottom .tooltip-arrow",".tooltip.bottom-left .tooltip-arrow",".tooltip.bottom-right .tooltip-arrow",".tooltip.in",".tooltip.left",".tooltip.left .tooltip-arrow",".tooltip.right",".tooltip.right .tooltip-arrow",".tooltip.top",".tooltip.top .tooltip-arrow",".tooltip.top-left .tooltip-arrow",".tooltip.top-right .tooltip-arrow",".top-bar",".top-bar > .container > div",".top-bar > .container > div > ul > li > a",".top-bar ul",".top-bar ul > li",".top-bar ul > li a",".top-bar ul > li.dropdown .dropdown-menu",".top-bar ul > li.dropdown .dropdown-menu li",".top-bar ul > li.dropdown .dropdown-menu li a",".top-bar ul.right",".top-bar ul.right > li a",".top-cart-row-container",".uppercase",".visible-lg",".visible-md",".visible-print",".visible-sm",".visible-xs",".well",".well blockquote",".well-lg",".well-sm",".wobble",".yamm .collapse",".yamm .container",".yamm .dropdown",".yamm .dropdown-menu",".yamm .dropdown.yamm-fw .dropdown-menu",".yamm .dropup",".yamm .nav",".yamm .nav.navbar-right .dropdown-menu",".yamm .yamm-content",".zoomIn",".zoomInDown",".zoomInLeft",".zoomInRight",".zoomInUp",".zoomOut",".zoomOutDown",".zoomOutLeft",".zoomOutRight",".zoomOutUp","[data-toggle=\"buttons\"] > .btn > input[type=\"checkbox\"]","[data-toggle=\"buttons\"] > .btn > input[type=\"radio\"]","[hidden]","a","a.badge","a.bg-danger","a.bg-info","a.bg-primary","a.bg-success","a.bg-warning","a.list-group-item","a.list-group-item .list-group-item-heading","a.list-group-item-danger","a.list-group-item-danger .list-group-item-heading","a.list-group-item-danger.active","a.list-group-item-info","a.list-group-item-info .list-group-item-heading","a.list-group-item-info.active","a.list-group-item-success","a.list-group-item-success .list-group-item-heading","a.list-group-item-success.active","a.list-group-item-warning","a.list-group-item-warning .list-group-item-heading","a.list-group-item-warning.active","a.list-group-item.active","a.list-group-item.active .list-group-item-heading","a.list-group-item.active .list-group-item-text","a.list-group-item.active > .badge","a.text-danger","a.text-info","a.text-primary","a.text-success","a.text-warning","a.thumbnail","a.thumbnail.active","abbr[data-original-title]","abbr[title]","address","article","aside","audio","b","blockquote","blockquote .small","blockquote footer","blockquote ol","blockquote p","blockquote small","blockquote ul","blockquote.pull-right","blockquote.pull-right .small","blockquote.pull-right footer","blockquote.pull-right small","body","button","button.close","button[disabled]","canvas","cite","code","dd","details","dfn","dl","dt","fieldset","fieldset[disabled] .btn","fieldset[disabled] .btn-danger","fieldset[disabled] .btn-danger.active","fieldset[disabled] .btn-default","fieldset[disabled] .btn-default.active","fieldset[disabled] .btn-info","fieldset[disabled] .btn-info.active","fieldset[disabled] .btn-link","fieldset[disabled] .btn-primary","fieldset[disabled] .btn-primary.active","fieldset[disabled] .btn-success","fieldset[disabled] .btn-success.active","fieldset[disabled] .btn-warning","fieldset[disabled] .btn-warning.active","fieldset[disabled] .checkbox","fieldset[disabled] .checkbox-inline","fieldset[disabled] .form-control","fieldset[disabled] .radio","fieldset[disabled] .radio-inline","fieldset[disabled] input[type=\"checkbox\"]","fieldset[disabled] input[type=\"radio\"]","figcaption","figure","footer","h1","h1 .small","h1 small","h1.border","h2","h2 .small","h2 small","h2.border","h3","h3 .small","h3 small","h4","h4 .small","h4 small","h5","h5 .small","h5 small","h6","h6 .small","h6 small","header","header .logo svg","header .logo-holder .logo","header .top-cart-holder .basket .checkout .le-button","header .top-cart-holder .basket .dropdown-menu","header .top-cart-row","header .top-cart-row .top-cart-holder","header .top-cart-row .top-cart-holder .basket-item","header .top-cart-row .top-cart-holder .basket-item .close-btn","header .top-cart-row .top-cart-holder .basket-item .price","header .top-cart-row .top-cart-holder .basket-item .thumb","header .top-cart-row .top-cart-holder .basket-item .title","header .top-cart-row .top-cart-holder .basket-item-count","header .top-cart-row .top-cart-holder .basket-item-count .count","header .top-cart-row .top-cart-holder .checkout a","header .top-cart-row .top-cart-holder .dropdown-menu","header .top-cart-row .top-cart-holder .dropdown-toggle","header .top-cart-row .top-cart-holder .dropdown-toggle .lbl","header .top-cart-row .top-cart-holder .dropdown-toggle .total-price","header .top-cart-row .top-cart-holder .total-price-basket","header .top-cart-row .top-cart-holder li","header .top-cart-row .wishlist-compare-holder","header .top-cart-row .wishlist-compare-holder a","header .top-cart-row .wishlist-compare-holder a i","header .top-cart-row .wishlist-compare-holder i","header .top-search-holder","header .top-search-holder .contact-row","header .top-search-holder .contact-row .contact","header .top-search-holder .contact-row .phone","header .top-search-holder .contact-row > div","header .top-search-holder .contact-row i","header .top-search-holder .search-area","header .top-search-holder .search-area .categories-filter","header .top-search-holder .search-area .categories-filter .dropdown-menu","header .top-search-holder .search-area .categories-filter .dropdown-toggle","header .top-search-holder .search-area .search-button","header .top-search-holder .search-area .search-field","header .top-search-holder .search-field","header.ver2","hgroup","hr","html","html input[disabled]","html input[type=\"button\"]","img","img[src=\"assets/images/blank.gif\"]","input","input[type=\"button\"].btn-block","input[type=\"checkbox\"]","input[type=\"checkbox\"][disabled]","input[type=\"date\"]","input[type=\"file\"]","input[type=\"number\"]","input[type=\"radio\"]","input[type=\"radio\"][disabled]","input[type=\"range\"]","input[type=\"reset\"]","input[type=\"reset\"].btn-block","input[type=\"search\"]","input[type=\"submit\"]","input[type=\"submit\"].btn-block","kbd","label","label.error","legend","main","mark","nav","ol","ol ol","ol ul","optgroup","output","p","pre","pre code","progress","samp","section","select","select.input-group-lg > .form-control","select.input-group-lg > .input-group-addon","select.input-group-lg > .input-group-btn > .btn","select.input-group-sm > .form-control","select.input-group-sm > .input-group-addon","select.input-group-sm > .input-group-btn > .btn","select.input-lg","select.input-sm","select.top-drop-menu","select[multiple]","select[multiple].input-group-lg > .form-control","select[multiple].input-group-lg > .input-group-addon","select[multiple].input-group-lg > .input-group-btn > .btn","select[multiple].input-group-sm > .form-control","select[multiple].input-group-sm > .input-group-addon","select[multiple].input-group-sm > .input-group-btn > .btn","select[multiple].input-lg","select[multiple].input-sm","select[size]","small","strong","sub","summary","sup","table","table col[class*=\"col-\"]","table td[class*=\"col-\"]","table th[class*=\"col-\"]","table.visible-lg","table.visible-md","table.visible-sm","table.visible-xs","td","td.visible-lg","td.visible-md","td.visible-sm","td.visible-xs","template","textarea","textarea.form-control","textarea.input-group-lg > .form-control","textarea.input-group-lg > .input-group-addon","textarea.input-group-lg > .input-group-btn > .btn","textarea.input-group-sm > .form-control","textarea.input-group-sm > .input-group-addon","textarea.input-group-sm > .input-group-btn > .btn","textarea.input-lg","textarea.input-sm","th","th.visible-lg","th.visible-md","th.visible-sm","th.visible-xs","tr.visible-lg","tr.visible-md","tr.visible-sm","tr.visible-xs","ul","ul ol","ul ul","video"];pagespeed.criticalCssBeaconInit('/mod_pagespeed_beacon','index0a7d.html?page=home-2&amp;style=alt&amp;style=alt1&amp;style=alt1&amp;style=alt1&amp;style=alt1&amp;style=alt1&amp;style=alt1&amp;style=alt1&amp;style=alt1&amp;style=alt&amp;style=alt&amp;style=alt&amp;style=alt','kljsRbUF_a','Dp45iGqB0zk',pagespeed.selectors);
//]]></script></body>

<!-- Mirrored from demo2.transvelo.in/html/media-center/index.php?page=home-2&style=alt&style=alt1&style=alt1&style=alt1&style=alt1&style=alt1&style=alt1&style=alt1&style=alt1&style=alt&style=alt&style=alt&style=alt by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 21 Apr 2016 05:04:06 GMT -->
</html>