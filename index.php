<!DOCTYPE html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
</head>
<!----------------------------------------------->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.0/jquery.min.js"></script>
<script type="text/javascript">
$(function() {

$('.more_button').live("click",function() 
{
var getId = $(this).attr("id");
if(getId)
{
$("#load_more_"+getId).html('<img src="load_img.gif" style="padding:10px 0 0 100px;"/>');  
$.ajax({
type: "POST",
url: "more_content.php",
data: "getLastContentId="+ getId, 
cache: false,
success: function(html){
$("ul#load_more_ctnt").append(html);
$("#load_more_"+getId).remove();
}
});
}
else
{
$(".more_tab").html('The End');
}
return false;
});
});

</script>
<!---------------------------------------------->
<body>
<!-- ============================================================= HEADER : END ============================================================= -->	

<!-- ========================================== SECTION – HERO ========================================= -->
<?php
include("topbar.php");
$random=rand(1000,99999);
$_SESSION['id']=$random;
?>
<div id="hero">
<div id="owl-main" class="owl-carousel owl-inner-nav owl-ui-sm">
<div class="item" style="background-image:url(assets/images/sliders/xslider01.jpg.pagespeed.ic.ovD4GDHVDZ.jpg)">
<div class="container-fluid">
<div class="caption vertical-center text-left">
<div class="big-text fadeInDown-1">
Save up to a<span class="big"><span class="sign">$</span>400</span>
</div>
<div class="excerpt fadeInDown-2">
on selected laptops<br>
& desktop pcs or<br>
smartphones
</div>
<div class="small fadeInDown-2">
terms and conditions apply
</div>
<div class="button-holder fadeInDown-3">
<a href="#" class="big le-button ">shop now</a>
</div>
</div><!-- /.caption -->
</div><!-- /.container-fluid -->
</div><!-- /.item -->
<div class="item" style="background-image:url(assets/images/sliders/xslider03.jpg.pagespeed.ic.K3Ng59Tc9I.jpg)">
<div class="container-fluid">
<div class="caption vertical-center text-left">
<div class="big-text fadeInDown-1">
Want a<span class="big"><span class="sign">$</span>200</span>Discount?
</div>
<div class="excerpt fadeInDown-2">
on selected <br>desktop pcs<br>
</div>
<div class="small fadeInDown-2">
terms and conditions apply
</div>
<div class="button-holder fadeInDown-3">
<a href="#" class="big le-button ">shop now</a>
</div>
</div><!-- /.caption -->
</div><!-- /.container-fluid -->
</div><!-- /.item -->
</div><!-- /.owl-carousel -->
</div>
<!-- ========================================= SECTION – HERO : END ========================================= -->
</div><!-- /.homebanner-holder -->
</div><!-- /.container -->
</div><!-- /#top-banner-and-menu -->
<!-- ========================================= SECTION – HERO : END ========================================= -->	<!-- /.homepage2 -->
<!-- ========================================= HOME BANNERS ========================================= -->
<section id="banner-holder" class="wow fadeInUp">
<div class="container">
<div class="row">
  <div class="col-xs-12 col-md-6 col-sm-6 col-lg-6 text-right banner">
  <a href="index7bbc.html?page=category-grid-2">
  <div class="banner-text right">
<h1>Time &amp; Style</h1>
<span class="tagline">Checkout new arrivals</span>
</div>
</a>
</div>
</div>
</div><!-- /.container -->
</section><!-- /#banner-holder -->
<!-- ========================================= HOME BANNERS : END ========================================= -->
<div id="products-tab" class="wow fadeInUp">
<div class="container">
<div class="tab-holder">
<!-- Nav tabs -->
<ul class="nav nav-tabs">
<li class="active"><a href="#featured" data-toggle="tab">featured</a></li>
<li><a href="#new-arrivals" data-toggle="tab">new arrivals</a>
</ul>
<!-- Tab panes -->
<div class="tab-content">
<div class="tab-pane active" id="featured">
<div class="product-grid-holder">
<?php
//include("dboperation.php");
$obj1=new dboperation();
$query1="select * from `productt`";
$result1=$obj1->selectdata($query1);
$rowcount=0;
while($f1=$obj1->fetch($result1))
{
$va=$f1['p_id'];
echo'<div class="col-sm-4 col-md-3 no-margin product-item-holder hover">
<div class="product-item">
<div class="ribbon red"><span>sale</span></div>
<div class="ribbon green"><span>bestseller</span></div>
<div class="image">
<img alt="" src="images/'.$f1['p_image'].'"/>
</div>
<div class="body">
<div class="label-discount clear"></div>
<div class="title">';
echo"<a href='specific_product.php?var=$va'>".$f1['product'].'</a>
</div>';
$br=$f1['brand_id'];
$query9="SELECT brand_name FROM `brand` WHERE brand_id='".$br."'";
$result9=$obj1->selectdata($query9);
if($f9=$obj1->fetch($result9))
{
echo'<div class="brand">'.$f9[0].'</div>';
}
echo'</div>
<div class="prices">
<div class="price-prev">'.$f1['price'].'</div>
<div class="price-current pull-right">'.$f1['price'].'</div>
</div>
<div class="hover-area">
<div class="add-cart-button">';
echo"<a href='specific_product.php?var=$va' class='le-button'>add to cart</a>";
echo'</div>
</div>';

echo'</div>
</div>';
$rowcount=$rowcount+1;
if($rowcount % 4 == 0)
{
echo"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
echo'<br><br><br>';
}
if($rowcount==8)
{
	break;
}
}
?>

</div>
<div class="loadmore-holder text-center">
<a class="btn-loadmore" href="loadproduct.php">
<i class="fa fa-plus"></i>
load more products</a>
</div>
</div>
<!--=================================================
===================================================-->
<div class="tab-pane" id="new-arrivals">
<div class="product-grid-holder">

<?php
$query2="select * from `productt`order by date DESC";
$result2=$obj1->selectdata($query2);
$rowcount=0;
while($f2=$obj1->fetch($result2))
{
$pid=$f2['p_id'];
echo'<div class="col-sm-4 col-md-3 no-margin product-item-holder hover">
<div class="product-item">
<div class="ribbon red"><span>sale</span></div>
<div class="ribbon green"><span>bestseller</span></div>
<div class="image">
<img alt="" src="images/'.$f2['p_image'].'"/>
</div>
<div class="body">
<div class="label-discount clear"></div>
<div class="title">';
echo"<a href='specific_product.php?var=$pid'>"
.$f2['product'].'</a>
</div>';
$br=$f2['brand_id'];
$query7="SELECT brand_name FROM `brand` WHERE brand_id='".$br."'";
$result7=$obj1->selectdata($query7);
if($f7=$obj1->fetch($result7))
{
echo'<div class="brand">'.$f7[0].'</div>';
}
echo'</div>
<div class="prices">
<div class="price-prev">'.$f2['price'].'</div>
<div class="price-current pull-right">'.$f2['price'].'</div>
</div>
<div class="hover-area">
<div class="add-cart-button">';
echo"<a href='specific_product.php?var=$pid' class='le-button'>add to cart</a>";
echo'</div>
</div>';

echo'</div>
</div>';
$rowcount=$rowcount+1;
if($rowcount % 4 == 0)
{
echo"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
echo'<br><br><br>';
}
if($rowcount==8)
{
	break;
}
}

?>
</div>
<div class="loadmore-holder text-center">
<a class="btn-loadmore" href="loadproduct.php">
<i class="fa fa-plus"></i>
load more products</a>
</div>
</div>
<!----=====================================================
======================================================
===========================================--->

</div>
</div>
</div>
</div>
<!-- ========================================= BEST SELLERS ========================================= -->

<!-- ========================================= BEST SELLERS : END ========================================= -->
<!-- ========================================= RECENTLY VIEWED ========================================= -->
<section id="recently-reviewd" class="wow fadeInUp">
<div class="container">
<div class="carousel-holder hover">
<div class="title-nav">
<h2 class="h1">Recently Viewed</h2>
<div class="nav-holder">
<a href="#prev" data-target="#owl-recently-viewed" class="slider-prev btn-prev fa fa-angle-left"></a>
<a href="#next" data-target="#owl-recently-viewed" class="slider-next btn-next fa fa-angle-right"></a>
</div>
</div><!-- /.title-nav -->
<div id="owl-recently-viewed" class="owl-carousel product-grid-holder">
<?php
$query3="select * from `productt`";
$result3=$obj1->selectdata($query3);
while($f3=$obj1->fetch($result3))
{
	$ppid=$f3['p_id'];
echo'<div class=" no-margin carousel-item product-item-holder size-small hover">
<div class="product-item">
<div class="ribbon blue"><span>new!</span></div>
<div class="image">
<img alt="" src="images/'.$f3['p_image'].'"/>
</div>
<div class="body"><div class="title">';
echo"
<a href='specific_product.php?var=$ppid'>
".$f3['product'].'
</div>';
$br=$f3['brand_id'];
$query8="SELECT brand_name FROM `brand` WHERE brand_id='".$br."'";
$result8=$obj1->selectdata($query8);
if($f8=$obj1->fetch($result8))
{
echo'<div class="brand">'.$f8[0].'</div>';
}
echo'</div>
<div class="prices">
<div class="price-current text-right">'.$f3['price'].'</div>
</div>
<div class="hover-area">
<div class="add-cart-button">';
echo"<a href='specific_product.php?var=$ppid' class='le-button'>add to cart</a>";
echo'</div>

</div>
</div><!-- /.product-item -->
</div><!-- /.product-item-holder -->';
}
?>
</div><!-- /#recently-carousel -->
</div><!-- /.carousel-holder -->
</div><!-- /.container -->
</section><!-- /#recently-reviewd -->
<!-- ========================================= RECENTLY VIEWED : END ========================================= -->
<!-- ========================================= TOP BRANDS ========================================= -->
<section id="top-brands" class="wow fadeInUp">
<div class="container">
<div class="carousel-holder">
<div class="title-nav">
<h1>Top Brands</h1>
<div class="nav-holder">
<a href="#prev" data-target="#owl-brands" class="slider-prev btn-prev fa fa-angle-left"></a>
<a href="#next" data-target="#owl-brands" class="slider-next btn-next fa fa-angle-right"></a>
</div>
</div><!-- /.title-nav -->
<div id="owl-brands" class="owl-carousel brands-carousel">
<div class="carousel-item">
<a href="#">
<img alt="" src="assets/images/brands/xbrand-01.jpg.pagespeed.ic.kIH08WkO6X.jpg"/>
</a>
</div><!-- /.carousel-item -->
<div class="carousel-item">
<a href="#">
<img alt="" src="assets/images/brands/xbrand-02.jpg.pagespeed.ic.BIRo28nbI_.jpg"/>
</a>
</div><!-- /.carousel-item -->
<div class="carousel-item">
<a href="#">
<img alt="" src="assets/images/brands/xbrand-03.jpg.pagespeed.ic.hIhTU044uJ.jpg"/>
</a>
</div><!-- /.carousel-item -->
<div class="carousel-item">
<a href="#">
<img alt="" src="assets/images/brands/xbrand-04.jpg.pagespeed.ic.yC5XGevAqN.jpg"/>
</a>
</div><!-- /.carousel-item -->
<div class="carousel-item">
<a href="#">
<img alt="" src="assets/images/brands/xbrand-01.jpg.pagespeed.ic.kIH08WkO6X.jpg"/>
</a>
</div><!-- /.carousel-item -->
<div class="carousel-item">
<a href="#">
<img alt="" src="assets/images/brands/xbrand-02.jpg.pagespeed.ic.BIRo28nbI_.jpg"/>
</a>
</div><!-- /.carousel-item -->
<div class="carousel-item">
<a href="#">
<img alt="" src="assets/images/brands/xbrand-03.jpg.pagespeed.ic.hIhTU044uJ.jpg"/>
</a>
</div><!-- /.carousel-item -->
<div class="carousel-item">
<a href="#">
<img alt="" src="assets/images/brands/xbrand-04.jpg.pagespeed.ic.yC5XGevAqN.jpg"/>
</a>
</div><!-- /.carousel-item -->
</div><!-- /.brands-caresoul -->
</div><!-- /.carousel-holder -->
</div><!-- /.container -->
</section><!-- /#top-brands -->
<!-- ========================================= TOP BRANDS : END ========================================= -->	<!-- ============================================================= FOOTER ============================================================= -->
<?php
include("footer.php");
?>
<!-- ============================================================= FOOTER : END ============================================================= -->	</div><!-- /.wrapper -->
<div class="config open"></div>

</body>
</html>