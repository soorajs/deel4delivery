<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
<?php
include("topbar.php");
?>
<div class="main-content" id="main-content">
<div class="container">
<div class="inner-xs">
<div class="page-header">
<h2 class="page-title">
Product Comparison
</h2>
</div>
</div><!-- /.section-page-title -->
<div class="table-responsive inner-bottom-xs inner-top-xs">
<table class="table table-bordered table-striped compare-list">
<thead>
<tr>
<td>&nbsp;</td>
<td class="text-center">
<div class="image-wrap">

<img width="220" height="154" alt="Iconia W700" class="attachment-yith-woocompare-image" src="../../../demo.transvelo.com/media-center-wp/wp-content/uploads/2014/09/p4.2-220x154.png">
</div>
<p><strong>Iconia W700</strong></p>
</td>
<td class="text-center">
<div class="image-wrap">
<a data-product_id="34" href="#" class="remove-link"><i class="fa fa-times-circle"></i></a>
<img width="220" height="154" alt="POV Action Cam" class="attachment-yith-woocompare-image" src="../../../demo.transvelo.com/media-center-wp/wp-content/uploads/2014/09/p3-220x154.png">
</div>
<p><strong>AS100V Action Cam</strong></p>
</td>
<td class="text-center">
<div class="image-wrap">
<a data-product_id="30" href="#" class="remove-link"><i class="fa fa-times-circle"></i></a>
<img width="220" height="154" alt="PlayStation 4" class="attachment-yith-woocompare-image" src="../../../demo.transvelo.com/media-center-wp/wp-content/uploads/2014/09/p6-220x154.jpeg">
</div>
<p><strong>PlayStation 4</strong></p>
</td>
<td class="text-center">
<div class="image-wrap">
<a data-product_id="20" href="#" class="remove-link"><i class="fa fa-times-circle"></i></a>
<img width="220" height="154" alt="Cyber-shot Digital Camera WX300" class="attachment-yith-woocompare-image" src="../../../demo.transvelo.com/media-center-wp/wp-content/uploads/2014/09/p9.2-220x154.png">
</div>
<p><strong>Cyber-shot Digital Camera WX300</strong></p>
</td>
</tr>
<tr class="tr-add-to-cart">
<td>&nbsp;</td>
<td class="text-center">
<div class="add-cart-button">
<a class="le-button add_to_cart_button  product_type_simple" href="index5f38.html?page=cart">Add to cart</a>
</div>
</td>
<td class="text-center">
<div class="add-cart-button">
<a class="le-button add_to_cart_button  product_type_simple" href="index5f38.html?page=cart">Add to cart</a>
</div>
</td>
<td class="text-center">
<div class="add-cart-button">
<a class="le-button add_to_cart_button  product_type_simple" href="index5f38.html?page=cart">Add to cart</a>
</div>
</td>
<td class="text-center">
<div class="add-cart-button">
<a class="le-button add_to_cart_button  product_type_variable" href="index5f38.html?page=cart">Add to cart</a>
</div>
</td>
</tr>
</thead>
<tbody>
<tr class="comparison-item price">
<th>Price</th>
<td class="comparison-item-cell odd product_39">
<span class="amount">$629.99</span>
</td>
<td class="comparison-item-cell even product_34">
<del><span class="amount">$299.99</span></del>
<ins><span class="amount">$269.99</span></ins>
</td>
<td class="comparison-item-cell odd product_30">
<span class="amount">$399.99</span>
</td>
<td class="comparison-item-cell even product_20">
<del><span class="amount">$329.99</span></del>
<ins><span class="amount">$249.99</span>&ndash;<span class="amount">$329.99</span></ins>
</td>
</tr>
<tr class="comparison-item description">
<th>Description</th>
<td class="comparison-item-cell odd product_39">
<p>Nulla sed sapien a ligula auctor maximus. Aliquam eget condimentum nunc. Maecenas efficitur pretium nunc in semper. Nullam ac luctus nisl. </p>
</td>
<td class="comparison-item-cell even product_34">
<p>Nulla sed sapien a ligula auctor maximus. Aliquam eget condimentum nunc. Maecenas efficitur pretium nunc in semper. Nullam ac luctus nisl. </p>
</td>
<td class="comparison-item-cell odd product_30">
<p>Nulla sed sapien a ligula auctor maximus. Aliquam eget condimentum nunc. Maecenas efficitur pretium nunc in semper. Nullam ac luctus nisl. </p>
</td>
<td class="comparison-item-cell even product_20">
<p>Nulla sed sapien a ligula auctor maximus. Aliquam eget condimentum nunc. Maecenas efficitur pretium nunc in semper. Nullam ac luctus nisl. </p>
</td>
</tr>
<tr class="comparison-item stock">
<th>Availability</th>
<td class="comparison-item-cell odd product_39">
<span class="label label-success"><span class="">In stock</span></span>
</td>
<td class="comparison-item-cell even product_34">
<span class="label label-success"><span class="">In stock</span></span>
</td>
<td class="comparison-item-cell odd product_30">
<span class="label label-success"><span class="">In stock</span></span>
</td>
<td class="comparison-item-cell even product_20">
<span class="label label-success"><span class="">In stock</span></span>
</td>
</tr>
<tr class="price repeated">
<th>Price</th>
<td class="odd product_39"><span class="amount">$629.99</span></td>
<td class="even product_34"><del><span class="amount">$299.99</span></del> <ins><span class="amount">$269.99</span></ins></td>
<td class="odd product_30"><span class="amount">$399.99</span></td>
<td class="even product_20"><del><span class="amount">$329.99</span></del> <ins><span class="amount">$249.99</span>&ndash;<span class="amount">$329.99</span></ins></td>
</tr>
</tbody>
</table>
</div><!-- /.table-responsive -->
</div><!-- /.container -->
</div>

</body>
</html>